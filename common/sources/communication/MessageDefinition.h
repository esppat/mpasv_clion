//
// Created by esppat on 08/04/2020.
//

#pragma once

#include <string>

namespace mpasv::communication
{
	inline std::string TopicDeclarationNameToTopicName(const char * Name_With_Underscores)
	{
		std::string result = Name_With_Underscores;
		std::replace(result.begin(), result.end(), '_', '/');
		return std::move(result);
	}

#define DECLARE_STRUCT_AND_TOPICNAME(Name_With_Underscores) \
    const TopicName Topic_##Name_With_Underscores { TopicDeclarationNameToTopicName(#Name_With_Underscores) }; \
    struct Data_##Name_With_Underscores

	// Global

	enum class CaptainOrder
		: uint32_t
	{
		  Invalid       = 0
		, InternalCheck = 1
		, };

	DECLARE_STRUCT_AND_TOPICNAME(Global_CaptainOrder)
	{
		CaptainOrder order;
	};

	// Routing

	DECLARE_STRUCT_AND_TOPICNAME(Routing_Route_BasicData)
	{
		uint64_t totalWaypointCount;
		char     routeName[256];
	};

	DECLARE_STRUCT_AND_TOPICNAME(Routing_Waypoint_Description)
	{
		double lat_deg;
		double lon_deg;
		double maxDistanceToPass_m;
		char   name[64];
		double distanceFromStart_m;
		double durationToStay_s;
		double maxDistanceToStay_m;
		double speedToHere_kts;
	};

	// Situation

	DECLARE_STRUCT_AND_TOPICNAME(Situation_Gps_Full)
	{
		double lat_deg;
		double lon_deg;
		double speed_kts;
		double bearing_deg;
	};

	DECLARE_STRUCT_AND_TOPICNAME(Situation_Wind_Full)
	{
		double speed_kts;
		double bearing_deg;
	};

	DECLARE_STRUCT_AND_TOPICNAME(Situation_Water_RelativeSpeed)
	{
		double speed_kts;
	};

	DECLARE_STRUCT_AND_TOPICNAME(Situation_IMU_Compass)
	{
		double bearing_deg;
	};
}

using namespace mpasv::communication;
