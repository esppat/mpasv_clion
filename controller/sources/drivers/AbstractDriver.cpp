/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   AbstractDriver.cpp
 * Author: esppat
 *
 * Created on December 17, 2019, 10:57 PM
 */

#include "AbstractDriver.h"

#include <utility>

namespace mpasv::drivers
{

	AbstractDriver::AbstractDriver(const std::string name, std::shared_ptr<controller::simulation::SimulationController> simulationController)
		: m_name(std::move(name)), m_simulationController(simulationController), m_simulationMode(DM()->getConstantValue<bool>(
		"Param.VirtualCaptain.Simulation.Do simulation (bool)"))
	{
		CHECK_FALSE(m_name.empty());
	}
} // mpasv
