/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   DriverFactory.h
 * Author: esppat
 *
 * Created on December 18, 2019, 12:14 AM
 */

#pragma once

#include "tools/miscel/Demangler.h"
#include "tools/miscel/Exception.h"
#include "tools/miscel/Tools.h"
#include "tools/parameters/ParameterManager.h"

#include "AbstractDriver.h"
#include "AbstractSensor.h"
#include "AbstractActuator.h"

#include <map>

namespace mpasv
{
	namespace controller
	{
		namespace simulation
		{
			class SimulationController;
		}
	}

	namespace drivers
	{

		class DriverFactory
			: public enable_shared_from_this<DriverFactory>
		{
		public:

			DriverFactory(std::shared_ptr<controller::simulation::SimulationController> simulationController);

			DriverFactory(const DriverFactory & orig) = delete;

			virtual ~DriverFactory();

			template<class DRIVER>
			std::shared_ptr<DRIVER> spawnDriver(const std::string & name
			                                    , const std::shared_ptr<controller::simulation::SimulationController> simulationController)
			{
				LOG_DEBUG("Spawning driver \"", typeName<DRIVER>(), "\" named \"", name, "\"");
				return std::make_shared<DRIVER>(name, simulationController);
			}

		protected:
		protected:
		private:

		private:

			std::shared_ptr<controller::simulation::SimulationController> m_simulationController;
		};
	} // drivers
} // mpasv
