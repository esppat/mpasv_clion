//
// Created by esppat on 29/03/2020.
//

#include "PowerSensor.h"
#include "drivers/sensors/ampere/AmperageSensor.h"
#include "drivers/sensors/voltage/VoltageSensor.h"

namespace mpasv::drivers::power
{

	PowerSensor::PowerSensor(const std::string & name, const std::shared_ptr<controller::simulation::SimulationController> simulationController)
		: AbstractSensor(name, simulationController)
	{
		DM()->declareData<watt_t>(name + ".Sensor.Power.Power");
		DM()->declareDataLimits(name + ".Sensor.Power.Power", -1000.0_W, +1000.0_W);
	}

	void PowerSensor::setSensors(std::shared_ptr<mpasv::drivers::amperage::AmperageSensor> amperageSensor
	                             , std::shared_ptr<mpasv::drivers::voltage::VoltageSensor> voltageSensor)
	{
		CHECK_NOT_NULLPTR(amperageSensor);
		CHECK_NOT_NULLPTR(voltageSensor);

		m_amperageSensor = amperageSensor;
		m_voltageSensor  = voltageSensor;
	}

	void PowerSensor::pullImpl()
	{
		m_amperageSensor->pull();
		m_voltageSensor->pull();

		if (DM()->valueIsAvailable<ampere_t>(m_amperageSensor->name() + ".Sensor.Amperage.Amperage") &&
		    DM()->valueIsAvailable<volt_t>(m_voltageSensor->name() + ".Sensor.Voltage.Voltage"))
		{
			DM()->setValue<watt_t>(name() + ".Sensor.Power.Power", DM()->getValue<ampere_t>(m_amperageSensor->name() + ".Sensor.Amperage.Amperage") *
			                                                       DM()->getValue<volt_t>(m_voltageSensor->name() + ".Sensor.Voltage.Voltage"));
		}
		else
		{
			DM()->setValueUnavailable<watt_t>(name() + ".Sensor.Power.Power");
		}
	}
} // mpasv
