//
// Created by esppat on 29/03/2020.
//

#pragma once

#include "drivers/AbstractSensor.h"

namespace mpasv::drivers
{
	namespace amperage
	{
		class AmperageSensor;
	}

	namespace voltage
	{
		class VoltageSensor;
	}

	namespace power
	{

		class PowerSensor
			: public drivers::AbstractSensor
			  , public enable_shared_from_this<PowerSensor>
		{
		public:

			PowerSensor(const std::string & name, const std::shared_ptr<controller::simulation::SimulationController> simulationController);

			PowerSensor(const PowerSensor & orig) = delete;

			virtual ~PowerSensor() = default;

			void setSensors(std::shared_ptr<mpasv::drivers::amperage::AmperageSensor> amperageSensor
			                , std::shared_ptr<mpasv::drivers::voltage::VoltageSensor> voltageSensor);

		protected:

			virtual void pullImpl() override;

		protected:
		private:
		private:

			std::shared_ptr<mpasv::drivers::amperage::AmperageSensor> m_amperageSensor;
			std::shared_ptr<mpasv::drivers::voltage::VoltageSensor>   m_voltageSensor;
		};
	}
}

using namespace mpasv::drivers::power;
