/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   WaterSpeedSensor.cpp
 * Author: esppat
 *
 * Created on December 17, 2019, 11:31 PM
 */

#include "WaterSpeedSensor.h"

namespace mpasv::drivers::water
{

	WaterSpeedSensor::WaterSpeedSensor(const std::string & name, const std::shared_ptr<controller::simulation::SimulationController> simulationController)
		: AbstractSensor(name, simulationController)
	{
		DM()->declareData<knot_t>(name + ".Sensor.Water speed.Speed");
		DM()->declareDataLimits(name + ".Sensor.Water speed.Speed", -30.0_kts, +30.0_kts);
	}

	void WaterSpeedSensor::pullImpl()
	{
		if (m_simulationMode)
		{
			// TODO get value from simulation controller

			if (simulationController().hasWaterSpeed())
			{
				DM()->setValue<knot_t>(name() + ".Sensor.Water speed.Speed", simulationController().getWaterSpeed());
			}
			else
			{
				DM()->setValueUnavailable<knot_t>(name() + ".Sensor.Water speed.Speed");
			}
		}
		else
		{
			// TODO

			DM()->setValueUnavailable<knot_t>(name() + ".Sensor.Water speed.Speed");
		}
	}
} // mpasv
