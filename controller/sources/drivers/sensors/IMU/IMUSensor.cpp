/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   IMUSensor.cpp
 * Author: esppat
 *
 * Created on December 17, 2019, 11:31 PM
 */

#include "IMUSensor.h"

namespace mpasv::drivers::IMU
{

	IMUSensor::IMUSensor(const std::string & name, const std::shared_ptr<controller::simulation::SimulationController> simulationController)
		: AbstractSensor(name, simulationController)
	{
		DM()->declareData<degree_t>(name + ".Sensor.IMU.Compass");
		DM()->declareDataLimits(name + ".Sensor.IMU.Compass", -180.0_deg, +180.0_deg);

		DM()->declareData<meters_per_second_squared_t>(name + ".Sensor.IMU.accelX");
		DM()->declareData<meters_per_second_squared_t>(name + ".Sensor.IMU.accelY");
		DM()->declareData<meters_per_second_squared_t>(name + ".Sensor.IMU.accelZ");

		DM()->declareData<radians_per_second_t>(name + ".Sensor.IMU.gyroX");
		DM()->declareData<radians_per_second_t>(name + ".Sensor.IMU.gyroY");
		DM()->declareData<radians_per_second_t>(name + ".Sensor.IMU.gyroZ");
	}

	void IMUSensor::pullImpl()
	{
		if (m_simulationMode)
		{
			// TODO get value from simulation controller

			if (simulationController().hasCompass())
			{
				DM()->setValue<degree_t>(name() + ".Sensor.IMU.Compass", simulationController().getCompass());
			}
			else
			{
				DM()->setValueUnavailable<degree_t>(name() + ".Sensor.IMU.Compass");
			}

			if (simulationController().hasAccelX())
			{
				DM()->setValue<meters_per_second_squared_t>(name() + ".Sensor.IMU.accelX", simulationController().getAccelX());
			}
			else
			{
				DM()->setValueUnavailable<meters_per_second_squared_t>(name() + ".Sensor.IMU.accelX");
			}

			if (simulationController().hasAccelY())
			{
				DM()->setValue<meters_per_second_squared_t>(name() + ".Sensor.IMU.accelY", simulationController().getAccelY());
			}
			else
			{
				DM()->setValueUnavailable<meters_per_second_squared_t>(name() + ".Sensor.IMU.accelY");
			}

			if (simulationController().hasAccelZ())
			{
				DM()->setValue<meters_per_second_squared_t>(name() + ".Sensor.IMU.accelZ", simulationController().getAccelZ());
			}
			else
			{
				DM()->setValueUnavailable<meters_per_second_squared_t>(name() + ".Sensor.IMU.accelZ");
			}

			if (simulationController().hasGyroX())
			{
				DM()->setValue<radians_per_second_t>(name() + ".Sensor.IMU.gyroX", simulationController().getGyroX());
			}
			else
			{
				DM()->setValueUnavailable<radians_per_second_t>(name() + ".Sensor.IMU.gyroX");
			}

			if (simulationController().hasGyroY())
			{
				DM()->setValue<radians_per_second_t>(name() + ".Sensor.IMU.gyroY", simulationController().getGyroY());
			}
			else
			{
				DM()->setValueUnavailable<radians_per_second_t>(name() + ".Sensor.IMU.gyroY");
			}

			if (simulationController().hasGyroZ())
			{
				DM()->setValue<radians_per_second_t>(name() + ".Sensor.IMU.gyroZ", simulationController().getGyroZ());
			}
			else
			{
				DM()->setValueUnavailable<radians_per_second_t>(name() + ".Sensor.IMU.gyroZ");
			}
		}
		else
		{
			// TODO

			DM()->setValueUnavailable<degree_t>(name() + ".Sensor.IMU.Compass");

			DM()->setValueUnavailable<meters_per_second_squared_t>(name() + ".Sensor.IMU.accelX");
			DM()->setValueUnavailable<meters_per_second_squared_t>(name() + ".Sensor.IMU.accelY");
			DM()->setValueUnavailable<meters_per_second_squared_t>(name() + ".Sensor.IMU.accelZ");

			DM()->setValueUnavailable<radians_per_second_t>(name() + ".Sensor.IMU.gyroX");
			DM()->setValueUnavailable<radians_per_second_t>(name() + ".Sensor.IMU.gyroY");
			DM()->setValueUnavailable<radians_per_second_t>(name() + ".Sensor.IMU.gyroZ");
		}
	}
} // mpasv
