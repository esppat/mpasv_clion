//
// Created by esppat on 04/04/2020.
//

#include "GPSSensorFusion.h"
#include "drivers/DriverManager.h"

namespace mpasv::drivers::GPS
{

	GPSSensorFusion::GPSSensorFusion(const std::string & name, const std::shared_ptr<controller::simulation::SimulationController> simulationController)
		: GPSSensor(name, simulationController)
	{
	}

	void GPSSensorFusion::setGPSSensors(const std::list<std::string> & GPSSensorNames)
	{
		for (std::string GPSSensorName : GPSSensorNames)
		{
			m_GPSSensors.push_back(simulationController().getDriverManager()->getSensor<GPSSensor>(GPSSensorName));
		}
	}

	void GPSSensorFusion::pullImpl()
	{
		bool hasFix = false;

		for (auto GPSSensor : m_GPSSensors)
		{
			GPSSensor->pull();
			if (!hasFix &&
			    DM()->valueIsAvailable<bool>(GPSSensor->name() + ".Sensor.GPS.Has fix") &&
			    DM()->getValue<bool>(GPSSensor->name() + ".Sensor.GPS.Has fix"))
			{
				hasFix = true;
			}
		}

		DM()->setValue(name() + ".Sensor.GPS.Has fix", hasFix);

		uint64_t nbFix = 0;

		double lon     = 0;
		double lat     = 0;
		double bearing = 0;
		double speed   = 0;

		if (hasFix)
		{
			for (auto GPSSensor : m_GPSSensors)
			{
				if (DM()->valueIsAvailable<bool>(GPSSensor->name() + ".Sensor.GPS.Has fix") && DM()->getValue<bool>(GPSSensor->name() + ".Sensor.GPS.Has fix"))
				{
					lon += doubleFrom<longitude_t>(DM()->getValue<longitude_t>(GPSSensor->name() + ".Sensor.GPS.Lon"));
					lat += doubleFrom<latitude_t>(DM()->getValue<latitude_t>(GPSSensor->name() + ".Sensor.GPS.Lat"));
					bearing += doubleFrom<degree_t>(bearingToTrigo(DM()->getValue<degree_t>(GPSSensor->name() + ".Sensor.GPS.Bearing")));
					speed += doubleFrom<knot_t>(DM()->getValue<knot_t>(GPSSensor->name() + ".Sensor.GPS.Speed"));

					nbFix++;
				}
			}

			CHECK_GT(nbFix, 0);

			lon /= nbFix;
			lat /= nbFix;
			bearing = doubleFrom<degree_t>(trigoToBearing((degree_t)bearing / nbFix));
			speed /= nbFix;

			DM()->setValue<longitude_t>(name() + ".Sensor.GPS.Lon", (longitude_t)lon);
			DM()->setValue<latitude_t>(name() + ".Sensor.GPS.Lat", (latitude_t)lat);
			DM()->setValue<degree_t>(name() + ".Sensor.GPS.Bearing", (degree_t)bearing);
			DM()->setValue<knot_t>(name() + ".Sensor.GPS.Speed", (knot_t)speed);
		}
	}
} // mpasv
