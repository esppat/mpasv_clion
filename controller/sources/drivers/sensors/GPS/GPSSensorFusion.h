//
// Created by esppat on 04/04/2020.
//

#pragma once

#include "GPSSensor.h"

namespace mpasv
{
	namespace drivers
	{
		namespace GPS
		{
			class GPSSensorFusion
				: public GPSSensor
				  , public enable_shared_from_this<GPSSensorFusion>
			{
			public:

				GPSSensorFusion(const std::string & name, const std::shared_ptr<SimulationController> simulationController);

				GPSSensorFusion(const GPSSensorFusion & orig) = delete;

				virtual ~GPSSensorFusion() = default;

				void setGPSSensors(const std::list<std::string> & GPSSensorNames);

			protected:

				virtual void pullImpl() override;

			protected:
			private:
			private:

				std::list<std::shared_ptr<GPSSensor>> m_GPSSensors;
			};
		} // GPS
	} // drivers
} // mpasv

using namespace mpasv::drivers::GPS;
