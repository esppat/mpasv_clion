/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   GPSSensor.cpp
 * Author: esppat
 *
 * Created on December 17, 2019, 11:31 PM
 */

#include "GPSSensor.h"

namespace mpasv::drivers::GPS
{

	GPSSensor::GPSSensor(const std::string & name, const std::shared_ptr<controller::simulation::SimulationController> simulationController)
		: AbstractSensor(name, simulationController)
	{
		DM()->declareData<bool>(name + ".Sensor.GPS.Has fix");

		DM()->declareData<longitude_t>(name + ".Sensor.GPS.Lon");
		DM()->declareDataLimits(name + ".Sensor.GPS.Lon", -180.0_deg, +180.0_deg);

		DM()->declareData<latitude_t>(name + ".Sensor.GPS.Lat");
		DM()->declareDataLimits(name + ".Sensor.GPS.Lat", -90.0_deg, +90.0_deg);

		DM()->declareData<degree_t>(name + ".Sensor.GPS.Bearing");
		DM()->declareDataLimits(name + ".Sensor.GPS.Bearing", -180.0_deg, +180.0_deg);

		DM()->declareData<knot_t>(name + ".Sensor.GPS.Speed");
		DM()->declareDataLimits(name + ".Sensor.GPS.Speed", 0.0_kts, +30.0_kts);
	}

	void GPSSensor::pullImpl()
	{
		if (m_simulationMode)
		{
			// TODO get value from simulation controller

			bool hasFix = simulationController().hasGeoPos() && simulationController().getGeoPos().isValid();

			DM()->setValue(name() + ".Sensor.GPS.Has fix", hasFix);

			if (hasFix)
			{
				const GeoPos & geoPos = simulationController().getGeoPos();
				DM()->setValue<longitude_t>(name() + ".Sensor.GPS.Lon", geoPos.lon());
				DM()->setValue<latitude_t>(name() + ".Sensor.GPS.Lat", geoPos.lat());
				DM()->setValue<degree_t>(name() + ".Sensor.GPS.Bearing", simulationController().getGPSBearing());
				DM()->setValue<knot_t>(name() + ".Sensor.GPS.Speed", simulationController().getGPSSpeed());
			}
		}
		else
		{
			// TODO

			DM()->setValue(name() + ".Sensor.GPS.Has fix", false);

			DM()->setValueUnavailable<longitude_t>(name() + ".Sensor.GPS.Lon");
			DM()->setValueUnavailable<latitude_t>(name() + ".Sensor.GPS.Lat");
			DM()->setValueUnavailable<degree_t>(name() + ".Sensor.GPS.Bearing");
			DM()->setValueUnavailable<knot_t>(name() + ".Sensor.GPS.Speed");
		}
	}
} // mpasv
