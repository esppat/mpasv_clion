/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   GPSSensor.h
 * Author: esppat
 *
 * Created on December 17, 2019, 11:31 PM
 */

#pragma once

#include "drivers/AbstractSensor.h"

namespace mpasv
{
	namespace drivers
	{
		namespace GPS
		{
			class GPSSensor
				: public drivers::AbstractSensor
				  , public enable_shared_from_this<GPSSensor>
			{
			public:

				GPSSensor(const std::string & name, const std::shared_ptr<controller::simulation::SimulationController> simulationController);

				GPSSensor(const GPSSensor & orig) = delete;

				virtual ~GPSSensor() = default;

			protected:

				virtual void pullImpl() override;

			protected:
			private:
			private:
			};
		} // GPS
	} // drivers
} // mpasv

using namespace mpasv::drivers::GPS;
