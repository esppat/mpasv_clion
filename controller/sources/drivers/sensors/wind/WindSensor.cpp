/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   WindSensor.cpp
 * Author: esppat
 *
 * Created on December 17, 2019, 11:31 PM
 */

#include "WindSensor.h"

namespace mpasv::drivers::wind
{

	WindSensor::WindSensor(const std::string & name, const std::shared_ptr<controller::simulation::SimulationController> simulationController)
		: AbstractSensor(name, simulationController)
	{
		DM()->declareData<knot_t>(name + ".Sensor.Wind.Speed");
		DM()->declareDataLimits(name + ".Sensor.Wind.Speed", 0.0_kts, +200.0_kts);

		DM()->declareData<degree_t>(name + ".Sensor.Wind.Bearing");
		DM()->declareDataLimits(name + ".Sensor.Wind.Bearing", -180.0_deg, +180.0_deg);
	}

	void WindSensor::pullImpl()
	{
		if (m_simulationMode)
		{
			if (simulationController().hasRelativeWindSpeed())
			{
				DM()->setValue<knot_t>(name() + ".Sensor.Wind.Speed", simulationController().getRelativeWindSpeed());
			}
			else
			{
				DM()->setValueUnavailable<knot_t>(name() + ".Sensor.Wind.Speed");
			}

			if (simulationController().hasRelativeWindBearing())
			{
				DM()->setValue<degree_t>(name() + ".Sensor.Wind.Bearing", simulationController().getRelativeWindBearing());
			}
			else
			{
				DM()->setValueUnavailable<degree_t>(name() + ".Sensor.Wind.Bearing");
			}
		}
		else
		{
			// TODO

			DM()->setValueUnavailable<knot_t>(name() + ".Sensor.Wind.Speed");
			DM()->setValueUnavailable<degree_t>(name() + ".Sensor.Wind.Bearing");
		}
	}
} // mpasv
