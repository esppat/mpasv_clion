/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   WindSensor.h
 * Author: esppat
 *
 * Created on December 17, 2019, 11:31 PM
 */

#pragma once

#include "drivers/AbstractSensor.h"

namespace mpasv::drivers::wind
{
	class WindSpeedSensorSim;

	class WindSensor
		: public drivers::AbstractSensor
		  , public enable_shared_from_this<WindSensor>
	{
	public:

		typedef WindSpeedSensorSim SimulatedType;

		WindSensor(const std::string & name, const std::shared_ptr<controller::simulation::SimulationController> simulationController);

		WindSensor(const WindSensor & orig) = delete;

		virtual ~WindSensor() = default;

	protected:

		virtual void pullImpl() override;

	protected:
	private:
	private:
	};
}

using namespace mpasv::drivers::wind;
