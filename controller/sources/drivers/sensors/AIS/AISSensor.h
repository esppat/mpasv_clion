/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   AISSensor.h
 * Author: esppat
 *
 * Created on December 17, 2019, 11:31 PM
 */

#pragma once

#include "drivers/AbstractSensor.h"

namespace mpasv::drivers::AIS
{

	class AISSensor
		: public drivers::AbstractSensor
		  , public enable_shared_from_this<AISSensor>
	{
	public:

		AISSensor(const std::string & name, const std::shared_ptr<controller::simulation::SimulationController> simulationController);

		AISSensor(const AISSensor & orig) = delete;

		virtual ~AISSensor() = default;

	protected:

		virtual void pullImpl() override;

	protected:
	private:
	private:
	};
}
