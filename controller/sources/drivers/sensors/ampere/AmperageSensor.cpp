/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   AmperageSensor.cpp
 * Author: esppat
 *
 * Created on December 17, 2019, 11:31 PM
 */

#include "AmperageSensor.h"

namespace mpasv::drivers::amperage
{

	AmperageSensor::AmperageSensor(const std::string & name, const std::shared_ptr<controller::simulation::SimulationController> simulationController)
		: AbstractSensor(name, simulationController)
	{
		DM()->declareData<ampere_t>(name + ".Sensor.Amperage.Amperage");
		DM()->declareDataLimits(name + ".Sensor.Amperage.Amperage", -1000.0_A, +1000.0_A);
	}

	void AmperageSensor::pullImpl()
	{
		if (m_simulationMode)
		{
			if (simulationController().m_amperages.isAvailable(name() + ".Sensor.Amperage.Amperage"))
			{
				DM()->setValue<ampere_t>(name() + ".Sensor.Amperage.Amperage", simulationController().m_amperages.get(name() + ".Sensor.Amperage.Amperage"));
			}
			else
			{
				DM()->setValueUnavailable<ampere_t>(name() + ".Sensor.Amperage.Amperage");
			}
		}
		else
		{
			// TODO

			DM()->setValueUnavailable<ampere_t>(name() + ".Sensor.Amperage.Amperage");
		}
	}
} // mpasv
