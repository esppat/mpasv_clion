/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   VoltageSensor.h
 * Author: esppat
 *
 * Created on December 17, 2019, 11:31 PM
 */

#pragma once

#include "drivers/AbstractSensor.h"

namespace mpasv::drivers::voltage
{
	class VoltageSensorSim;

	class VoltageSensor
		: public drivers::AbstractSensor
		  , public enable_shared_from_this<VoltageSensor>
	{
	public:

		VoltageSensor(const std::string & name, const std::shared_ptr<controller::simulation::SimulationController> simulationController);

		VoltageSensor(const VoltageSensor & orig) = delete;

		virtual ~VoltageSensor() = default;

	protected:

		virtual void pullImpl() override;

	protected:
	private:
	private:
	};
}

using namespace mpasv::drivers::voltage;
