/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   VoltageSensor.cpp
 * Author: esppat
 *
 * Created on December 17, 2019, 11:31 PM
 */

#include "VoltageSensor.h"

namespace mpasv::drivers::voltage
{

	VoltageSensor::VoltageSensor(const std::string & name, const std::shared_ptr<controller::simulation::SimulationController> simulationController)
		: AbstractSensor(name, simulationController)
	{
		DM()->declareData<volt_t>(name + ".Sensor.Voltage.Voltage");
		DM()->declareDataLimits(name + ".Sensor.Voltage.Voltage", -1000.0_V, +1000.0_V);
	}

	void VoltageSensor::pullImpl()
	{
		if (m_simulationMode)
		{
			if (simulationController().m_voltages.isAvailable(name() + ".Sensor.Voltage.Voltage"))
			{
				DM()->setValue<volt_t>(name() + ".Sensor.Voltage.Voltage", simulationController().m_voltages.get(name() + ".Sensor.Voltage.Voltage"));
			}
			else
			{
				DM()->setValueUnavailable<volt_t>(name() + ".Sensor.Voltage.Voltage");
			}
		}
		else
		{
			// TODO

			DM()->setValueUnavailable<volt_t>(name() + ".Sensor.Voltage.Voltage");
		}
	}
} // mpasv
