/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   BrushlessController.cpp
 * Author: esppat
 *
 * Created on December 17, 2019, 11:43 PM
 */

#include "BrushlessController.h"

namespace mpasv::drivers::brushless_controller
{

	BrushlessController::BrushlessController(const std::string & name, const std::shared_ptr<controller::simulation::SimulationController> simulationController)
		: AbstractActuator(name, simulationController), m_value(0.0, 0.5_Hz, -1.0, +1.0)
	{
		DM()->declareData<dimensionless_t>(name + ".Brushless controller.Requested value");
		DM()->declareDataLimits<dimensionless_t>(name + ".Brushless controller.Requested value", -1.0, +1.0);
		DM()->setValue<dimensionless_t>(name + ".Brushless controller.Requested value", 0.0);

		DM()->declareData<dimensionless_t>(name + ".Brushless controller.Current value");
		DM()->declareDataLimits<dimensionless_t>(name + ".Brushless controller.Current value", -1.0, +1.0);
		DM()->setValue<dimensionless_t>(name + ".Brushless controller.Current value", 0.0);
	}

	void BrushlessController::pushImpl()
	{
		if (DM()->valueIsAvailable<dimensionless_t>(name() + ".Brushless controller.Requested value"))
		{
			m_value.set(DM()->getValue<dimensionless_t>(name() + ".Brushless controller.Requested value"));

			dimensionless_t pos = m_value.get();
			DM()->setValue<dimensionless_t>(name() + ".Brushless controller.Current value", pos);

			if (!m_simulationMode)
			{
				// TODO
			}
		}
		else
		{
			LOG_DEBUG("'" + name() + ".Brushless controller.Requested value' is not available");
		}
	}
} // mpasv
