/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   BrushlessController.h
 * Author: esppat
 *
 * Created on December 17, 2019, 11:43 PM
 */

#pragma once

#include "drivers/AbstractActuator.h"

namespace mpasv::drivers::brushless_controller
{

	class BrushlessController
		: public drivers::AbstractActuator
		  , public enable_shared_from_this<BrushlessController>
	{
	public:

		BrushlessController(const std::string & name, const std::shared_ptr<controller::simulation::SimulationController> simulationController);

		BrushlessController(const BrushlessController & orig) = delete;

		virtual ~BrushlessController() = default;

	protected:

	protected:

		virtual void pushImpl() override;

	private:
	private:

		RegulatedValue<dimensionless_t, hertz_t> m_value;
	};
}
