/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   PWMServo.cpp
 * Author: esppat
 *
 * Created on December 17, 2019, 11:43 PM
 */

#include "PWMServo.h"

namespace mpasv::drivers::pwm_servo
{

	PWMServo::PWMServo(const std::string & name, const std::shared_ptr<controller::simulation::SimulationController> simulationController)
		: AbstractActuator(name, simulationController), m_value(0.0, 1.0_Hz, -1.0, +1.0)
	{
		DM()->declareData<dimensionless_t>(name + ".Servo.Requested value");
		DM()->declareDataLimits<dimensionless_t>(name + ".Servo.Requested value", -1.0, +1.0);

		DM()->declareData<dimensionless_t>(name + ".Servo.Current value");
		DM()->declareDataLimits<dimensionless_t>(name + ".Servo.Current value", -1.0, +1.0);

		DM()->declareData<degree_t>(name + ".Servo.Current angle");

		DM()->setValue<dimensionless_t>(name + ".Servo.Requested value", 0.0);
		DM()->setValue<dimensionless_t>(name + ".Servo.Current value", 0.0);
		DM()->setValue<degree_t>(name + ".Servo.Current angle", 0.0_deg);
	}

	void PWMServo::pushImpl()
	{
		if (DM()->valueIsAvailable<dimensionless_t>(name() + ".Servo.Requested value"))
		{
			m_value.set(DM()->getValue<dimensionless_t>(name() + ".Servo.Requested value"));

			dimensionless_t pos = m_value.get();
			DM()->setValue<dimensionless_t>(name() + ".Servo.Current value", pos);
			DM()->setValue<degree_t>(name() + ".Servo.Current angle", pos * m_stroke);

			if (!m_simulationMode)
			{
				// TODO
			}
		}
		else
		{
			LOG_DEBUG("." + name() + ".Servo.Requested value' is not available");
		}
	}
} // mpasv
