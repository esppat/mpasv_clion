/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   AbstractActuator.h
 * Author: esppat
 *
 * Created on December 17, 2019, 10:57 PM
 */

#pragma once

#include "AbstractDriver.h"

#include <memory>

namespace mpasv
{
	namespace drivers
	{

		class AbstractActuator
			: public drivers::AbstractDriver
			  , public enable_shared_from_this<AbstractActuator>
		{
		public:
			AbstractActuator(const std::string & name, const std::shared_ptr<controller::simulation::SimulationController> simulationController);

			AbstractActuator(const AbstractActuator & orig) = delete;

			virtual ~AbstractActuator();

		protected:

			virtual bool canPush() const override final
			{
				return true;
			}

		protected:
		private:
		private:
		};
	} // drivers
} // mpasv
