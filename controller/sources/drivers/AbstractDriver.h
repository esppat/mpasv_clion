/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   AbstractDriver.h
 * Author: esppat
 *
 * Created on December 17, 2019, 10:57 PM
 */

#pragma once

#include "tools/miscel/ThreadedTask.h"
#include "tools/miscel/Logger.h"
#include "tools/miscel/Tools.h"
#include "tools/miscel/Exception.h"
#include "tools/data/DataManager.h"

#include "controller/simulation/SimulationController.h"

#include <string>

namespace mpasv::drivers
{
	class AbstractDriver
		: public enable_shared_from_this<AbstractDriver>
	{
	public:

		AbstractDriver(const std::string name, const std::shared_ptr<controller::simulation::SimulationController> simulationController);

		AbstractDriver(const AbstractDriver &) = delete;

		virtual ~AbstractDriver() = default;

		AbstractDriver & operator=(const AbstractDriver &) = delete;

		std::string name() const
		{
			return m_name;
		}

		void pull()
		{
			CHECK_TRUE(canPull());
			pullImpl();
		}

		void push()
		{
			CHECK_TRUE(canPush());
			pushImpl();
		}

	protected:

		virtual void pullImpl()
		{
			THROW_ALWAYS("Internal error: must be redefined");
		}

		virtual void pushImpl()
		{
			THROW_ALWAYS("Internal error: must be redefined");
		}

		virtual bool canPull() const
		{
			return false;
		}

		virtual bool canPush() const
		{
			return false;
		}

		SimulationController & simulationController()
		{
			CHECK_NOT_NULLPTR(m_simulationController);
			return *m_simulationController;
		}

	protected:

		const bool m_simulationMode;

	private:
	private:

		const std::string                                                   m_name;
		const std::shared_ptr<controller::simulation::SimulationController> m_simulationController;
	};
}
