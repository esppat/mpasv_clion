/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   VirtualCaptain.cpp
 * Author: esppat
 *
 * Created on 18 novembre 2018, 14:07
 */

#include "VirtualCaptain.h"

#include "tools/miscel/Logger.h"
#include "tools/data/DataManager.h"

#include "controller/route/RouteController.h"
#include "controller/route/RouteControllerGoal.h"
#include "controller/route/RouteControllerGoalFollowRoute.h"

#include "controller/simulation/SimulationController_ReactPhysics3d.h"

#include "controller/communication/CommunicationController.h"

#include "drivers/DriverManager.h"
#include "drivers/DriverFactory.h"
#include "drivers/actuators/brushless_controller/BrushlessController.h"
#include "drivers/actuators/pwm_servo/PWMServo.h"
#include "drivers/sensors/GPS/GPSSensor.h"
#include "drivers/sensors/GPS/GPSSensorFusion.h"
#include "drivers/sensors/AIS/AISSensor.h"
#include "drivers/sensors/IMU/IMUSensor.h"
#include "drivers/sensors/water/WaterSpeedSensor.h"
#include "drivers/sensors/wind/WindSensor.h"
#include "drivers/sensors/ampere/AmperageSensor.h"
#include "drivers/sensors/voltage/VoltageSensor.h"
#include "drivers/sensors/power/PowerSensor.h"

#include "controller/situation/GlobalSituationController.h"

#include <memory>

namespace mpasv::virtual_captain
{

	VirtualCaptain::VirtualCaptain()
		: tools::ThreadedTask("VirtualCaptain")
	{
		LOG_DEBUG("");
	}

	VirtualCaptain::~VirtualCaptain()
	{
		LOG_ERR("------------------- DELETING VIRTUAL CAPTAIN -------------------");
	}

	void VirtualCaptain::alert(const std::shared_ptr<const virtual_captain::Alert> & /*alert*/)
	{
		// TODO
	}

	bool VirtualCaptain::setup() noexcept
	{
		bool result = false;

		try
		{
			m_communicationController = std::make_shared<controller::communication::CommunicationController>();
			addPriorTask(m_communicationController);

			m_driverManager = std::make_shared<drivers::DriverManager>();

			if (DM()->getConstantValue<bool>("Param.VirtualCaptain.Simulation.Do simulation (bool)"))
			{
				m_simulationController = std::make_shared<controller::simulation::SimulationController_ReactPhysics3d>();
				m_simulationController->setDriverManager(m_driverManager);
				addPriorTask(m_simulationController);
			}

			m_driverFactory = std::make_shared<drivers::DriverFactory>(m_simulationController);

			initializeDrivers();

			m_routeController = std::make_shared<controller::route::RouteController>();
			m_routeController->setDriverManager(m_driverManager);
			addPriorTask(m_routeController);

			// TODO

			result = ThreadedTask::setup();
		}
		catch (...)
		{
			LOG_ERR("Exception caught");
		}

		return result;
	}

	void VirtualCaptain::initialize() noexcept
	{
		ThreadedTask::initialize();

		// TODO

		auto goal = std::make_shared<controller::route::RouteControllerGoalFollowRoute>();

		goal->loadFromFile(DM()->getConstantValue<std::string>("Param.VirtualCaptain.Simulation.Route"));

		m_routeController->pushGoal(goal);

		if (DM()->getConstantValue<bool>("Param.VirtualCaptain.Simulation.Do simulation (bool)"))
		{
			auto startGeoPos = goal->route().getWaypoint(0).pos();
			startGeoPos =
				startGeoPos.forwardTo(DM()->getConstantValue<degree_t>("Param.VirtualCaptain.Simulation.Start azymuth decal (deg)"), DM()->getConstantValue<
					meter_t>("Param.VirtualCaptain.Simulation.Start distance decal (m)"));
			m_simulationController->setStartPoint(startGeoPos, DM()->getConstantValue<degree_t>("Param.VirtualCaptain.Simulation.Start bearing (deg)"));
		}
	}

	void VirtualCaptain::run(const second_t /*tickDuration*/) noexcept
	{
		// TODO
	}

	void VirtualCaptain::finalize() noexcept
	{
		// TODO
		m_routeController.reset();
		m_simulationController.reset();
		m_driverFactory.reset();
		m_driverManager.reset();
		m_communicationController.reset();

		ThreadedTask::finalize();
	}

	void VirtualCaptain::initializeDrivers()
	{
		// IMU

		auto IMUSensor = m_driverFactory->spawnDriver<drivers::IMU::IMUSensor>("IMU 1", m_simulationController);
		// TODO init sensor
		m_driverManager->addDriver(IMUSensor);

		// GPS

		auto GPSSensor = m_driverFactory->spawnDriver<drivers::GPS::GPSSensor>("GPS 1", m_simulationController);
		// TODO init sensor
		m_driverManager->addDriver(GPSSensor);

		GPSSensor = m_driverFactory->spawnDriver<drivers::GPS::GPSSensor>("GPS 2", m_simulationController);
		// TODO init sensor
		m_driverManager->addDriver(GPSSensor);

		auto GPSSensorFusion = m_driverFactory->spawnDriver<drivers::GPS::GPSSensorFusion>("GPS", m_simulationController);
		GPSSensorFusion->setGPSSensors({"GPS 1", "GPS 2"});
		m_driverManager->addDriver(GPSSensorFusion);

		// AIS

		auto AISSensor = m_driverFactory->spawnDriver<drivers::AIS::AISSensor>("AIS 1", m_simulationController);
		// TODO init sensor
		m_driverManager->addDriver(AISSensor);

		// water

		auto waterSpeedSensor = m_driverFactory->spawnDriver<drivers::water::WaterSpeedSensor>("Lock 1", m_simulationController);
		// TODO init sensor
		m_driverManager->addDriver(waterSpeedSensor);

		// wind

		auto windSpeedSensor = m_driverFactory->spawnDriver<drivers::wind::WindSensor>("Wind 1", m_simulationController);
		// TODO init sensor
		m_driverManager->addDriver(windSpeedSensor);

		// Helm servo

		auto helmServo = m_driverFactory->spawnDriver<drivers::pwm_servo::PWMServo>("Helm 1", m_simulationController);
		helmServo->setStroke(DM()->getConstantValue<degree_t>("Param.HelmController.Maximum helm angle (deg)"));
		helmServo->setAngularVelocity(DM()->getConstantValue<degrees_per_second_t>("Param.HelmController.Angular velocity (dps)"));
		// TODO init actuator
		m_driverManager->addDriver(helmServo);

		if (DM()->getConstantValue<bool>("Param.Physical description.Has two hulls (bool)"))
		{
			helmServo = m_driverFactory->spawnDriver<drivers::pwm_servo::PWMServo>("Helm 2", m_simulationController);
			helmServo->setStroke(DM()->getConstantValue<degree_t>("Param.HelmController.Maximum helm angle (deg)"));
			helmServo->setAngularVelocity(DM()->getConstantValue<degrees_per_second_t>("Param.HelmController.Angular velocity (dps)"));
			// TODO init actuator
			m_driverManager->addDriver(helmServo);
		}

		// Engine brushless controller

		auto brushlessControllerEngine1 = m_driverFactory->spawnDriver<drivers::brushless_controller::BrushlessController>("Engine 1", m_simulationController);
		// TODO init actuator
		m_driverManager->addDriver(brushlessControllerEngine1);

		if (DM()->getConstantValue<bool>("Param.Physical description.Has two hulls (bool)"))
		{
			// Engine brushless controller
			auto brushlessControllerEngine2 =
				     m_driverFactory->spawnDriver<drivers::brushless_controller::BrushlessController>("Engine 2", m_simulationController);
			// TODO init actuator
			m_driverManager->addDriver(brushlessControllerEngine2);
		}

		// solar panel

		auto solarPanelAmperageSensor1 = m_driverFactory->spawnDriver<drivers::amperage::AmperageSensor>("Solar panel 1", m_simulationController);
		// TODO init sensor
		m_driverManager->addDriver(solarPanelAmperageSensor1);

		auto solarPanelAmperageSensor2 = m_driverFactory->spawnDriver<drivers::amperage::AmperageSensor>("Solar panel 2", m_simulationController);
		// TODO init sensor
		m_driverManager->addDriver(solarPanelAmperageSensor2);

		auto solarPanelVoltageSensor1 = m_driverFactory->spawnDriver<drivers::voltage::VoltageSensor>("Solar panel 1", m_simulationController);
		// TODO init sensor
		m_driverManager->addDriver(solarPanelVoltageSensor1);

		auto solarPanelVoltageSensor2 = m_driverFactory->spawnDriver<drivers::voltage::VoltageSensor>("Solar panel 2", m_simulationController);
		// TODO init sensor
		m_driverManager->addDriver(solarPanelVoltageSensor2);

		auto solarPanelPowerSensor1 = m_driverFactory->spawnDriver<drivers::power::PowerSensor>("Solar panel 1", m_simulationController);
		solarPanelPowerSensor1->setSensors(solarPanelAmperageSensor2, solarPanelVoltageSensor2);
		m_driverManager->addDriver(solarPanelPowerSensor1);

		auto solarPanelPowerSensor2 = m_driverFactory->spawnDriver<drivers::power::PowerSensor>("Solar panel 2", m_simulationController);
		solarPanelPowerSensor2->setSensors(solarPanelAmperageSensor2, solarPanelVoltageSensor2);
		m_driverManager->addDriver(solarPanelPowerSensor2);

		// wind generator

		auto windTurbineAmperageSensor = m_driverFactory->spawnDriver<drivers::amperage::AmperageSensor>("Wind turbine", m_simulationController);
		// TODO init sensor
		m_driverManager->addDriver(windTurbineAmperageSensor);

		auto windTurbineVoltageSensor = m_driverFactory->spawnDriver<drivers::voltage::VoltageSensor>("Wind turbine", m_simulationController);
		// TODO init sensor
		m_driverManager->addDriver(windTurbineVoltageSensor);

		auto windTurbinePowerSensor = m_driverFactory->spawnDriver<drivers::power::PowerSensor>("Wind turbine", m_simulationController);
		windTurbinePowerSensor->setSensors(windTurbineAmperageSensor, windTurbineVoltageSensor);
		m_driverManager->addDriver(windTurbinePowerSensor);

		// battery

		auto batteryAmperageSensor = m_driverFactory->spawnDriver<drivers::amperage::AmperageSensor>("Battery", m_simulationController);
		// TODO init sensor
		m_driverManager->addDriver(batteryAmperageSensor);

		auto batteryVoltageSensor = m_driverFactory->spawnDriver<drivers::voltage::VoltageSensor>("Battery", m_simulationController);
		// TODO init sensor
		m_driverManager->addDriver(batteryVoltageSensor);

		auto batteryPowerSensor = m_driverFactory->spawnDriver<drivers::power::PowerSensor>("Battery", m_simulationController);
		batteryPowerSensor->setSensors(batteryAmperageSensor, batteryVoltageSensor);
		m_driverManager->addDriver(batteryPowerSensor);

		// computer

		auto computerAmperageSensor = m_driverFactory->spawnDriver<drivers::amperage::AmperageSensor>("Computer", m_simulationController);
		// TODO init sensor
		m_driverManager->addDriver(computerAmperageSensor);

		auto computerVoltageSensor = m_driverFactory->spawnDriver<drivers::voltage::VoltageSensor>("Computer", m_simulationController);
		// TODO init sensor
		m_driverManager->addDriver(computerVoltageSensor);

		auto computerPowerSensor = m_driverFactory->spawnDriver<drivers::power::PowerSensor>("Computer", m_simulationController);
		computerPowerSensor->setSensors(computerAmperageSensor, computerVoltageSensor);
		m_driverManager->addDriver(computerPowerSensor);

		// engines

		auto engineAmperageSensor = m_driverFactory->spawnDriver<drivers::amperage::AmperageSensor>("Engine 1", m_simulationController);
		// TODO init sensor
		m_driverManager->addDriver(engineAmperageSensor);

		auto engineVoltageSensor = m_driverFactory->spawnDriver<drivers::voltage::VoltageSensor>("Engine 1", m_simulationController);
		// TODO init sensor
		m_driverManager->addDriver(engineVoltageSensor);

		auto enginePowerSensor = m_driverFactory->spawnDriver<drivers::power::PowerSensor>("Engine 1", m_simulationController);
		enginePowerSensor->setSensors(engineAmperageSensor, engineVoltageSensor);
		m_driverManager->addDriver(enginePowerSensor);

		if (DM()->getConstantValue<bool>("Param.Physical description.Has two hulls (bool)"))
		{
			engineAmperageSensor = m_driverFactory->spawnDriver<drivers::amperage::AmperageSensor>("Engine 2", m_simulationController);
			// TODO init sensor
			m_driverManager->addDriver(engineAmperageSensor);

			engineVoltageSensor = m_driverFactory->spawnDriver<drivers::voltage::VoltageSensor>("Engine 2", m_simulationController);
			// TODO init sensor
			m_driverManager->addDriver(engineVoltageSensor);

			enginePowerSensor = m_driverFactory->spawnDriver<drivers::power::PowerSensor>("Engine 2", m_simulationController);
			enginePowerSensor->setSensors(engineAmperageSensor, engineVoltageSensor);
			m_driverManager->addDriver(enginePowerSensor);
		}
	}
} // mpasv
