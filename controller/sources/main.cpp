#include "tools/miscel/Logger.h"
#include "tools/miscel/Tools.h"
#include "tools/miscel/Exception.h"
#include "tools/parameters/ParameterManager.h"
#include "tools/physics/Units.h"
#include "tools/physics/SClock.h"

#include "virtual_captain/VirtualCaptain.h"

#include <thread>

#include <csignal>

static bool sm_SIGIXXX_received = false;

void receive_SIGINT(int /*param*/)
{
	sm_SIGIXXX_received = true;
}

void receive_SIGABRT(int /*param*/)
{
	sm_SIGIXXX_received = true;
}

void receive_SIGPIPE(int /*param*/)
{
}

int main()
{
	// install signal handlers
	signal(SIGINT, receive_SIGINT);
	signal(SIGABRT, receive_SIGABRT);
	signal(SIGPIPE, receive_SIGPIPE);

	// install logger
	//tools::logger.open("/media/USBKEY/mp-asv");
	mpasv::tools::logger.open("dyndata/logs/mp-asv");
	mpasv::tools::logger.set_cout_level(mpasv::tools::LoggerLevel::Debug);
	//mpasv::tools::logger.hideHeader();

	// install data logger
	//tools::data_logger.open("/media/USBKEY/mp-asv-data");
	//mpasv::tools::dataLogger.open("dyndata/logs/mp-asv-data");
	//mpasv::tools::dataLogger.set_minimal_mode();

	mpasv::tools::parameters::ParameterManager::get();
	mpasv::tools::parameters::ParameterManager::get()->save(); // for auto-formatting

	//	testClientServer();

	// start Virtual Captain
	std::shared_ptr<mpasv::virtual_captain::VirtualCaptain> virtualCaptain = std::make_shared<mpasv::virtual_captain::VirtualCaptain>();
	if (virtualCaptain->start())
	{
		second_t delay = 1_hr;
		auto     start = theClock.now();
		LOG_INFO("Working for ", delay);
		while (theClock.now() - start < delay && !sm_SIGIXXX_received)
		{
			mpasv::tools::physics::sleepFor(1_ms);
		}

		LOG_INFO("Working done: stopping ...");

		virtualCaptain->stop();
	}
	return 0;
}
