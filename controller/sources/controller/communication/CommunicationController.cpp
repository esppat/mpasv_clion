/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   CommunicationController.cpp
 * Author: esppat
 *
 * Created on January 16, 2020, 8:54 PM
 */

#include "controller/communication/CommunicationController.h"
#include "controller/route/RouteControllerGoal.h"
#include "controller/situation/GlobalSituationController.h"

#include "tools/miscel/Exception.h"
#include "tools/miscel/Logger.h"
#include "tools/parameters/ParameterManager.h"
#include "tools/gps/GeoPos.h"

#include "communication/MessageDefinition.h"

namespace mpasv::controller::communication
{

	CommunicationController::CommunicationController()
		: Controller("CommunicationController"), m_client("CommunicationController")
	{
	}

	void CommunicationController::onAsyncConnectFailure(const mqtt::token & asyncActionToken)
	{
	}

	void CommunicationController::onAsyncConnectSuccess(const mqtt::token & asyncActionToken)
	{
	}

	void CommunicationController::onAsyncDisconnectFailure(const mqtt::token & asyncActionToken)
	{
	}

	void CommunicationController::onAsyncDisconnectSuccess(const mqtt::token & asyncActionToken)
	{
	}

	void CommunicationController::onAsyncPublishFailure(const mqtt::token & asyncActionToken)
	{
	}

	void CommunicationController::onAsyncPublishSuccess(const mqtt::token & asyncActionToken)
	{
	}

	void CommunicationController::onAsyncSubscribeFailure(const mqtt::token & asyncActionToken)
	{
	}

	void CommunicationController::onAsyncSubscribeSuccess(const mqtt::token & asyncActionToken)
	{
	}

	void CommunicationController::onAsyncUnsubscribeFailure(const mqtt::token & asyncActionToken)
	{
	}

	void CommunicationController::onAsyncUnsubscribeSuccess(const mqtt::token & asyncActionToken)
	{
	}

	void CommunicationController::onMessageReceived(const TopicName & topicName, const char * buffer, const size_t bufferSize)
	{
#define DEFINE_TOPIC_MANAGEMENT(Name) \
        if (topicName == Topic_##Name) \
        { \
            if (bufferSize != sizeof(Data_##Name)) \
            { \
                LOG_ERR("Bad buffer size received for Data_", #Name); \
            } \
            else \
            { \
                Data_##Name * data = (Data_##Name *)buffer; \
                manage_##Name(data); \
            } \
            return; \
        }

		DEFINE_TOPIC_MANAGEMENT(Global_CaptainOrder)
	}

	void CommunicationController::manage_Global_CaptainOrder(Data_Global_CaptainOrder * data)
	{
		LOG_DEBUG("Received order ", (uint32_t)data->order);
	}

	void CommunicationController::publishCurrentRoute() const
	{
		if (DM()->valueExists<size_t>("Route.Waypoint number") &&
		    DM()->valueIsAvailable<size_t>("Route.Waypoint number") &&
		    !DM()->isArchived("Route.Waypoint number"))
		{
			Data_Routing_Route_BasicData data;
			data.totalWaypointCount = DM()->getValue<size_t>("Route.Waypoint number");
			std::string name = DM()->getValue<std::string>("Route.Name");
			std::strncpy(data.routeName, name.c_str(), name.length());

			if (!m_client.asyncPublish(Topic_Routing_Route_BasicData, data))
			{
				LOG_ERR("Cannot publish on topic '", Topic_Routing_Route_BasicData, "'");
			}
			else
			{
				LOG_DEBUG("Published topic '", Topic_Routing_Route_BasicData, "'");
				DM()->setArchived("Route.Waypoint number");
				DM()->setArchived("Route.Name");
			}
		}
	}

	bool CommunicationController::setup() noexcept
	{
		bool result            = false;

		m_client.setCallback(shared_from_this());

		std::string serverAddress {"tcp://192.168.1.25"}; // TODO add this in parameters
		uint32_t    serverPort = 1883;
		std::string persistenceDirectory {"./dyndata/persistence"};

		if (m_client.asyncConnect(serverAddress, serverPort, persistenceDirectory))
		{
			m_client.waitForAsyncConnectionComplete();

			LOCK(mutex());

			if (m_client.isConnected())
			{
				// TODO define topics to subscribe and to publish

				if (m_client.registerPublishTopics({Topic_Routing_Route_BasicData
				                                    , Topic_Routing_Waypoint_Description
				                                    , Topic_Situation_Gps_Full
				                                    , Topic_Situation_Wind_Full
				                                    , Topic_Situation_Water_RelativeSpeed
				                                    , Topic_Situation_IMU_Compass
				                                    , Topic_Global_CaptainOrder}))
				{
					const std::vector<TopicName> subscribeTopicNames {Topic_Global_CaptainOrder};

					if (m_client.registerSubscribeTopics(subscribeTopicNames))
					{
						result = true;

						for (auto topicName : subscribeTopicNames)
						{
							if (!m_client.asyncSubscribe(topicName))
							{
								result = false;
								break;
							}
						}
					}
				}
			}
		}

		return result;
	}

	void CommunicationController::initialize() noexcept
	{
		Controller::initialize();

		// debug
		if (m_client.isConnected())
		{
			Data_Global_CaptainOrder order;
			order.order = CaptainOrder::InternalCheck;

			if (!m_client.asyncPublish(Topic_Global_CaptainOrder, order))
			{
				LOG_ERR("Cannot publish");
			}
		}
	}

	void CommunicationController::run(const second_t /*tickDuration*/) noexcept
	{
		LOCK(mutex());

		if (m_client.isConnected())
		{
			// TODO publish what needs to be published

			// Routing_Route_BasicData
			publishCurrentRoute();
		}
	}

	void CommunicationController::finalize() noexcept
	{
		{
			LOCK(mutex());

			if (m_client.isConnected())
			{
				if (m_client.asyncDisconnect())
				{
					m_client.waitForAsyncDisconnectComplete();
				}
			}

			// Whatever the state of the client, we consider it as disconnected
		}

		Controller::finalize();
	}
}
