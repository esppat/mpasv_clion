//
// Created by esppat on 26/04/2020.
//

#include "PeriodicDataPublisher.h"

namespace mpasv::controller::communication
{

	PeriodicDataPublisher::PeriodicDataPublisher()
	{
	}

	PeriodicDataPublisher::~PeriodicDataPublisher()
	{
	}

	void PeriodicDataPublisher::init(const nanosecond_t period)
	{
		m_pulse.init(period, false);
	}

	void PeriodicDataPublisher::start()
	{
		m_pulse.setActive(true);
	}

	void PeriodicDataPublisher::stop()
	{
		m_pulse.setActive(false);
	}

	void PeriodicDataPublisher::check()
	{
		if (m_pulse.shouldPulse())
		{
			pulse();
		}
	}
}
