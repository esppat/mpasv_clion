/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   CommunicationController.h
 * Author: esppat
 *
 * Created on January 16, 2020, 8:54 PM
 */

#pragma once

#include "controller/Controller.h"
#include "controller/communication/PeriodicDataPublisher.h"

#include "tools/physics/Pulse.h"
#include "tools/miscel/Exception.h"
#include "tools/communication/Client.h"
#include "tools/data/DataManager.h"

#include "communication/MessageDefinition.h"

namespace mpasv::controller::communication
{

	class CommunicationController
		: public std::enable_shared_from_this<CommunicationController>
		  , public Controller
		  , public IClientCallback
	{
	public:

		CommunicationController();

		virtual ~CommunicationController() = default;

		CommunicationController & operator=(const CommunicationController &) = delete;

	protected:

		// IClientCallback >>>>>>
		virtual void onAsyncConnectFailure(const mqtt::token & asyncActionToken) override;
		virtual void onAsyncConnectSuccess(const mqtt::token & asyncActionToken) override;

		virtual void onAsyncDisconnectFailure(const mqtt::token & asyncActionToken) override;
		virtual void onAsyncDisconnectSuccess(const mqtt::token & asyncActionToken) override;

		virtual void onAsyncPublishFailure(const mqtt::token & asyncActionToken) override;
		virtual void onAsyncPublishSuccess(const mqtt::token & asyncActionToken) override;

		virtual void onAsyncSubscribeFailure(const mqtt::token & asyncActionToken) override;
		virtual void onAsyncSubscribeSuccess(const mqtt::token & asyncActionToken) override;

		virtual void onAsyncUnsubscribeFailure(const mqtt::token & asyncActionToken) override;
		virtual void onAsyncUnsubscribeSuccess(const mqtt::token & asyncActionToken) override;

		virtual void onMessageReceived(const TopicName & topicName, const char * buffer, const size_t bufferSize) override;
		// IClientCallback <<<<<<

	protected:
	private:

		void manage_Global_CaptainOrder(Data_Global_CaptainOrder * data);

		void publishCurrentRoute() const;

		virtual bool setup() noexcept override;

		virtual void initialize() noexcept override;

		virtual void run(const second_t tickDuration) noexcept override;

		virtual void finalize() noexcept override;

	private:

		Client m_client;
	};
}

using namespace mpasv::controller::communication;
