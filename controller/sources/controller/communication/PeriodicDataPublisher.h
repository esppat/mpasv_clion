//
// Created by esppat on 26/04/2020.
//

#pragma once

#include "tools/physics/Pulse.h"

namespace mpasv::controller::communication
{

	class PeriodicDataPublisher
	{
	public:

		PeriodicDataPublisher();
		virtual ~PeriodicDataPublisher();

		PeriodicDataPublisher & operator=(const PeriodicDataPublisher &) = delete;
		PeriodicDataPublisher & operator=(PeriodicDataPublisher &&) = delete;

		void init(const nanosecond_t period);

		void start();
		void stop();

		void check();

	public:
	protected:

		virtual void pulse() = 0;

	protected:
	private:
	private:

		Pulse m_pulse;
	};
}

using namespace mpasv::controller::communication;
