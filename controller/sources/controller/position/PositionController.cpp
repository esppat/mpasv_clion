/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   PositionController.cpp
 * Author: esppat
 *
 * Created on January 16, 2020, 8:54 PM
 */

#include "PositionController.h"

#include "tools/miscel/Exception.h"
#include "tools/miscel/Logger.h"
#include "tools/parameters/ParameterManager.h"
#include "tools/physics/Units.h"
#include "tools/gps/GeoPos.h"

#include "drivers/DriverManager.h"
#include "drivers/actuators/brushless_controller/BrushlessController.h"
#include "drivers/actuators/pwm_servo/PWMServo.h"

namespace mpasv::controller::position
{

	PositionController::PositionController()
		: Controller("PositionController"), m_hasTwoHulls(param<bool>("Physical description.Has two hulls (bool)"))
	{
	}

	PositionController::~PositionController() = default;

	void PositionController::setDriverManager(const std::shared_ptr<drivers::DriverManager> & driverManager)
	{
		CHECK_NOT_NULLPTR(driverManager);
		m_driverManager = driverManager;
	}

	bool PositionController::setup() noexcept
	{
		bool result = false;

		try
		{
			m_brushlessController_engine1 = m_driverManager->getActuator<drivers::brushless_controller::BrushlessController>("Engine 1");
			m_pwmServo_helm1              = m_driverManager->getActuator<drivers::pwm_servo::PWMServo>("Helm 1");

			if (m_hasTwoHulls)
			{
				m_brushlessController_engine2 = m_driverManager->getActuator<drivers::brushless_controller::BrushlessController>("Engine 2");
				m_pwmServo_helm2              = m_driverManager->getActuator<drivers::pwm_servo::PWMServo>("Helm 2");
			}

			// TODO

			result = Controller::setup();
		}
		catch (std::exception & e)
		{
			LOG_DEBUG("Caught exception: ", e.what());
		}

		return result;
	}

	void PositionController::initialize() noexcept
	{
		Controller::initialize();

		// TODO
	}

	void PositionController::run(const second_t /*tickDuration*/) noexcept
	{
		calculateThrottle();
		calculateHelmAngle();

		updateActuators();
	}

	void PositionController::finalize() noexcept
	{
		m_brushlessController_engine1.reset();
		m_pwmServo_helm1.reset();

		if (m_hasTwoHulls)
		{
			m_brushlessController_engine2.reset();
			m_pwmServo_helm2.reset();
		}

		// TODO

		Controller::finalize();
	}

	void PositionController::calculateThrottle()
	{
		if (DM()->valueIsAvailable<knot_t>("Calculated.Target GPS speed") &&
		    DM()->valueIsAvailable<knot_t>("GPS 1.Sensor.GPS.Speed") &&
		    DM()->valueIsAvailable<bool>("Calculated.Target GPS speed forward"))
		{
			knot_t targetSpeed = DM()->getValue<knot_t>("Calculated.Target GPS speed");

			if (targetSpeed == 0.0_kts)
			{
				// TODO should be an active process, no just cut engines

				DM()->setValue<dimensionless_t>("Engine 1.Brushless controller.Requested value", 0.0);

				if (m_hasTwoHulls)
				{
					DM()->setValue<dimensionless_t>("Engine 2.Brushless controller.Requested value", 0.0);
				}
			}
			else
			{
				const knot_t speedDiff = targetSpeed - DM()->getValue<knot_t>("GPS 1.Sensor.GPS.Speed");

				dimensionless_t throttle1 = DM()->getValue<dimensionless_t>("Engine 1.Brushless controller.Requested value");
				throttle1 += (speedDiff / 50.0_kts);

				dimensionless_t throttle2;
				if (m_hasTwoHulls)
				{
					throttle2 = DM()->getValue<dimensionless_t>("Engine 2.Brushless controller.Requested value");
					throttle2 += (speedDiff / 50.0_kts);
				}

				// TODO limit to N percent (as a param)

				if (DM()->getValue<bool>("Calculated.Target GPS speed forward"))
				{
					mpasv::tools::setInLimit(throttle1, 0.0, +1.0);
					if (m_hasTwoHulls)
					{
						mpasv::tools::setInLimit(throttle2, 0.0, +1.0);
					}
				}
				else
				{
					mpasv::tools::setInLimit(throttle1, -1.0, 0.0);
					if (m_hasTwoHulls)
					{
						mpasv::tools::setInLimit(throttle2, -1.0, 0.0);
					}
				}

				DM()->setValue("Engine 1.Brushless controller.Requested value", throttle1);
				if (m_hasTwoHulls)
				{
					DM()->setValue("Engine 2.Brushless controller.Requested value", throttle2);
				}
			}
		}
		else
		{
			DM()->setValue("Engine 1.Brushless controller.Requested value", (dimensionless_t)0.0);
			DM()->setValue("Engine 2.Brushless controller.Requested value", (dimensionless_t)0.0);
		}
	}

	template<typename T> bool sign(const T & t)
	{
		return t >= T(0);
	}

	void PositionController::calculateHelmAngle()
	{
		if (DM()->valueIsAvailable<degree_t>("Calculated.Target GPS bearing") && DM()->valueIsAvailable<degree_t>("GPS 1.Sensor.GPS.Bearing"))
		{
			degree_t targetBearing = DM()->getValue<degree_t>("Calculated.Target GPS bearing");

			degree_t bearingToAdd = targetBearing - DM()->getValue<degree_t>("GPS 1.Sensor.GPS.Bearing");
			bearingToAdd = normalize180(bearingToAdd);

			degree_t        servoStroke  = m_pwmServo_helm1->stroke();
			dimensionless_t helmPosition = bearingToAdd / servoStroke;

			mpasv::tools::setInLimit(helmPosition, -1.0, +1.0);

			// TODO limit to N percent (as a param)
			// TODO use gain (as a param)

			DM()->setValue<dimensionless_t>("Helm 1.Servo.Requested value", helmPosition);
			if (m_hasTwoHulls)
			{
				DM()->setValue<dimensionless_t>("Helm 2.Servo.Requested value", helmPosition);
			}
		}
		else
		{
			DM()->setValue<dimensionless_t>("Helm 1.Servo.Requested value", (dimensionless_t)0.0);
			if (m_hasTwoHulls)
			{
				DM()->setValue<dimensionless_t>("Helm 2.Servo.Requested value", (dimensionless_t)0.0);
			}
		}
	}

	void PositionController::updateActuators()
	{
		m_brushlessController_engine1->push();
		m_pwmServo_helm1->push();

		if (m_hasTwoHulls)
		{
			m_brushlessController_engine2->push();
			m_pwmServo_helm2->push();
		}
	}
}
