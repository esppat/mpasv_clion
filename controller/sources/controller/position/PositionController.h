/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   PositionController.h
 * Author: esppat
 *
 * Created on January 16, 2020, 8:54 PM
 */

#pragma once

#include "controller/Controller.h"

#include "tools/physics/ValueHistory.h"

namespace mpasv
{
	namespace drivers
	{
		class DriverManager;

		namespace brushless_controller
		{
			class BrushlessController;
		}

		namespace pwm_servo
		{
			class PWMServo;
		}
	}

	namespace controller::position
	{

		class PositionController
			: public controller::Controller
		{
		public:

			PositionController();

			virtual ~PositionController();

			void setDriverManager(const std::shared_ptr<drivers::DriverManager> & driverManager);

		protected:
		protected:
		private:

			virtual bool setup() noexcept override;

			virtual void initialize() noexcept override;

			virtual void run(const second_t tickDuration) noexcept override;

			virtual void finalize() noexcept override;

			void calculateThrottle();

			void calculateHelmAngle();

			void updateActuators();

		private:

			std::shared_ptr<drivers::DriverManager> m_driverManager;

			std::shared_ptr<drivers::brushless_controller::BrushlessController> m_brushlessController_engine1;
			std::shared_ptr<drivers::brushless_controller::BrushlessController> m_brushlessController_engine2;

			std::shared_ptr<drivers::pwm_servo::PWMServo> m_pwmServo_helm1;
			std::shared_ptr<drivers::pwm_servo::PWMServo> m_pwmServo_helm2;

			const bool m_hasTwoHulls;
		};
	}
} // mpasv
