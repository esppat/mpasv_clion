/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   SimulationController.cpp
 * Author: esppat
 *
 * Created on January 16, 2020, 8:54 PM
 */

#include "SimulationController.h"

#include "drivers/actuators/pwm_servo/PWMServo.h"

#include "tools/miscel/Exception.h"
#include "tools/miscel/Logger.h"
#include "tools/parameters/ParameterManager.h"
#include "drivers/DriverManager.h"

namespace mpasv::controller::simulation
{

	SimulationController::SimulationController(const std::string & name)
		: Controller(name)
		  , m_oneHullWeight(param<gram_t, double>("Physical description.Hull.Mass (kg)"))
		  , m_windEffectRatio(param<dimensionless_t, double>("Physical simulator.Wind effect ratio (pct)"))
		  , m_currentEffectRatio(param<dimensionless_t, double>("Physical simulator.Current effect ratio (pct)"))
		  , m_hasTwoHulls(param<bool>("Physical description.Has two hulls (bool)"))
	{
	}

	void SimulationController::setDriverManager(const std::shared_ptr<drivers::DriverManager> & driverManager)
	{
		CHECK_NOT_NULLPTR(driverManager);
		m_driverManager = driverManager;
	}

	void SimulationController::setStartPoint(const tools::geo::GeoPos & geoPos, const degree_t bearing)
	{
		CHECK_TRUE(geoPos.isValid());
		CHECK_VALID_BEARING(bearing);

		setGeoPos(geoPos);
		calculateCurrentParameters();

		setGPSSpeed(m_currentSpeed);
		setGPSBearing(trigoToBearing(m_currentBearing_trigo));

		setCompass(bearing);
	}

	bool SimulationController::setup() noexcept
	{
		bool result = false;

		try
		{
			if (m_hasTwoHulls)
			{
				m_leftPropeller.reset(new SimuPropeller(shared_from_this(), {"Propeller", "Left"}));
				m_simuElements.push_back(m_leftPropeller);

				m_rightPropeller.reset(new SimuPropeller(shared_from_this(), {"Propeller", "Right"}));
				m_simuElements.push_back(m_rightPropeller);

				m_simuElements.push_back(std::shared_ptr<SimuElement>(new SimuDaggerBoardPlane(shared_from_this(), {"Hull", "Front left"})));
				m_simuElements.push_back(std::shared_ptr<SimuElement>(new SimuDaggerBoardPlane(shared_from_this(), {"Hull", "Rear left"})));
				m_simuElements.push_back(std::shared_ptr<SimuElement>(new SimuDaggerBoardPlane(shared_from_this(), {"Hull", "Front right"})));
				m_simuElements.push_back(std::shared_ptr<SimuElement>(new SimuDaggerBoardPlane(shared_from_this(), {"Hull", "Rear right"})));

				m_simuElements.push_back(std::shared_ptr<SimuElement>(new SimuDaggerBoardPlane(shared_from_this(), {"Keel", "Left"})));
				m_simuElements.push_back(std::shared_ptr<SimuElement>(new SimuDaggerBoardPlane(shared_from_this(), {"Keel", "Right"})));

				m_leftHelm.reset(new SimuDaggerBoardPlane(shared_from_this(), {"Helm", "Left"}));
				m_simuElements.push_back(m_leftHelm);

				m_rightHelm.reset(new SimuDaggerBoardPlane(shared_from_this(), {"Helm", "Right"}));
				m_simuElements.push_back(m_rightHelm);
			}
			else
			{
				m_leftPropeller.reset(new SimuPropeller(shared_from_this(), {"Propeller", "Center"}));
				m_simuElements.push_back(m_leftPropeller);

				m_simuElements.push_back(std::shared_ptr<SimuElement>(new SimuDaggerBoardPlane(shared_from_this(), {"Hull", "Front center"})));
				m_simuElements.push_back(std::shared_ptr<SimuElement>(new SimuDaggerBoardPlane(shared_from_this(), {"Hull", "Rear center"})));

				m_simuElements.push_back(std::shared_ptr<SimuElement>(new SimuDaggerBoardPlane(shared_from_this(), {"Keel", "Center"})));

				m_leftHelm.reset(new SimuDaggerBoardPlane(shared_from_this(), {"Helm", "Center"}));
				m_simuElements.push_back(m_leftHelm);
			}

			m_simuElements.push_back(std::shared_ptr<SimuElement>(new SimuWindTurbine(shared_from_this(), {"Wind turbine", "Center"})));

			create3dBody();

			m_amperages.declareMember("Engine 1.Sensor.Amperage.Amperage", 0.0_A, DM()->getConstantValue<ampere_t>(
				"Param.Physical simulator.Engine.Amperage max (A)"));
			m_voltages.declareMember("Engine 1.Sensor.Voltage.Voltage", 0.0_V, DM()->getConstantValue<volt_t>("Param.Physical simulator.Engine.Voltage (V)"));

			m_amperages.declareMember("Engine 2.Sensor.Amperage.Amperage", 0.0_A, DM()->getConstantValue<ampere_t>(
				"Param.Physical simulator.Engine.Amperage max (A)"));
			m_voltages.declareMember("Engine 2.Sensor.Voltage.Voltage", 0.0_V, DM()->getConstantValue<volt_t>("Param.Physical simulator.Engine.Voltage (V)"));

			m_amperages.declareMember("Computer.Sensor.Amperage.Amperage", 0.0_A, DM()->getConstantValue<ampere_t>(
				"Param.Physical simulator.Computer.Amperage (A)"));
			m_voltages.declareMember("Computer.Sensor.Voltage.Voltage", 0.0_V, DM()->getConstantValue<volt_t>("Param.Physical simulator.Computer.Voltage (V)"));

			m_amperages.declareMember("Battery.Sensor.Amperage.Amperage", -DM()->getConstantValue<ampere_t>(
				"Param.Physical simulator.Battery.Amperage charge max (A)"), DM()->getConstantValue<ampere_t>(
				"Param.Physical simulator.Battery.Amperage discharge max (A)"));
			m_voltages.declareMember("Battery.Sensor.Voltage.Voltage", 0.0_V, DM()->getConstantValue<volt_t>("Param.Physical simulator.Battery.Voltage (V)"));

			m_amperages.declareMember("Solar panel 1.Sensor.Amperage.Amperage", 0.0_A, DM()->getConstantValue<ampere_t>(
				"Param.Physical simulator.Solar panel.Amperage max (A)"));
			m_voltages.declareMember("Solar panel 1.Sensor.Voltage.Voltage", 0.0_V, DM()->getConstantValue<volt_t>(
				"Param.Physical simulator.Solar panel.Voltage max (V)"));

			m_amperages.declareMember("Solar panel 2.Sensor.Amperage.Amperage", 0.0_A, DM()->getConstantValue<ampere_t>(
				"Param.Physical simulator.Solar panel.Amperage max (A)"));
			m_voltages.declareMember("Solar panel 2.Sensor.Voltage.Voltage", 0.0_V, DM()->getConstantValue<volt_t>(
				"Param.Physical simulator.Solar panel.Voltage max (V)"));

			m_amperages.declareMember("Wind turbine.Sensor.Amperage.Amperage", 0.0_A, DM()->getConstantValue<ampere_t>(
				"Param.Physical simulator.Wind turbine.Amperage max (A)"));
			m_voltages.declareMember("Wind turbine.Sensor.Voltage.Voltage", 0.0_V, DM()->getConstantValue<volt_t>(
				"Param.Physical simulator.Wind turbine.Voltage max (V)"));

			result = Controller::setup();
		}
		catch (std::exception & e)
		{
			LOG_DEBUG("Caught exception: ", e.what());
		}

		return result;
	}

	void SimulationController::initialize() noexcept
	{
		Controller::initialize();

		// TODO
	}

	void SimulationController::run(const second_t tickDuration) noexcept
	{
		calculatePowerSimulation();

		if (DM()->valueIsAvailable<bool>("GPS 1.Sensor.GPS.Has fix") && DM()->getValue<bool>("GPS 1.Sensor.GPS.Has fix"))
		{
			doCalculation(tickDuration);
		}
		else
		{
			LOG_DEBUG("Missing 'GPS 1.Sensor.GPS.Has fix\"' to calculate simulation");
		}
	}

	void SimulationController::finalize() noexcept
	{
		// TODO

		Controller::finalize();
	}

	void SimulationController::doCalculation(const second_t tickDuration)
	{
		calculateWindParameters();
		calculateCurrentParameters();

		update3dBody(tickDuration);
		updateSimuElements();

		m_relativeWindLinearVelocity = m_windLinearVelocity - m_bodyLinearVelocity;

		setRelativeWindSpeed((knot_t)(meters_per_second_t)(m_relativeWindLinearVelocity.length()));
		setRelativeWindBearing(trigoToBearing((radian_t)std::atan2(m_relativeWindLinearVelocity.y, m_relativeWindLinearVelocity.x)));

		decimal angle;
		Vector3 axis;
		m_bodyTransform.getOrientation().getRotationAngleAxis(angle, axis);

		degree_t    compass    = trigoToBearing(radian_t(angle));
		longitude_t lon        = DM()->getValue<degree_t>("GPS 1.Sensor.GPS.Lon");
		latitude_t  lat        = DM()->getValue<degree_t>("GPS 1.Sensor.GPS.Lat");
		GeoPos      newPos {lat, lon};
		degree_t    gpsBearing = DM()->getValue<degree_t>("GPS 1.Sensor.GPS.Bearing");

		Vector3 moveDuringStep     = m_bodyTransform.getPosition() - m_previousBodyTransform.getPosition();
		meter_t distanceDuringStep = meter_t(moveDuringStep.length());

		if (distanceDuringStep > 0.0_m)
		{
			gpsBearing = trigoToBearing(radian_t(std::atan2(moveDuringStep.y, moveDuringStep.x)));
			newPos     = newPos.forwardTo(gpsBearing, distanceDuringStep);
		}

		setWaterSpeed(meters_per_second_t((m_bodyLinearVelocity - m_currentLinearVelocity).x));

		setCompass(compass);
		setAccelX(meters_per_second_squared_t(m_bodyLinearAcceleration.x));
		setAccelY(meters_per_second_squared_t(m_bodyLinearAcceleration.y));
		setAccelZ(meters_per_second_squared_t(m_bodyLinearAcceleration.z));
		setGyroX(radians_per_second_t(m_bodyAngularVelocity.x));
		setGyroY(radians_per_second_t(m_bodyAngularVelocity.y));
		setGyroZ(radians_per_second_t(m_bodyAngularVelocity.z));

		setGeoPos(newPos);
		setGPSBearing(gpsBearing);
		setGPSSpeed(distanceDuringStep / tickDuration);
	}

	void SimulationController::calculateWindParameters()
	{
		m_windSpeed         = DM()->getConstantValue<knot_t>("Param.Physical simulator.Wind speed (kts)");
		m_windBearing_trigo = bearingToTrigo(DM()->getConstantValue<degree_t>("Param.Physical simulator.Wind bearing (deg)"));

		m_windLinearVelocity = Vector3(doubleFrom<meters_per_second_t>(m_windSpeed), 0, 0);
		rotate(m_windLinearVelocity, 0.0_deg, 0.0_deg, m_windBearing_trigo);
	}

	void SimulationController::calculateCurrentParameters()
	{
		m_currentSpeed         = DM()->getConstantValue<knot_t>("Param.Physical simulator.Current speed (kts)");
		m_currentBearing_trigo = bearingToTrigo(DM()->getConstantValue<degree_t>("Param.Physical simulator.Current bearing (deg)"));

		m_currentLinearVelocity = Vector3(doubleFrom<meters_per_second_t>(m_currentSpeed), 0, 0);
		rotate(m_currentLinearVelocity, 0.0_deg, 0.0_deg, m_currentBearing_trigo);
	}

	void SimulationController::calculatePowerSimulation()
	{
		watt_t consumedPower = 0.0_W;

		// Engines

		m_voltages.set("Engine 1.Sensor.Voltage.Voltage", DM()->getConstantValue<volt_t>("Param.Physical simulator.Engine.Voltage (V)"));
		m_amperages.set("Engine 1.Sensor.Amperage.Amperage", DM()->getConstantValue<ampere_t>("Param.Physical simulator.Engine.Amperage max (A)") *
		                                                     DM()->getValue<dimensionless_t>("Engine 1.Brushless controller.Current value"));
		consumedPower += m_voltages.get("Engine 1.Sensor.Voltage.Voltage") * m_amperages.get("Engine 1.Sensor.Amperage.Amperage");

		if (m_hasTwoHulls)
		{
			m_voltages.set("Engine 2.Sensor.Voltage.Voltage", DM()->getConstantValue<volt_t>("Param.Physical simulator.Engine.Voltage (V)"));
			m_amperages.set("Engine 2.Sensor.Amperage.Amperage", DM()->getConstantValue<ampere_t>("Param.Physical simulator.Engine.Amperage max (A)") *
			                                                     DM()->getValue<dimensionless_t>("Engine 2.Brushless controller.Current value"));
			consumedPower += m_voltages.get("Engine 2.Sensor.Voltage.Voltage") * m_amperages.get("Engine 2.Sensor.Amperage.Amperage");
		}

		// Computer

		m_voltages.set("Computer.Sensor.Voltage.Voltage", DM()->getConstantValue<volt_t>("Param.Physical simulator.Computer.Voltage (V)"));
		m_amperages.set("Computer.Sensor.Amperage.Amperage", DM()->getConstantValue<ampere_t>("Param.Physical simulator.Computer.Amperage (A)"));
		consumedPower += m_voltages.get("Computer.Sensor.Voltage.Voltage") * m_amperages.get("Computer.Sensor.Amperage.Amperage");

		// Solar panels

		m_voltages.set("Solar panel 1.Sensor.Voltage.Voltage", DM()->getConstantValue<volt_t>("Param.Physical simulator.Solar panel.Voltage max (V)") * 0.5);
		m_amperages.set("Solar panel 1.Sensor.Amperage.Amperage", DM()->getConstantValue<ampere_t>("Param.Physical simulator.Solar panel.Amperage max (A)") *
		                                                          0.5);
		consumedPower -= m_voltages.get("Solar panel 1.Sensor.Voltage.Voltage") * m_amperages.get("Solar panel 1.Sensor.Amperage.Amperage");

		m_voltages.set("Solar panel 2.Sensor.Voltage.Voltage", DM()->getConstantValue<volt_t>("Param.Physical simulator.Solar panel.Voltage max (V)") * 0.5);
		m_amperages.set("Solar panel 2.Sensor.Amperage.Amperage", DM()->getConstantValue<ampere_t>("Param.Physical simulator.Solar panel.Amperage max (A)") *
		                                                          0.5);
		consumedPower -= m_voltages.get("Solar panel 2.Sensor.Voltage.Voltage") * m_amperages.get("Solar panel 2.Sensor.Amperage.Amperage");

		// Wind turbine

		if (hasRelativeWindSpeed())
		{
			knot_t windSpeedForCalculation = getRelativeWindSpeed();
			if (windSpeedForCalculation >= DM()->getConstantValue<knot_t>("Param.Physical simulator.Wind turbine.Relative wind speed min (kts)"))
			{
				if (windSpeedForCalculation > DM()->getConstantValue<knot_t>("Param.Physical simulator.Wind turbine.Relative wind speed max (kts)"))
				{
					windSpeedForCalculation = DM()->getConstantValue<knot_t>("Param.Physical simulator.Wind turbine.Relative wind speed max (kts)");
				}

				windSpeedForCalculation -= DM()->getConstantValue<knot_t>("Param.Physical simulator.Wind turbine.Relative wind speed min (kts)");
				knot_t maxWindSpeedForCalculation = DM()->getConstantValue<knot_t>("Param.Physical simulator.Wind turbine.Relative wind speed max (kts)") -
				                                    DM()->getConstantValue<knot_t>("Param.Physical simulator.Wind turbine.Relative wind speed min (kts)");

				dimensionless_t ratio = sqrt(windSpeedForCalculation / maxWindSpeedForCalculation);

				m_voltages.set("Wind turbine.Sensor.Voltage.Voltage", DM()->getConstantValue<volt_t>("Param.Physical simulator.Wind turbine.Voltage max (V)") *
				                                                      ratio);
				m_amperages.set("Wind turbine.Sensor.Amperage.Amperage", DM()->getConstantValue<ampere_t>(
					"Param.Physical simulator.Wind turbine.Amperage max (A)") * ratio);
				consumedPower -= m_voltages.get("Wind turbine.Sensor.Voltage.Voltage") * m_amperages.get("Wind turbine.Sensor.Amperage.Amperage");
			}
		}

		// Battery

		m_voltages.set("Battery.Sensor.Voltage.Voltage", DM()->getConstantValue<volt_t>("Param.Physical simulator.Battery.Voltage (V)"));
		ampere_t batteryAmpere = consumedPower / m_voltages.get("Battery.Sensor.Voltage.Voltage");
		m_amperages.set("Battery.Sensor.Amperage.Amperage", batteryAmpere * 1.05);
	}

	void SimulationController::updateSimuElements()
	{
		if (m_leftPropeller != nullptr)
		{
			m_leftPropeller->setThrottle(DM()->getValue<dimensionless_t>("Engine 1.Brushless controller.Current value"));
		}

		if (m_rightPropeller != nullptr)
		{
			CHECK_TRUE(m_hasTwoHulls);
			m_rightPropeller->setThrottle(DM()->getValue<dimensionless_t>("Engine 2.Brushless controller.Current value"));
		}

		if (m_leftHelm != nullptr)
		{
			m_leftHelm->setOrientation(DM()->getValue<degree_t>("Helm 1.Servo.Current angle"));
		}

		if (m_rightHelm != nullptr)
		{
			m_rightHelm->setOrientation(DM()->getValue<degree_t>("Helm 2.Servo.Current angle"));
		}
	}
}
