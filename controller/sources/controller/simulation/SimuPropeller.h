#pragma once

#include "SimuElement.h"
#include "tools/miscel/Exception.h"

namespace mpasv::controller::simulation
{

	class SimulationController;

	class SimuPropeller
		: public SimuElement
	{
	public:

		SimuPropeller(std::shared_ptr<SimulationController> simulationController, const std::list<std::string> names);

		void setThrottle(const dimensionless_t throttle)
		{
			CHECK_GE(throttle, -1.0);
			CHECK_LE(throttle, +1.0);
			m_throttle = throttle;
		}

		virtual void getThrust(Vector3 & applicationPoint, Vector3 & force);

	private:

		dimensionless_t m_throttle = 0.0;
		Vector3         m_position;
		newton_t        m_thrustMax;
	};
}
