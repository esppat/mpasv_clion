#include "SimuElement.h"

namespace mpasv::controller::simulation
{

	SimuElement::SimuElement(std::shared_ptr<SimulationController> simulationController, const std::list<std::string> names)
		: Nameable(names), m_simulationController(simulationController)
	{
	}
}
