#include "SimuPropeller.h"

#include "tools/miscel/Logger.h"
#include "tools/parameters/ParameterManager.h"
#include "SimulationController.h"

namespace mpasv::controller::simulation
{

	SimuPropeller::SimuPropeller(std::shared_ptr<SimulationController> simulationController, const std::list<std::string> names)
		: SimuElement(simulationController, names), m_position(param<double>(std::string("Physical simulator.") + name() + ".X (m)"), param<double>(std::string(
		"Physical simulator.") + name() + ".Y (m)"), param<double>(std::string("Physical simulator.") + name() + ".Z (m)")), m_thrustMax(param<newton_t
		                                                                                                                                       , double>(std::string(
		"Physical simulator.") + *names.begin() + ".Thrust (N)"))
	{
	}

	void SimuPropeller::getThrust(Vector3 & applicationPoint, Vector3 & force)
	{
		applicationPoint = m_position;
		force.x = decimal(m_thrustMax * m_throttle);
		force.y = force.z = 0.0;
	}
}
