//
// Created by esppat on 03/04/2020.
//

#include "SimuWindTurbine.h"

#include "tools/miscel/Logger.h"
#include "tools/parameters/ParameterManager.h"
#include "SimulationController.h"

namespace mpasv::controller::simulation
{

	SimuWindTurbine::SimuWindTurbine(std::shared_ptr<SimulationController> simulationController, const std::list<std::string> names)
		: SimuElement(simulationController, names), m_position(param<double>(std::string("Physical simulator.") + name() + ".X (m)"), param<double>(std::string(
		"Physical simulator.") + name() + ".Y (m)"), param<double>(std::string("Physical simulator.") + name() + ".Z (m)")), m_dragPerWindKnot(param<newton_t
		                                                                                                                                             , double>(
		std::string("Physical simulator.") + *names.begin() + ".Drag per wind knot (N)"))
	{
	}

	void SimuWindTurbine::getThrust(Vector3 & applicationPoint, Vector3 & force)
	{
		applicationPoint = m_position;
		force            = {1.0, 0.0, 0.0};

		if (m_simulationController->hasRelativeWindBearing() && m_simulationController->hasRelativeWindSpeed())
		{
			rotate(force, 0.0_deg, 0.0_deg, bearingToTrigo(m_simulationController->getRelativeWindBearing()));
			double windLinearVelocityKnot = doubleFrom<knot_t>(m_simulationController->getRelativeWindSpeed());
			force *= windLinearVelocityKnot * doubleFrom<newton_t>(m_dragPerWindKnot);
		}
	}
}
