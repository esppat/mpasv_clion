#include "SimuDaggerBoardPlane.h"

#include "tools/miscel/Tools.h"
#include "tools/miscel/Exception.h"
#include "tools/miscel/Logger.h"
#include "tools/parameters/ParameterManager.h"
#include "tools/gps/GeoPos.h"
#include "SimulationController.h"

namespace mpasv::controller::simulation
{
	std::map<degree_t, std::pair<dimensionless_t, dimensionless_t>> SimuDaggerBoardPlane::m_attackAngleToPercent;

	SimuDaggerBoardPlane::SimuDaggerBoardPlane(std::shared_ptr<SimulationController> simulationController, const std::list<std::string> & names)
		: SimuElement(simulationController, names)
		  , m_position(param<double>(std::string("Physical simulator.Dagger board planes.") + name() + ".X (m)")
		               , param<double>(std::string("Physical simulator.Dagger board planes.") + name() + ".Y (m)")
		               , param<double>(std::string("Physical simulator.Dagger board planes.") + name() + ".Z (m)"))
		  , m_drag(param<double>(std::string("Physical simulator.Dagger board planes.") + *names.begin() + ".Drag X (N per Kt)"), param<double>(std::string(
			"Physical simulator.Dagger board planes.") + *names.begin() + ".Drag Y (N per Kt)"), param<double>(std::string(
			"Physical simulator.Dagger board planes.") + *names.begin() + ".Drag Z (N per Kt)"))
	{
	}

	void SimuDaggerBoardPlane::calculateThrustsLevel(const Vector3 & localLinearVelocity, const degree_t attackAngle, Vector3 & thrustLevel)
	{
		thrustLevel.z = 0.0;

		if (m_attackAngleToPercent.size() == 0)
		{
			fillAttackAngleToPercent();
		}

		degree_t previousDegree, nextDegree;
		findPreviousAndNextDegrees(attackAngle, previousDegree, nextDegree);

		if (previousDegree == nextDegree)
		{
			CHECK_EQUAL(previousDegree, attackAngle);

			auto & p = m_attackAngleToPercent[previousDegree];
			thrustLevel.x = p.first;
			thrustLevel.y = p.second;
		}
		else
		{
			CHECK_LT(previousDegree, attackAngle);
			CHECK_LT(attackAngle, nextDegree);

			dimensionless_t percent = (attackAngle - previousDegree) / (nextDegree - previousDegree);

			auto & previous = m_attackAngleToPercent[previousDegree];
			auto & next     = m_attackAngleToPercent[nextDegree];

			dimensionless_t diff = next.first - previous.first;
			thrustLevel.x = previous.first + diff * percent;

			diff = next.second - previous.second;
			thrustLevel.y = previous.second + diff * percent;
		}
	}

	void SimuDaggerBoardPlane::fillAttackAngleToPercent()
	{
		m_attackAngleToPercent[-180.0_deg] = {+0.60, +0.00};
		m_attackAngleToPercent[-170.0_deg] = {+1.00, +0.20};
		m_attackAngleToPercent[-135.0_deg] = {+1.00, +0.60};
		m_attackAngleToPercent[-90.0_deg]  = {+0.00, +0.80};
		m_attackAngleToPercent[-45.0_deg]  = {-0.40, +1.00};
		m_attackAngleToPercent[-10.0_deg]  = {-0.20, +0.75};
		m_attackAngleToPercent[0.0_deg]    = {-0.20, +0.00};
		m_attackAngleToPercent[10.0_deg]   = {-0.20, -0.75};
		m_attackAngleToPercent[45.0_deg]   = {-0.40, -1.00};
		m_attackAngleToPercent[90.0_deg]   = {+0.00, -0.80};
		m_attackAngleToPercent[135.0_deg]  = {+1.00, -0.60};
		m_attackAngleToPercent[170.0_deg]  = {+1.00, -0.20};
		m_attackAngleToPercent[180.0_deg]  = {+0.60, -0.00};
	}

	void SimuDaggerBoardPlane::getThrust(Vector3 & applicationPoint, Vector3 & thrust)
	{
		applicationPoint = m_position;

		Vector3 localLinearVelocity = m_simulationController->getBodyLinearVelocity() + m_simulationController->getBodyAngularVelocity().cross(m_position);

		if (m_orientation != 0.0_deg)
		{
			rotate(localLinearVelocity, 0.0_deg, 0.0_deg, -m_orientation);
		}

		degree_t attackAngle = atan2(radian_t(localLinearVelocity.y), radian_t(localLinearVelocity.x));
		attackAngle = normalize180(attackAngle);

		Vector3 thrustLevel;
		calculateThrustsLevel(localLinearVelocity, attackAngle, thrustLevel);

		if (m_orientation != 0.0_deg)
		{
			rotate(thrustLevel, 0.0_deg, 0.0_deg, m_orientation);
		}

		thrust = (m_drag * localLinearVelocity.getAbsoluteVector()) * thrustLevel;

		//		LOG_DEBUG(name(),
		//		          " bodyLinearVelocity=", bodyLinearVelocity.to_string(),
		//		          " bodyAngularVelocity=", bodyAngularVelocity.to_string(),
		//		          " localLinearVelocity=", localLinearVelocity.to_string(),
		//		          " m_orientation=", degree_t(m_orientation),
		//		          " attackAngle=", attackAngle,
		//		          " thrustLevel=", thrustLevel.to_string(),
		//		          " thrust=", thrust.to_string());
	}

	void SimuDaggerBoardPlane::findPreviousAndNextDegrees(const degree_t & attackAngle, degree_t & previousDegree, degree_t & nextDegree)
	{
		auto first = m_attackAngleToPercent.begin();

		for (auto it = m_attackAngleToPercent.begin(); it != m_attackAngleToPercent.end(); it++)
		{
			if (it->first == attackAngle)
			{
				previousDegree = nextDegree = attackAngle;
				return;
			}

			if (it->first > attackAngle)
			{
				previousDegree = first->first;
				nextDegree     = it->first;
				return;
			}

			first = it;
		}

		THROW_ALWAYS("Internal error");
	}
}
