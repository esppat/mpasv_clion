/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   SimulationController_ReactPhysics3d.cpp
 * Author: esppat
 *
 * Created on January 16, 2020, 8:54 PM
 */

#include "SimulationController_ReactPhysics3d.h"
#include "tools/miscel/Logger.h"
#include "tools/parameters/ParameterManager.h"

namespace mpasv::controller::simulation
{

	SimulationController_ReactPhysics3d::SimulationController_ReactPhysics3d()
		: SimulationController("SimulationController_ReactPhysics3d")
	{
	}

	void SimulationController_ReactPhysics3d::setStartPoint(const tools::geo::GeoPos & geoPos, const degree_t bearing)
	{
		SimulationController::setStartPoint(geoPos, bearing);

		auto transform   = m_body->getTransform();
		auto orientation = Quaternion::fromEulerAngles(0.0, 0.0, doubleFrom<radian_t>(bearingToTrigo(bearing)));
		transform.setOrientation(orientation);
		m_body->setTransform(transform);
	}

	void SimulationController_ReactPhysics3d::create3dBody()
	{
		m_gravity                  = new Vector3(0.0, 0.0, 0.0);
		m_world                    = new DynamicsWorld(*m_gravity);

		// Change the number of iterations of the velocity solver
		m_world->setNbIterationsVelocitySolver(15);

		// Change the number of iterations of the position solver
		m_world->setNbIterationsPositionSolver(8);

		// Initial position and orientation of the rigid body
		Vector3    initPosition(0.0, 0.0, 0.0);
		Quaternion initOrientation = Quaternion::identity();
		Transform  transform(initPosition, initOrientation);

		// Create a rigid body in the world
		m_body = m_world->createRigidBody(transform);

		Vector3 hullHalfExtents
			        (param<decimal>("Physical description.Hull.X (Lenght) (m)") / 2.0, param<decimal>("Physical description.Hull.Y (Width) (m)") / 2.0, param<
				        decimal>("Physical description.Hull.Z (Height) (m)") / 2.0);
		m_hullCollisionShape = new BoxShape(hullHalfExtents);

		m_hullMass = param<decimal>("Physical description.Hull.Mass (kg)");

		initPosition = Vector3(0.0, param<decimal>("Physical description.Distance hull COG (m)"), 0.0);
		transform    = Transform(initPosition, initOrientation);
		m_leftHull   = m_body->addCollisionShape(m_hullCollisionShape, transform, m_hullMass);

		initPosition.y = -initPosition.y;
		transform   = Transform(initPosition, initOrientation);
		m_rightHull = m_body->addCollisionShape(m_hullCollisionShape, transform, m_hullMass);

		m_body->recomputeMassInformation();
	}

	void SimulationController_ReactPhysics3d::update3dBody(const second_t tickDuration)
	{
		m_world->update(decimal(delayBetweenRuns()));

		auto transform = m_body->getTransform();
		auto position  = transform.getPosition();
		position += ((m_windLinearVelocity * m_windEffectRatio) + (m_currentLinearVelocity * m_currentEffectRatio)) * doubleFrom<second_t>(tickDuration);
		transform.setPosition(position);
		m_body->setTransform(transform);

		m_previousBodyLinearVelocity  = m_bodyLinearVelocity;
		m_previousBodyAngularVelocity = m_bodyAngularVelocity;
		m_previousBodyTransform       = m_bodyTransform;

		m_bodyTransform       = m_body->getTransform();
		m_bodyLinearVelocity  = m_bodyTransform.getOrientation().getInverse() * m_body->getLinearVelocity();
		m_bodyAngularVelocity = m_body->getAngularVelocity();

		m_bodyLinearAcceleration  = (m_bodyLinearVelocity - m_previousBodyLinearVelocity) / double(tickDuration);
		m_bodyAngularAcceleration = (m_bodyAngularVelocity - m_previousBodyAngularVelocity) / double(tickDuration);

		for (auto element : m_simuElements)
		{
			Vector3 applicationPoint;
			Vector3 force;

			element->getThrust(applicationPoint, force);

			applicationPoint = m_bodyTransform * applicationPoint;
			force            = m_bodyTransform.getOrientation() * force;

			m_body->applyForce(force, applicationPoint);
		}
	}
}
