#pragma once

#include "SimuElement.h"
#include "tools/gps/GeoPos.h"
#include "tools/physics/Units.h"

#include <map>

namespace mpasv::controller::simulation
{

	class SimulationController;

	class SimuDaggerBoardPlane
		: public SimuElement
	{
	public:

		SimuDaggerBoardPlane(std::shared_ptr<SimulationController> simulationController, const std::list<std::string> & names);

		virtual void getThrust(Vector3 & applicationPoint, Vector3 & thrust);

		void setOrientation(const degree_t orientation)
		{
			m_orientation = normalize180(orientation);
		}

	private:

		void calculateThrustsLevel(const Vector3 & localLinearVelocity, const degree_t attackAngle, Vector3 & thrustPercent);

		static void fillAttackAngleToPercent();

		static void findPreviousAndNextDegrees(const degree_t & attackAngle, degree_t & previousDegree, degree_t & nextDegree);

	private:

		degree_t m_orientation = 0.0_deg;

		const Vector3 m_position;
		const Vector3 m_drag;

		static std::map<degree_t, std::pair<dimensionless_t, dimensionless_t>> m_attackAngleToPercent;
	};
}
