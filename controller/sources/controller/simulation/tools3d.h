//
// Created by esppat on 15/03/2020.
//

#pragma once

#include "tools/physics/Units.h"
#include "tools/gps/GeoPos.h"

#define IS_DOUBLE_PRECISION_ENABLED

#include "reactphysics3d.h"

using namespace rp3d;

template<class T> void rotate(Vector3 & v, const T & angleX, const T & angleY, const T & angleZ)
{
	Quaternion q = Quaternion::fromEulerAngles(decimal(radian_t(angleX)), decimal(radian_t(angleY)), decimal(radian_t(angleZ)));
	v = q * v;
}

template<typename T> void rotate(T & x, T & y, T & z, const radian_t & angleRadianX, const radian_t & angleRadianY, const radian_t & angleRadianZ)
{
	Vector3 v = {decimal(x), decimal(y), decimal(z)};
	rotate(v, angleRadianX, angleRadianY, angleRadianZ);
	x = T(v.x);
	y = T(v.y);
	z = T(v.z);
}

inline degree_t trigoToBearing(const degree_t degree)
{
	return normalize180(-degree + 90.0_deg);
}

inline degree_t bearingToTrigo(const degree_t degree)
{
	return normalize360(-degree + 90.0_deg);
}
