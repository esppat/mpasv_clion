/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   SimulationController.h
 * Author: esppat
 *
 * Created on January 16, 2020, 8:54 PM
 */

#pragma once

#include "controller/Controller.h"
#include "SimuDaggerBoardPlane.h"
#include "SimuElement.h"
#include "SimuPropeller.h"
#include "SimuWindTurbine.h"

#include "tools/gps/GeoPos.h"

#include <map>

#include "reactphysics3d.h"

using namespace reactphysics3d;

namespace mpasv
{
	namespace drivers
	{
		class DriverManager;
	}

	namespace controller::simulation
	{

		class SimulationController
			: public controller::Controller
			  , public enable_shared_from_this<SimulationController>
		{
		public:

			SimulationController(const std::string & name);

			virtual ~SimulationController() = default;

			void setDriverManager(const std::shared_ptr<drivers::DriverManager> & driverManager);

			std::shared_ptr<drivers::DriverManager> getDriverManager()
			{
				return m_driverManager;
			}

			virtual void setStartPoint(const tools::geo::GeoPos & geoPos, const degree_t bearing);

			const Vector3 & getBodyLinearVelocity() const
			{
				return m_bodyLinearVelocity;
			}

			const Vector3 & getBodyAngularVelocity() const
			{
				return m_bodyAngularVelocity;
			}

			const Transform & getBodyTransform() const
			{
				return m_bodyTransform;
			}

			MemberArray<ampere_t> m_amperages;
			MemberArray<volt_t>   m_voltages;

		DEF_MEMBER_WITH_LIMITS(gpsSpeed, GPSSpeed, knot_t, 0_kts, 30_kts);
		DEF_MEMBER_WITH_LIMITS(gpsBearing, GPSBearing, degree_t, -180_deg, 180_deg);
		DEF_MEMBER(geoPos, GeoPos, tools::geo::GeoPos);
		DEF_MEMBER_WITH_LIMITS(waterSpeed, WaterSpeed, knot_t, -30_kts, 30_kts);
		DEF_MEMBER_WITH_LIMITS(relativeWindSpeed, RelativeWindSpeed, knot_t, 0_kts, 300_kts);
		DEF_MEMBER_WITH_LIMITS(relativeWindBearing, RelativeWindBearing, degree_t, -180_deg, 180_deg);
		DEF_MEMBER_WITH_LIMITS(compass, Compass, degree_t, -180_deg, 180_deg);
		DEF_MEMBER(accelX, AccelX, meters_per_second_squared_t);
		DEF_MEMBER(accelY, AccelY, meters_per_second_squared_t);
		DEF_MEMBER(accelZ, AccelZ, meters_per_second_squared_t);
		DEF_MEMBER(gyroX, GyroX, degrees_per_second_t);
		DEF_MEMBER(gyroY, GyroY, degrees_per_second_t);
		DEF_MEMBER(gyroZ, GyroZ, degrees_per_second_t);

		protected:

			virtual void create3dBody() = 0;

			virtual void update3dBody(const second_t tickDuration) = 0;

		protected:

			std::list<std::shared_ptr<SimuElement>> m_simuElements;

			const kilogram_t m_oneHullWeight;

			Vector3   m_previousBodyLinearVelocity;
			Vector3   m_previousBodyAngularVelocity;
			Transform m_previousBodyTransform;

			Vector3   m_bodyLinearVelocity;
			Vector3   m_bodyAngularVelocity;
			Transform m_bodyTransform;

			Vector3 m_bodyLinearAcceleration;
			Vector3 m_bodyAngularAcceleration;

			knot_t                m_windSpeed         = 0.0_kts;
			degree_t              m_windBearing_trigo = 0.0_deg;
			const dimensionless_t m_windEffectRatio;

			knot_t m_currentSpeed = 0.0_kts;;
			degree_t              m_currentBearing_trigo = 0.0_deg;
			const dimensionless_t m_currentEffectRatio;

			Vector3 m_windLinearVelocity;
			Vector3 m_relativeWindLinearVelocity;
			Vector3 m_currentLinearVelocity;

		private:

			virtual bool setup() noexcept override;

			virtual void initialize() noexcept override;

			virtual void run(const second_t tickDuration) noexcept override;

			virtual void finalize() noexcept override;

			void doCalculation(const second_t tickDuration);

			void calculateWindParameters();

			void calculateCurrentParameters();

			void calculatePowerSimulation();

			void updateSimuElements();

		private:

			std::shared_ptr<drivers::DriverManager> m_driverManager;

			std::shared_ptr<SimuPropeller>        m_leftPropeller;
			std::shared_ptr<SimuPropeller>        m_rightPropeller;
			std::shared_ptr<SimuDaggerBoardPlane> m_leftHelm;
			std::shared_ptr<SimuDaggerBoardPlane> m_rightHelm;

			const bool m_hasTwoHulls;
		};
	}
}

using namespace mpasv::controller::simulation;
