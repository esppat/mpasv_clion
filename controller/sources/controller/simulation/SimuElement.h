#pragma once

#include "tools/miscel/Tools.h"
#include "tools/miscel/Nameable.h"
#include "tools/physics/Units.h"
#include "tools3d.h"

namespace mpasv::controller::simulation
{

	class SimulationController;

	class SimuElement
		: public Nameable
		  , public mpasv::tools::enable_shared_from_this<SimuElement>
	{
	public:

		SimuElement(std::shared_ptr<SimulationController> simulationController, const std::list<std::string> names);

		virtual void getThrust(Vector3 & applicationPoint, Vector3 & force) = 0;

	public:
	protected:
	protected:

		std::shared_ptr<SimulationController> m_simulationController;

	private:
	private:
	};
}
