//
// Created by esppat on 03/04/2020.
//

#pragma once

#include "SimuElement.h"

#include "tools/miscel/Exception.h"

namespace mpasv::controller::simulation
{

	class SimulationController;

	class SimuWindTurbine
		: public SimuElement
	{
	public:

		SimuWindTurbine(std::shared_ptr<SimulationController> simulationController, const std::list<std::string> names);

		virtual void getThrust(Vector3 & applicationPoint, Vector3 & force);

	private:

		Vector3  m_position;
		newton_t m_dragPerWindKnot;
	};
}
