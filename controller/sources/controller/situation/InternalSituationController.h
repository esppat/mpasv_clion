/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   InternalSituationController.h
 * Author: esppat
 *
 * Created on January 16, 2020, 8:54 PM
 */

#pragma once

#include "controller/Controller.h"

#include "tools/miscel/SetUnsetValue.h"
#include "tools/physics/AmortizedValue.h"
#include "tools/physics/ValueHistory.h"
#include "tools/miscel/Tools.h"
#include "tools/miscel/Exception.h"
#include "tools/gps/GeoPos.h"

#include "drivers/AbstractDriver.h"

#include <map>

namespace mpasv
{
	namespace drivers
	{
		class DriverManager;

		namespace power
		{
			class PowerSensor;
		}
	}

	namespace controller
	{
		namespace situation
		{

			class InternalSituationController
				: public controller::Controller
			{
			public:

				InternalSituationController();

				virtual ~InternalSituationController();

				void setDriverManager(const std::shared_ptr<drivers::DriverManager> & driverManager);

			DEF_MEMBER(engineVoltage, EngineVoltage, volt_t);
			DEF_MEMBER(engineAmperage, EngineAmperage, ampere_t);

			protected:
			protected:
			private:

				virtual bool setup() noexcept override;

				virtual void initialize() noexcept override;

				virtual void run(const second_t tickDuration) noexcept override;

				virtual void finalize() noexcept override;

				std::shared_ptr<drivers::DriverManager> m_driverManager;

				std::map<std::string, std::shared_ptr<drivers::AbstractDriver>> * m_powerSensors;
			};
		} // situation
	} // controller
} // mpasv
