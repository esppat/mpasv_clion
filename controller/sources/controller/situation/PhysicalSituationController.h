/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   PhysicalSituationController.h
 * Author: esppat
 *
 * Created on January 16, 2020, 8:54 PM
 */

#pragma once

#include "controller/Controller.h"

#include "tools/miscel/SetUnsetValue.h"
#include "tools/physics/AmortizedValue.h"
#include "tools/physics/ValueHistory.h"
#include "tools/miscel/Tools.h"
#include "tools/miscel/Exception.h"
#include "tools/gps/GeoPos.h"

namespace mpasv
{
	namespace drivers
	{
		class DriverManager;

		namespace GPS
		{
			class GPSSensorFusion;
		}

		namespace IMU
		{
			class IMUSensor;
		}

		namespace water
		{
			class WaterSpeedSensor;
		}

		namespace wind
		{
			class WindSensor;
		}
	}

	namespace controller
	{
		namespace situation
		{

			class PhysicalSituationController
				: public controller::Controller
			{
			public:

				PhysicalSituationController();

				virtual ~PhysicalSituationController();

				void setDriverManager(const std::shared_ptr<drivers::DriverManager> & driverManager);

			protected:
			protected:
			private:

				virtual bool setup() noexcept override;

				virtual void initialize() noexcept override;

				virtual void run(const second_t tickDuration) noexcept override;

				virtual void finalize() noexcept override;

				std::shared_ptr<drivers::DriverManager> m_driverManager;

				std::shared_ptr<drivers::IMU::IMUSensor> m_IMUSensor;

				std::shared_ptr<drivers::GPS::GPSSensorFusion> m_GPSSensorFusion;

				std::shared_ptr<drivers::water::WaterSpeedSensor> m_waterSpeedSensor;

				std::shared_ptr<drivers::wind::WindSensor> m_windSensor;
			};
		} // situation
	} // controller
} // mpasv
