/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   InternalSituationController.cpp
 * Author: esppat
 *
 * Created on January 16, 2020, 8:54 PM
 */

#include "InternalSituationController.h"

#include "tools/miscel/Exception.h"
#include "tools/miscel/Logger.h"
#include "tools/miscel/Tools.h"
#include "tools/parameters/ParameterManager.h"

#include "drivers/DriverManager.h"
#include "drivers/sensors/power/PowerSensor.h"

namespace mpasv::controller::situation
{

	InternalSituationController::InternalSituationController()
		: Controller("InternalSituationController")
	{
		DM()->declareData<watt_hour_t>("Calculated.Battery energy");
		DM()->declareDataLimits("Calculated.Battery energy", 0.0_Wh, DM()->getConstantValue<watt_hour_t>("Param.Physical simulator.Battery.Capacity (Wh)"));

		DM()->declareData<watt_hour_t>("Calculated.Consumed energy");
	}

	InternalSituationController::~InternalSituationController() = default;

	void InternalSituationController::setDriverManager(const std::shared_ptr<drivers::DriverManager> & driverManager)
	{
		CHECK_NOT_NULLPTR(driverManager);
		m_driverManager = driverManager;
	}

	bool InternalSituationController::setup() noexcept
	{
		bool result = false;

		try
		{
			m_powerSensors = &m_driverManager->getDriversByType<drivers::power::PowerSensor>();


			// battery is fully charged at start, in simulation mode
			DM()->setValue<watt_hour_t>("Calculated.Battery energy", DM()->getConstantValue<watt_hour_t>("Param.Physical simulator.Battery.Capacity (Wh)"));
			DM()->setValue<watt_hour_t>("Calculated.Consumed energy", 0.0_Wh);

			// TODO

			result = Controller::setup();
		}
		catch (std::exception & e)
		{
			LOG_DEBUG("Caught exception: ", e.what());
		}

		return result;
	}

	void InternalSituationController::initialize() noexcept
	{
		Controller::initialize();

		// TODO
	}

	void InternalSituationController::run(const second_t tickDuration) noexcept
	{
		watt_t immediatepower = 0_W;

		for (auto & it : *m_powerSensors)
		{
			it.second->pull();
			if (!contains(it.second->name(), "Battery") && !contains(it.second->name(), "Solar panel") && !contains(it.second->name(), "Wind turbine"))
			{
				if (DM()->valueIsAvailable<watt_t>(it.second->name() + ".Sensor.Power.Power"))
				{
					immediatepower += DM()->getValue<watt_t>(it.second->name() + ".Sensor.Power.Power");
				}
			}
		}

		watt_hour_t consumedEnergy = DM()->getValue<watt_hour_t>("Calculated.Consumed energy");
		consumedEnergy += tickDuration * immediatepower;
		DM()->setValue<watt_hour_t>("Calculated.Consumed energy", consumedEnergy);

		watt_hour_t batteryEnergy = DM()->getValue<watt_hour_t>("Calculated.Battery energy");
		if (DM()->valueIsAvailable<watt_t>("Battery.Sensor.Power.Power"))
		{
			batteryEnergy -= tickDuration * DM()->getValue<watt_t>("Battery.Sensor.Power.Power");
		}
		else
		{
			batteryEnergy -= tickDuration * immediatepower;
		}
		setInLimit(batteryEnergy, 0.0_Wh, DM()->getConstantValue<watt_hour_t>("Param.Physical simulator.Battery.Capacity (Wh)"));
		DM()->setValue<watt_hour_t>("Calculated.Battery energy", batteryEnergy);

		watt_t solarPanelPower = 0.0_W;
		if (DM()->valueIsAvailable<watt_t>("Solar panel 1.Sensor.Power.Power"))
		{
			solarPanelPower += DM()->getValue<watt_t>("Solar panel 1.Sensor.Power.Power");
		}
		if (DM()->valueIsAvailable<watt_t>("Solar panel 2.Sensor.Power.Power"))
		{
			solarPanelPower += DM()->getValue<watt_t>("Solar panel 2.Sensor.Power.Power");
		}
		consumedEnergy -= tickDuration * solarPanelPower;

		watt_t windTurbinePower = 0.0_W;
		if (DM()->valueIsAvailable<watt_t>("Wind turbine.Sensor.Power.Power"))
		{
			windTurbinePower += DM()->getValue<watt_t>("Wind turbine.Sensor.Power.Power");
		}
		consumedEnergy -= tickDuration * windTurbinePower;

		//		LOG_DEBUG("Instant comsumption=", immediatepower, " Cumulated consumption=", consumedEnergy, " Battery=", batteryEnergy, " SolarPanel=", solarPanelPower, " WindTurbine=", windTurbinePower);
	}

	void InternalSituationController::finalize() noexcept
	{
		// TODO

		Controller::finalize();
	}
} // mpasv
