/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   PhysicalSituationController.cpp
 * Author: esppat
 *
 * Created on January 16, 2020, 8:54 PM
 */

#include "PhysicalSituationController.h"

#include "tools/miscel/Exception.h"
#include "tools/miscel/Logger.h"

#include "drivers/DriverManager.h"
#include "drivers/sensors/GPS/GPSSensorFusion.h"
#include "drivers/sensors/IMU/IMUSensor.h"
#include "drivers/sensors/water/WaterSpeedSensor.h"
#include "drivers/sensors/wind/WindSensor.h"

namespace mpasv::controller::situation
{

	PhysicalSituationController::PhysicalSituationController()
		: Controller("PhysicalSituationController")
	{
	}

	PhysicalSituationController::~PhysicalSituationController() = default;

	void PhysicalSituationController::setDriverManager(const std::shared_ptr<drivers::DriverManager> & driverManager)
	{
		CHECK_NOT_NULLPTR(driverManager);
		m_driverManager = driverManager;
	}

	bool PhysicalSituationController::setup() noexcept
	{
		bool result = false;

		try
		{
			DM()->declareData<degree_t>("Calculated.Ground drift bearing");
			DM()->declareData<knot_t>("Calculated.Ground drift speed");
			DM()->declareData<knot_t>("Calculated.Real wind speed");
			DM()->declareData<degree_t>("Calculated.Real wind bearing");

			m_IMUSensor        = m_driverManager->getSensor<drivers::IMU::IMUSensor>("IMU 1");
			m_GPSSensorFusion  = m_driverManager->getSensor<drivers::GPS::GPSSensorFusion>("GPS");
			m_waterSpeedSensor = m_driverManager->getSensor<drivers::water::WaterSpeedSensor>("Lock 1");
			m_windSensor       = m_driverManager->getSensor<drivers::wind::WindSensor>("Wind 1");

			result = Controller::setup();
		}
		catch (std::exception & e)
		{
			LOG_DEBUG("Caught exception: ", e.what());
		}

		return result;
	}

	void PhysicalSituationController::initialize() noexcept
	{
		Controller::initialize();

		// TODO
	}

	void PhysicalSituationController::run(const second_t tickDuration) noexcept
	{
		bool haveFixBefore = DM()->valueIsAvailable<bool>("GPS 1.Sensor.GPS.Has fix") && DM()->getValue<bool>("GPS 1.Sensor.GPS.Has fix");

		m_IMUSensor->pull();
		m_GPSSensorFusion->pull();
		m_waterSpeedSensor->pull();
		m_windSensor->pull();

		bool haveFixAfter = DM()->valueIsAvailable<bool>("GPS 1.Sensor.GPS.Has fix") && DM()->getValue<bool>("GPS 1.Sensor.GPS.Has fix");

		if (haveFixBefore != haveFixAfter)
		{
			if (haveFixAfter)
			{
				LOG_INFO("Gained GPS fix");
			}
			else
			{
				LOG_WARN("Lost GPS fix");
			}
		}

		if (haveFixAfter)
		{
			auto         lat = DM()->getValue<latitude_t>("GPS 1.Sensor.GPS.Lat");
			auto         lon = DM()->getValue<longitude_t>("GPS 1.Sensor.GPS.Lon");
			const GeoPos posStart {lat, lon};

			GeoPos posEnd;

			if (DM()->valueIsAvailable<degree_t>("Wind 1.Sensor.Wind.Bearing") && DM()->valueIsAvailable<knot_t>("Wind 1.Sensor.Wind.Speed"))
			{
				posEnd = posStart.forwardTo(DM()->getValue<degree_t>("Wind 1.Sensor.Wind.Bearing"), DM()->getValue<knot_t>("Wind 1.Sensor.Wind.Speed") *
				                                                                                    tickDuration *
				                                                                                    DM()->getConstantValue<dimensionless_t>(
					                                                                                    "Param.Physical simulator.Wind effect ratio (pct)"));
			}
			else
			{
				posEnd = posStart;
			}

			posEnd = posEnd.forwardTo(DM()->getValue<degree_t>("GPS 1.Sensor.GPS.Bearing"), -DM()->getValue<knot_t>("GPS 1.Sensor.GPS.Speed") * tickDuration);

			DM()->setValue("Calculated.Real wind speed", knot_t(posStart.distanceTo(posEnd) / tickDuration));
			DM()->setValue("Calculated.Real wind bearing", posEnd.bearingTo(posStart) + 180.0_deg);

			if (DM()->valueIsAvailable<degree_t>("IMU 1.Sensor.IMU.Compass") && DM()->valueIsAvailable<knot_t>("Lock 1.Sensor.Water speed.Speed"))
			{
				auto posEndTheorical =
					     posStart.forwardTo(DM()->getValue<degree_t>("IMU 1.Sensor.IMU.Compass"), DM()->getValue<knot_t>("Lock 1.Sensor.Water speed.Speed") *
					                                                                              tickDuration);
				auto posEndReal      =
					     posStart.forwardTo(DM()->getValue<degree_t>("GPS 1.Sensor.GPS.Bearing"), DM()->getValue<knot_t>("GPS 1.Sensor.GPS.Speed") *
					                                                                              tickDuration);

				DM()->setValue("Calculated.Ground drift bearing", posEndTheorical.bearingTo(posEndReal));
				DM()->setValue("Calculated.Ground drift speed", knot_t(((posEndTheorical.distanceTo(posEndReal)) / tickDuration)));
			}
			else
			{
				LOG_DEBUG("Missing 'Lock 1.Sensor.Water speed.Speed' and/or 'IMU 1.Sensor.IMU.Compass' to calculate drift");

				DM()->setValueUnavailable<degree_t>("Calculated.Ground drift bearing");
				DM()->setValueUnavailable<knot_t>("Calculated.Ground drift speed");
			}
		}
		else
		{
			LOG_DEBUG("Missing 'GPS 1.Sensor.GPS.Has fix' to calculate relative wind");

			DM()->setValueUnavailable<knot_t>("Calculated.Real wind speed");
			DM()->setValueUnavailable<degree_t>("Calculated.Real wind bearing");
		}
	}

	void PhysicalSituationController::finalize() noexcept
	{
		m_IMUSensor.reset();
		m_GPSSensorFusion.reset();
		m_waterSpeedSensor.reset();
		m_windSensor.reset();

		// TODO

		Controller::finalize();
	}
} // mpasv
