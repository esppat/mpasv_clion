/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   RouteControllerGoalFollowRoute.h
 * Author: esppat
 *
 * Created on January 16, 2020, 9:08 PM
 */

#pragma once

#include "RouteControllerGoal.h"

#include "tools/physics/Units.h"

#include "tools/routing/Route.h"

namespace mpasv::controller::route
{

	class RouteControllerGoalFollowRoute
		: public controller::route::RouteControllerGoal
	{
	public:

		RouteControllerGoalFollowRoute();

		virtual ~RouteControllerGoalFollowRoute() = default;

		virtual std::string toString() const override;

		virtual void update() override;

		void loadFromFile(const std::string & file_name);

		const routing::Route & route() const
		{
			return m_route;
		}

		routing::Route & route()
		{
			return m_route;
		}

	protected:
	protected:
	private:
	private:

		routing::Route m_route;

		unsigned m_nextWaypointIndex;
		meter_t  m_distanceToNextWaypoint;

		tools::geo::GeoPos m_startPos;

		const bool m_hasTwoHulls;
	};
}
