/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   RouteController.cpp
 * Author: esppat
 *
 * Created on January 16, 2020, 8:54 PM
 */

#include "RouteController.h"
#include "RouteControllerGoal.h"
#include "controller/situation/GlobalSituationController.h"
#include "controller/position/PositionController.h"

#include "tools/miscel/Logger.h"
#include "tools/miscel/Exception.h"
#include "tools/routing/Route.h"
#include "RouteControllerGoalFollowRoute.h"
#include "RouteControllerGoalStop.h"
#include "tools/data/DataManager.h"

namespace mpasv::controller::route
{

	RouteController::RouteController()
		: Controller("RouteController")
	{
		DM()->declareData<degree_t>("Calculated.Target GPS bearing");
		DM()->declareData<knot_t>("Calculated.Target GPS speed");
		DM()->declareData<bool>("Calculated.Target GPS speed forward");
		DM()->declareData<meter_t>("Calculated.Cross track error");

		pushGoal(std::make_shared<RouteControllerGoalStop>());
	}

	RouteController::~RouteController() = default;

	void RouteController::setDriverManager(const std::shared_ptr<drivers::DriverManager> & driverManager)
	{
		CHECK_NOT_NULLPTR(driverManager);
		m_driverManager = driverManager;
	}

	std::shared_ptr<RouteControllerGoal> RouteController::currentGoal()
	{
		auto result = std::dynamic_pointer_cast<RouteControllerGoal>(Controller::currentGoal());
		CHECK_NOT_NULLPTR(result);
		return result;
	}

	bool RouteController::setup() noexcept
	{
		bool result = false;

		try
		{
			m_globalSituationController = std::make_shared<controller::situation::GlobalSituationController>();
			m_globalSituationController->setDriverManager(m_driverManager);
			addPriorTask(m_globalSituationController);

			m_positionController = std::make_shared<controller::position::PositionController>();
			m_positionController->setDriverManager(m_driverManager);
			addPriorTask(m_positionController);

			// TODO

			result = Controller::setup();
		}
		catch (std::exception & e)
		{
			LOG_DEBUG("Caught exception: ", e.what());
		}

		return result;
	}

	void RouteController::initialize() noexcept
	{
		Controller::initialize();

		// TODO
	}

	void RouteController::run(const second_t tickDuration) noexcept
	{
		currentGoal()->update();
		if (currentGoal()->isComplete())
		{
			popGoal();
		}
	}

	void RouteController::finalize() noexcept
	{
		m_globalSituationController.reset();
		m_positionController.reset();

		// TODO

		Controller::finalize();
	}
} // mpasv
