/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   RouteControllerGoalFollowRoute.cpp
 * Author: esppat
 *
 * Created on January 16, 2020, 9:08 PM
 */

#include "RouteControllerGoalFollowRoute.h"
#include "controller/situation/GlobalSituationController.h"
#include "controller/position/PositionController.h"
#include "controller/situation/PhysicalSituationController.h"
#include "controller/simulation/tools3d.h"
#include "controller/simulation/tools3d.h"

#include "tools/data/DataManager.h"
#include "tools/miscel/Exception.h"
#include "tools/miscel/Logger.h"
#include "tools/gps/GeoPos.h"
#include "tools/parameters/ParameterManager.h"
#include "tools/routing/Waypoint.h"

namespace mpasv::controller::route
{

	RouteControllerGoalFollowRoute::RouteControllerGoalFollowRoute()
		: m_hasTwoHulls(DM()->getConstantValue<bool>("Param.Physical description.Has two hulls (bool)"))
	{
	}

	std::string RouteControllerGoalFollowRoute::toString() const
	{
		return std::string("Follow route goal: \"") + m_route.getName() + "\"";
	}

	void RouteControllerGoalFollowRoute::update()
	{
		if (!DM()->valueIsAvailable<bool>("GPS 1.Sensor.GPS.Has fix") || !DM()->getValue<bool>("GPS 1.Sensor.GPS.Has fix"))
		{
			return;
		}

		auto         lat = DM()->getValue<latitude_t>("GPS 1.Sensor.GPS.Lat");
		auto         lon = DM()->getValue<longitude_t>("GPS 1.Sensor.GPS.Lon");
		const GeoPos currentPos {lat, lon};

		if (!m_startPos.isValid())
		{
			m_startPos = currentPos;
		}

		m_distanceToNextWaypoint = currentPos.distanceTo(m_route.getWaypoint(m_nextWaypointIndex).pos());

		while (m_distanceToNextWaypoint <= m_route.getWaypoint(m_nextWaypointIndex).getMaxDistanceToPass())
		{
			LOG_INFO("Passing waypoint ", m_nextWaypointIndex);

			m_nextWaypointIndex++;
			m_distanceToNextWaypoint = currentPos.distanceTo(m_route.getWaypoint(m_nextWaypointIndex).pos());

			if (m_nextWaypointIndex == m_route.getWaypointCount())
			{
				setComplete();
				break;
			}
		}

		meter_t crossTrackError;

		if (!isComplete())
		{
			tools::geo::GeoPos currentRouteFirstPoint;
			tools::geo::GeoPos currentRouteSecondPoint = m_route.getWaypoint(m_nextWaypointIndex).pos();

			if (m_nextWaypointIndex == 0)
			{
				currentRouteFirstPoint = m_startPos;
			}
			else
			{
				currentRouteFirstPoint = m_route.getWaypoint(m_nextWaypointIndex - 1).pos();
			}

			crossTrackError = currentPos.crossTrackError(currentRouteFirstPoint, currentRouteSecondPoint);

			DM()->setValue("Calculated.Cross track error", crossTrackError);

			meter_t  absCrossTrackError = fabs(crossTrackError);
			degree_t theoreticalCourse  = currentRouteFirstPoint.bearingTo(currentRouteSecondPoint);

			degree_t directCourseToRoute;
			if (crossTrackError > 0.0_m)
			{
				directCourseToRoute = normalize360(theoreticalCourse - 90.0_deg);
			}
			else
			{
				directCourseToRoute = normalize360(theoreticalCourse + 90.0_deg);
			}

			auto targetPos = currentPos.forwardTo(directCourseToRoute, absCrossTrackError);

			// TODO improve route to store more information

			second_t durationToAnticipate = 5.0_s;
			meter_t  distanceToAnticipate = 10_m;

			meter_t totalDistanceToAnticipate = distanceToAnticipate;
			if (DM()->getValue<knot_t>("GPS 1.Sensor.GPS.Speed") > 0.5_kts)
			{
				totalDistanceToAnticipate += DM()->getValue<knot_t>("GPS 1.Sensor.GPS.Speed") * durationToAnticipate;
			}

			if (absCrossTrackError < totalDistanceToAnticipate)
			{
				totalDistanceToAnticipate -= absCrossTrackError;

				if (totalDistanceToAnticipate <= targetPos.distanceTo(currentRouteSecondPoint))
				{
					targetPos = targetPos.forwardTo(theoreticalCourse, totalDistanceToAnticipate);
				}
				else
				{
					targetPos = m_route.getWaypoint(m_nextWaypointIndex).pos();
				}
			}

			DM()->setValue<degree_t>("Calculated.Target GPS bearing", currentPos.bearingTo(targetPos));
			DM()->setValue<knot_t>("Calculated.Target GPS speed", m_route.getWaypoint(m_nextWaypointIndex).getSpeedToHere());
			DM()->setValue<bool>("Calculated.Target GPS speed forward", true);
		}
		else
		{
			DM()->setValueUnavailable<degree_t>("Calculated.Target GPS bearing");
			DM()->setValueUnavailable<knot_t>("Calculated.Target GPS speed");
			DM()->setValueUnavailable<bool>("Calculated.Target GPS speed forward");
			DM()->setValueUnavailable<meter_t>("Calculated.Cross track error");

			crossTrackError = -9999_m;
		}

		knot_t               gpsSpeed         =
			                     (DM()->valueIsAvailable<knot_t>("GPS 1.Sensor.GPS.Speed") ? DM()->getValue<knot_t>("GPS 1.Sensor.GPS.Speed") : -9999_kts);
		knot_t               targetGpsSpeed   =
			                     (DM()->valueIsAvailable<knot_t>("Calculated.Target GPS speed") ? DM()->getValue<knot_t>("Calculated.Target GPS speed")
			                                                                               : -9999.0_kts);
		degree_t             gpsBearing       =
			                     (DM()->valueIsAvailable<degree_t>("GPS 1.Sensor.GPS.Bearing") ? DM()->getValue<degree_t>("GPS 1.Sensor.GPS.Bearing") : -9999.0_deg);
		degree_t             targetGpsBearing =
			                     (DM()->valueIsAvailable<degree_t>("Calculated.Target GPS bearing") ? DM()->getValue<degree_t>("Calculated.Target GPS bearing")
			                                                                                   : -9999.0_deg);
		radians_per_second_t gyroZ            = (DM()->valueIsAvailable<radians_per_second_t>("IMU 1.Sensor.IMU.gyroZ") ? DM()->getValue<radians_per_second_t>(
			"IMU 1.Sensor.IMU.gyroZ") : -9999.0_rps);

		//		if (m_hasTwoHulls)
		//		{
		//			LOG_DEBUG(std::right, std::fixed, "Speed ", std::setw(4), std::setprecision(1), gpsSpeed, " / ", std::setw(4), targetGpsSpeed, " | ", "Course "
		//			          , std::setw(4), std::setprecision(0), gpsBearing, " / ", std::setw(4), targetGpsBearing, " | ", "Rot ", std::setw(6), std::setprecision(1)
		//			          , (degrees_per_second_t)gyroZ, " | ", "Helm ", std::setw(3), std::setprecision(0), DM()->getValue<degree_t>("Helm 1.Servo.Current angle")
		//			          , " | ", "LeftT ", std::setw(4), std::setprecision(0),
		//				DM()->getValue<dimensionless_t>("Engine 1.Brushless controller.Current value") * 100.0, " | ", "RightT ", std::setw(4), std::setprecision(0),
		//				DM()->getValue<dimensionless_t>("Engine 2.Brushless controller.Current value") * 100.0, " | ", "CTE ", std::setw(7), std::setprecision(1)
		//			          , crossTrackError, " | ", "DTNW ", std::setw(7), std::setprecision(0), m_distanceToNextWaypoint);
		//		}
		//		else
		//		{
		//			LOG_DEBUG(std::right, std::fixed, "Speed ", std::setw(4), std::setprecision(1), gpsSpeed, " / ", std::setw(4), targetGpsSpeed, " | ", "Course "
		//			          , std::setw(4), std::setprecision(0), gpsBearing, " / ", std::setw(4), targetGpsBearing, " | ", "Rot ", std::setw(6), std::setprecision(1)
		//			          , (degrees_per_second_t)gyroZ, " | ", "Helm ", std::setw(3), std::setprecision(0), DM()->getValue<degree_t>("Helm 1.Servo.Current angle")
		//			          , " | ", "Throttle ", std::setw(4), std::setprecision(0),
		//				DM()->getValue<dimensionless_t>("Engine 1.Brushless controller.Current value") * 100.0, " | ", "CTE ", std::setw(7), std::setprecision(1)
		//			          , crossTrackError, " | ", "DTNW ", std::setw(7), std::setprecision(0), m_distanceToNextWaypoint);
		//		}
	}

	void RouteControllerGoalFollowRoute::loadFromFile(const std::string & file_name)
	{
		m_route.loadRoute(file_name);
		m_nextWaypointIndex = 0;

		if (!DM()->valueExists<size_t>("Route.Waypoint number"))
		{
			DM()->declareData<size_t>("Route.Waypoint number");
		}
		DM()->setValue<size_t>("Route.Waypoint number", m_route.getWaypointCount());

		if (!DM()->valueExists<std::string>("Route.Name"))
		{
			DM()->declareData<std::string>("Route.Name");
		}
		DM()->setValue<std::string>("Route.Name", m_route.getName());

		size_t    waypointNextNumero = 0;
		for (auto waypoint : m_route.getWaypoints())
		{
			std::string RouteWaypointNumero = "Route.Waypoint " + std::to_string(waypointNextNumero);
			if (!DM()->valueExists<Waypoint>(RouteWaypointNumero))
			{
				DM()->declareData<Waypoint>(RouteWaypointNumero);
			}
			DM()->setValueNoLimits<Waypoint>(RouteWaypointNumero, m_route.getWaypoint(waypointNextNumero));
		}
	}
}
