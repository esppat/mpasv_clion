/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   RouteControllerGoal.cpp
 * Author: esppat
 *
 * Created on January 16, 2020, 9:08 PM
 */

#include "RouteControllerGoal.h"
#include "controller/situation/GlobalSituationController.h"
#include "controller/position/PositionController.h"
#include "controller/situation/PhysicalSituationController.h"

#include "tools/miscel/Logger.h"
#include "tools/miscel/Demangler.h"

namespace mpasv::controller::route
{

	RouteControllerGoal::RouteControllerGoal()
	{
		// TODO
	}

	std::string RouteControllerGoal::toString() const
	{
		return std::string("Empty goal (does nothing)");
	}

	void RouteControllerGoal::update()
	{
		LOG_ERR(toString());
	}
} // mpasv
