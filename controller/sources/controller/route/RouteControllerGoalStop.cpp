/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   RouteControllerGoalStop.cpp
 * Author: esppat
 *
 * Created on January 16, 2020, 9:08 PM
 */

#include "RouteControllerGoalStop.h"
#include "controller/situation/GlobalSituationController.h"
#include "controller/position/PositionController.h"
#include "controller/situation/PhysicalSituationController.h"
#include "tools/data/DataManager.h"

#include "tools/miscel/Logger.h"
#include "tools/miscel/Demangler.h"

namespace mpasv::controller::route
{

	RouteControllerGoalStop::RouteControllerGoalStop()
	{
		// TODO
	}

	RouteControllerGoalStop::~RouteControllerGoalStop() = default;

	std::string RouteControllerGoalStop::toString() const
	{
		return std::string("Empty goal (does nothing)");
	}

	void RouteControllerGoalStop::update()
	{
		if (DM()->valueIsAvailable<bool>("GPS 1.Sensor.GPS.Has fix") && DM()->getValue<bool>("GPS 1.Sensor.GPS.Has fix"))
		{
			LOG_DEBUG("Speed = ", DM()->getValue<knot_t>("GPS 1.Sensor.GPS.Speed"), ", Course = ", DM()->getValue<knot_t>("GPS 1.Sensor.GPS.Bearing"));
		}

		DM()->setValue<degree_t>("Calculated.Target GPS bearing", 0.0_deg);
		DM()->setValue<knot_t>("Calculated.Target GPS speed", 0.0_kts);
		DM()->setValue<bool>("Calculated.Target GPS speed forward", true);
	}
} // mpasv
