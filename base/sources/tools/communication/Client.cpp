//
// Created by esppat on 22/04/2020.
//

#include "Client.h"

#include "tools/miscel/Tools.h"

namespace mpasv::tools::communication
{

	Client::Client(const std::string & taskName)
		: Nameable(taskName)
	{
		// TODO
	}

	Client::~Client()
	{
		LOCK(mutex());

		// TODO
	}

	void Client::setCallback(std::shared_ptr<IClientCallback> clientCallback)
	{
		m_clientCallback = clientCallback;
	}

	bool Client::asyncConnect(const std::string & serverAddress, const uint32_t serverPort, const std::string & persistenceDirectory)
	{
		// TODO check params

		LOCK(mutex());

		bool result    = false;
		auto serverURI = serverAddress + ":" + std::to_string(serverPort);

		if (m_connectToken == nullptr)
		{
			if (!isConnected())
			{
				m_mqttClient.reset(new mqtt::async_client(serverURI, name(), persistenceDirectory));

				m_mqttClient->set_callback(*this);

				mqtt::connect_options options;
				options.set_keep_alive_interval(20);
				options.set_clean_session(true);
				options.set_user_name("esppat"); // TODO pass as params
				options.set_password("poupinou");

				m_connectToken = m_mqttClient->connect(options, nullptr, *this);

				result = true;

				LOG_DEBUG("asyncConnect to server '", serverURI, "' started");
			}
			else
			{
				LOG_ERR("asyncConnect failed: asyncConnect called while already connected");
			}
		}
		else
		{
			LOG_ERR("asyncConnect to server '", serverURI, "' failed: An asynchronous connection is pending");
		}

		return result;
	}

	bool Client::isConnecting() const
	{
		LOCK(mutex());
		return m_connectToken != nullptr;
	}

	void Client::waitForAsyncConnectionComplete() const
	{
		if (!isConnected())
		{
			if (m_mqttClient != nullptr)
			{
				LOG_DEBUG("Wait for asyncConnect complete");
				while (isConnecting())
				{
					sleepFor(1_ms);
				}

				if (isConnected())
					LOG_DEBUG("asyncConnect complete");
				else
					LOG_DEBUG("asyncConnect failed");
			}
		}
	}

	bool Client::isConnected() const
	{
		LOCK(mutex());

		bool result = false;

		if (m_mqttClient != nullptr)
		{
			result = m_mqttClient->is_connected();
		}

		return result;
	}

	bool Client::asyncDisconnect()
	{
		LOCK(mutex());

		bool result = false;

		if (isConnected())
		{
			if (m_disconnectToken == nullptr)
			{
				mqtt::disconnect_options options; // TODO

				m_disconnectToken = m_mqttClient->disconnect(options);
				result            = true;

				LOG_DEBUG("asyncDisconnect started");
			}
			else
			{
				LOG_ERR("asyncDisconnect failed: An asynchronous disconnection is pending");
			}
		}
		else
		{
			LOG_ERR("asyncDisconnect failed: client is not connected");
		}

		return result;
	}

	bool Client::isDisconnecting() const
	{
		LOCK(mutex());
		return m_disconnectToken != nullptr;
	}

	void Client::waitForAsyncDisconnectComplete() const
	{
		if (isConnected())
		{
			if (m_mqttClient != nullptr)
			{
				LOG_DEBUG("Wait for asyncDisconnect complete");
				while (isDisconnecting())
				{
					sleepFor(1_ms);
				}

				if (isConnected())
					LOG_DEBUG("asyncDisconnect failed");
				else
					LOG_DEBUG("asyncDisconnect complete");
			}
		}
	}

	bool Client::registerPublishTopics(const std::vector<TopicName> & topicNames)
	{
		return registerTopics(topicNames, m_publishedTopics, "published");
	}

	bool Client::unregisterPublishTopics(const std::vector<TopicName> & topicNames)
	{
		return unregisterTopics(topicNames, m_publishedTopics, "published");
	}

	bool Client::registerSubscribeTopics(const std::vector<TopicName> & topicNames)
	{
		return registerTopics(topicNames, m_subscribedTopics, "subscribed");
	}

	bool Client::unregisterSubscribeTopics(const std::vector<TopicName> & topicNames)
	{
		return unregisterTopics(topicNames, m_subscribedTopics, "subscribed");
	}

	bool Client::asyncPublish(const TopicName & topicName, const char * buffer, size_t bufferSize) const
	{
		LOCK(mutex());

		bool result = false;

		if (isConnected())
		{
			if (m_publishedTopics.find(topicName) != m_publishedTopics.end())
			{
				// TODO
				int  qos      = 1;
				bool retained = false;

				m_mqttClient->publish(topicName, buffer, bufferSize, qos, retained);

				result = true;
			}
			else
			{
				LOG_ERR("asyncPublish failed:topicName not registered");
			}
		}
		else
		{
			LOG_ERR("asyncPublish failed: client is not connected");
		}

		return result;
	}

	bool Client::asyncSubscribe(const TopicName & topicName)
	{
		LOCK(mutex());

		bool result = false;

		if (m_subscribedTopics.find(topicName) != m_subscribedTopics.end())
		{
			if (isConnected())
			{
				startConsummingForSubscribe();

				m_mqttClient->subscribe(topicName, 1);

				result = true;
			}
			else
			{
				LOG_ERR("asyncSubscribe failed for topic '", topicName, "': client is not connected");
			}
		}
		else
		{
			LOG_ERR("asyncSubscribe failed for topic '", topicName, "': this topic name is not registered");
		}

		return result;
	}

	bool Client::asyncUnsubscribe(const TopicName & topicName)
	{
		LOCK(mutex());

		bool result = false;

		if (m_subscribedTopics.find(topicName) != m_subscribedTopics.end())
		{
			if (isConnected())
			{
				m_mqttClient->unsubscribe(topicName);

				result = true;
			}
			else
			{
				LOG_ERR("asyncUnsubscribe failed for topic '", topicName, "': client is not connected");
			}
		}
		else
		{
			LOG_ERR("asyncUnsubscribe failed for topic '", topicName, "': this topic name is not registered");
		}

		return result;
	}

	bool Client::asyncSubscribe(const std::vector<TopicName> & topicNames)
	{
		LOCK(mutex());

		bool result = true;

		for (auto topicName : topicNames)
		{
			if (!asyncSubscribe(topicName))
			{
				result = false;
				break;
			}
		}

		return result;
	}

	bool Client::asyncUnsubscribe(const std::vector<TopicName> & topicNames)
	{
		LOCK(mutex());

		bool result = true;

		for (auto topicName : topicNames)
		{
			if (!asyncUnsubscribe(topicName))
			{
				result = false;
				break;
			}
		}

		return result;
	}

	[[nodiscard]]
	bool Client::registerTopics(const std::vector<TopicName> & topicNames, std::set<TopicName> & topicCollection, const std::string nameForMessage)
	{
		LOCK(mutex());

		bool result = false;

		for (auto topicName : topicNames)
		{
			if (topicCollection.find(topicName) == topicCollection.end())
			{
				topicCollection.insert(topicName);
				LOG_DEBUG("Added ", nameForMessage, " topic '", topicName, "' in client '", name(), "'");
				result = true;
			}
			else
			{
				LOG_WARN("Skipped adding ", nameForMessage, " topic '", topicName, "' in client '", name(), "' reason='Topic already exists'");
			}
		}

		return result;
	}

	[[nodiscard]]
	bool Client::unregisterTopics(const std::vector<TopicName> & topicNames, std::set<TopicName> & topicCollection, const std::string nameForMessage)
	{
		LOCK(mutex());

		bool result = false;

		for (auto topicName : topicNames)
		{
			if (topicCollection.find(topicName) != topicCollection.end())
			{
				topicCollection.erase(topicName);
				LOG_DEBUG("Removed ", nameForMessage, " topic '", topicName, "' in client '", name(), "'");
				result = true;
			}
			else
			{
				LOG_WARN("Skipped removing ", nameForMessage, " topic '", topicName, "' in client '", name(), "' reason='Topic does not exist'");
			}
		}

		return result;
	}

	void Client::onAsyncConnectFailure(const mqtt::token & asyncActionToken)
	{
		LOG_WARN("Cannot connect: return_code=", asyncActionToken.get_return_code());
		m_connectToken.reset();
		m_mqttClient.reset();
		if (m_clientCallback != nullptr)
		{
			m_clientCallback->onAsyncConnectFailure(asyncActionToken);
		}
	}

	void Client::onAsyncConnectSuccess(const mqtt::token & asyncActionToken)
	{
		LOG_INFO("Connected");
		m_connectToken.reset();
		if (m_clientCallback != nullptr)
		{
			m_clientCallback->onAsyncConnectSuccess(asyncActionToken);
		}
	}

	void Client::onAsyncDisconnectFailure(const mqtt::token & asyncActionToken)
	{
		LOG_WARN("Cannot disconnect: return_code=", asyncActionToken.get_return_code());
		m_disconnectToken.reset();
		if (m_clientCallback != nullptr)
		{
			m_clientCallback->onAsyncDisconnectFailure(asyncActionToken);
		}
	}

	void Client::onAsyncDisconnectSuccess(const mqtt::token & asyncActionToken)
	{
		LOG_INFO("Disconnected");
		m_disconnectToken.reset();
		m_mqttClient.reset();
		if (m_clientCallback != nullptr)
		{
			m_clientCallback->onAsyncDisconnectSuccess(asyncActionToken);
		}
	}

	void Client::onAsyncPublishFailure(const mqtt::token & asyncActionToken)
	{
		LOG_WARN("Cannot publish: return_code=", asyncActionToken.get_return_code()); // TODO improve message
		if (m_clientCallback != nullptr)
		{
			m_clientCallback->onAsyncPublishFailure(asyncActionToken);
		}
	}

	void Client::onAsyncPublishSuccess(const mqtt::token & asyncActionToken)
	{
		LOG_INFO("Publish succeed message_id=", asyncActionToken.get_message_id());
		if (m_clientCallback != nullptr)
		{
			m_clientCallback->onAsyncPublishSuccess(asyncActionToken);
		}
	}

	void Client::onAsyncSubscribeFailure(const mqtt::token & asyncActionToken)
	{
		std::string topcisAsString = stringCollectionAsOneString(asyncActionToken.get_topics());
		LOG_WARN("Cannot subcribe for (", topcisAsString, "): return_code=", asyncActionToken.get_return_code());
		if (m_clientCallback != nullptr)
		{
			m_clientCallback->onAsyncSubscribeFailure(asyncActionToken);
		}
	}

	void Client::onAsyncSubscribeSuccess(const mqtt::token & asyncActionToken)
	{
		std::string topcisAsString = stringCollectionAsOneString(asyncActionToken.get_topics());
		LOG_WARN("Subscribed successfully for (", topcisAsString, ")");
		if (m_clientCallback != nullptr)
		{
			m_clientCallback->onAsyncSubscribeSuccess(asyncActionToken);
		}
	}

	void Client::onAsyncUnsubscribeFailure(const mqtt::token & asyncActionToken)
	{
		std::string topcisAsString = stringCollectionAsOneString(asyncActionToken.get_topics());
		LOG_WARN("Cannot unsubcribe for (", topcisAsString, "): return_code=", asyncActionToken.get_return_code());
		if (m_clientCallback != nullptr)
		{
			m_clientCallback->onAsyncUnsubscribeFailure(asyncActionToken);
		}
	}

	void Client::onAsyncUnsubscribeSuccess(const mqtt::token & asyncActionToken)
	{
		std::string topcisAsString = stringCollectionAsOneString(asyncActionToken.get_topics());
		LOG_WARN("Unsubscribed successfully for (", topcisAsString, ")");
		if (m_clientCallback != nullptr)
		{
			m_clientCallback->onAsyncUnsubscribeSuccess(asyncActionToken);
		}
	}

	void Client::onMessageReceived(const TopicName & topicName, const char * buffer, const size_t bufferSize)
	{
		//		LOG_DEBUG("Received message on topic '", topicName, "'");
		if (m_clientCallback != nullptr)
		{
			m_clientCallback->onMessageReceived(topicName, buffer, bufferSize);
		}
	}

	// mqtt::iaction_listener
	void Client::on_failure(const mqtt::token & asyncActionToken)
	{
		LOCK(mutex());

		switch (asyncActionToken.get_type())
		{
			case mqtt::token::Type::CONNECT:
				onAsyncConnectFailure(asyncActionToken);
				break;

			case mqtt::token::Type::SUBSCRIBE:
				onAsyncSubscribeFailure(asyncActionToken);
				break;

			case mqtt::token::Type::PUBLISH:
				onAsyncPublishFailure(asyncActionToken);
				break;

			case mqtt::token::Type::UNSUBSCRIBE:
				onAsyncUnsubscribeFailure(asyncActionToken);
				break;

			case mqtt::token::Type::DISCONNECT:
				onAsyncDisconnectFailure(asyncActionToken);
				break;

			default:
				LOG_ERR("Unknown asyncActionToken.get_type() received: ", asyncActionToken.get_type());
				break;
		}
	}

	void Client::on_success(const mqtt::token & asyncActionToken)
	{
		LOCK(mutex());

		switch (asyncActionToken.get_type())
		{
			case mqtt::token::Type::CONNECT:
				onAsyncConnectSuccess(asyncActionToken);
				break;

			case mqtt::token::Type::SUBSCRIBE:
				onAsyncSubscribeSuccess(asyncActionToken);
				break;

			case mqtt::token::Type::PUBLISH:
				onAsyncPublishSuccess(asyncActionToken);
				break;

			case mqtt::token::Type::UNSUBSCRIBE:
				onAsyncUnsubscribeSuccess(asyncActionToken);
				break;

			case mqtt::token::Type::DISCONNECT:
				onAsyncDisconnectSuccess(asyncActionToken);
				break;

			default:
				LOG_ERR("Unknown asyncActionToken.get_type() received: ", asyncActionToken.get_type());
				break;
		}
	}

	void Client::connected(const std::string & cause)
	{
		LOG_DEBUG("Connection OK cause='", cause, "'");
	}

	void Client::connection_lost(const std::string & cause)
	{
		LOG_WARN("Connection lost cause='", cause, "'");
	}

	void Client::message_arrived(mqtt::const_message_ptr msg)
	{
		//		LOG_DEBUG("Message arrived on topic '", msg->get_topic(), "'");
		onMessageReceived(msg->get_topic(), msg->get_payload().data(), msg->get_payload().size());
	}

	void Client::delivery_complete(mqtt::delivery_token_ptr tok)
	{
		//		LOG_DEBUG("Delivery complete for message id ", tok->get_message_id());
	}

	void Client::startConsummingForSubscribe()
	{
		if (!m_consumming)
		{
			if (isConnected())
			{
				m_mqttClient->start_consuming();
			}
			else
			{
				LOG_ERR("failed: client not connected");
			}
		}
	}

	std::string Client::stringCollectionAsOneString(mqtt::const_string_collection_ptr collection)
	{
		std::string topcisAsString;
		for (size_t index = 0; index < collection->size(); index++)
		{
			if (index == 0)
			{
				topcisAsString = "'" + collection->operator[](index);
			}
			else
			{
				topcisAsString += "', '" + collection->operator[](index);
			}
		}
		if (collection->size() > 0)
		{
			topcisAsString += "'";
		}
		return std::move(topcisAsString);
	}

	mqtt::const_string_collection_ptr Client::toMqttStringCollection(const std::vector<TopicName> & collection)
	{
		mqtt::string_collection_ptr stringCollection(new mqtt::string_collection);
		for (auto                   str : collection)
		{
			stringCollection->push_back(str);
		}

		return stringCollection;
	}
}
