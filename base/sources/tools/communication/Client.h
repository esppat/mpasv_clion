//
// Created by esppat on 22/04/2020.
//

#pragma once

#include "tools/miscel/Logger.h"
#include "tools/miscel/Lockable.h"
#include "tools/miscel/ThreadedTask.h"

#include "mqtt/client.h"
#include "mqtt/async_client.h"

#include <set>
#include <map>

namespace mpasv::tools::communication
{
	typedef std::string TopicName;

	class IClientCallback
	{
	public:

		virtual void onAsyncConnectFailure(const mqtt::token & asyncActionToken) = 0;
		virtual void onAsyncConnectSuccess(const mqtt::token & asyncActionToken) = 0;

		virtual void onAsyncDisconnectFailure(const mqtt::token & asyncActionToken) = 0;
		virtual void onAsyncDisconnectSuccess(const mqtt::token & asyncActionToken) = 0;

		virtual void onAsyncPublishFailure(const mqtt::token & asyncActionToken) = 0;
		virtual void onAsyncPublishSuccess(const mqtt::token & asyncActionToken) = 0;

		virtual void onAsyncSubscribeFailure(const mqtt::token & asyncActionToken) = 0;
		virtual void onAsyncSubscribeSuccess(const mqtt::token & asyncActionToken) = 0;

		virtual void onAsyncUnsubscribeFailure(const mqtt::token & asyncActionToken) = 0;
		virtual void onAsyncUnsubscribeSuccess(const mqtt::token & asyncActionToken) = 0;

		virtual void onMessageReceived(const TopicName & topicName, const char * buffer, const size_t bufferSize) = 0;
	};

	class Client
		: public Lockable
		  , public Nameable
		  , public mqtt::iaction_listener
		  , public mqtt::callback
	{
	public:

		Client(const std::string & taskName);
		virtual ~Client();

		Client & operator=(const Client &) = delete;
		Client & operator=(Client &&) = delete;

		void setCallback(std::shared_ptr<IClientCallback> clientCallback);

		[[nodiscard]]
		bool asyncConnect(const std::string & serverAddress, const uint32_t serverPort, const std::string & persistenceDirectory);

		[[nodiscard]]
		bool isConnecting() const;

		void waitForAsyncConnectionComplete() const;

		[[nodiscard]]
		bool isConnected() const;

		[[nodiscard]]
		bool asyncDisconnect();

		[[nodiscard]]
		bool isDisconnecting() const;

		void waitForAsyncDisconnectComplete() const;

		[[nodiscard]]
		bool registerPublishTopics(const std::vector<TopicName> & topicNames);

		[[nodiscard]]
		bool unregisterPublishTopics(const std::vector<TopicName> & topicNames);

		[[nodiscard]]
		bool registerSubscribeTopics(const std::vector<TopicName> & topicNames);

		[[nodiscard]]
		bool unregisterSubscribeTopics(const std::vector<TopicName> & topicNames);

		template<typename T> [[nodiscard]]
		bool asyncPublish(const TopicName & topicName, const T & value) const;

		[[nodiscard]]
		bool asyncPublish(const TopicName & topicName, const char * buffer, const size_t bufferSize) const;

		bool asyncSubscribe(const TopicName & topicName);
		bool asyncUnsubscribe(const TopicName & topicName);

		bool asyncSubscribe(const std::vector<TopicName> & topicNames);
		bool asyncUnsubscribe(const std::vector<TopicName> & topicNames);

	public:
	protected:
	protected:
	private:

		void onAsyncConnectFailure(const mqtt::token & asyncActionToken);
		void onAsyncConnectSuccess(const mqtt::token & asyncActionToken);
		void onAsyncDisconnectFailure(const mqtt::token & asyncActionToken);
		void onAsyncDisconnectSuccess(const mqtt::token & asyncActionToken);
		void onAsyncPublishFailure(const mqtt::token & asyncActionToken);
		void onAsyncPublishSuccess(const mqtt::token & asyncActionToken);
		void onAsyncSubscribeFailure(const mqtt::token & asyncActionToken);
		void onAsyncSubscribeSuccess(const mqtt::token & asyncActionToken);
		void onAsyncUnsubscribeFailure(const mqtt::token & asyncActionToken);
		void onAsyncUnsubscribeSuccess(const mqtt::token & asyncActionToken);
		void onMessageReceived(const TopicName & topicName, const char * buffer, const size_t bufferSize);

		[[nodiscard]]
		bool registerTopics(const std::vector<TopicName> & topicNames, std::set<TopicName> & topicCollection, const std::string nameForMessage);

		[[nodiscard]]
		bool unregisterTopics(const std::vector<TopicName> & topicNames, std::set<TopicName> & topicCollection, const std::string nameForMessage);

		// mqtt::iaction_listener
		virtual void on_failure(const mqtt::token & asyncActionToken) override final;
		virtual void on_success(const mqtt::token & asyncActionToken) override final;

		// mqtt::callback
		virtual void connected(const std::string & cause) override;
		virtual void connection_lost(const std::string & cause) override;
		virtual void message_arrived(mqtt::const_message_ptr msg) override;
		virtual void delivery_complete(mqtt::delivery_token_ptr tok) override;

		void startConsummingForSubscribe();
		std::string stringCollectionAsOneString(mqtt::const_string_collection_ptr collection);
		mqtt::const_string_collection_ptr toMqttStringCollection(const std::vector<TopicName> & collection);

	private:

		std::shared_ptr<mqtt::async_client> m_mqttClient;
		std::set<TopicName>                 m_publishedTopics;
		std::set<TopicName>                 m_subscribedTopics;
		mqtt::token_ptr                     m_connectToken;
		mqtt::token_ptr                     m_disconnectToken;
		bool                                m_consumming = false;
		std::shared_ptr<IClientCallback>    m_clientCallback;
	};

	template<typename T> bool Client::asyncPublish(const TopicName & topicName, const T & value) const
	{
		return asyncPublish(topicName, (const char *)&value, sizeof(value));
	}
}

using namespace mpasv::tools::communication;
