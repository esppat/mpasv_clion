#include "tools/miscel/Logger.h"

#include "tools/miscel/Lock.h"

#include "tools/miscel/Exception.h"
#include "tools/gps/GeoPos.h"
#include "tools/data/DataManager.h"
#include "Route.h"

#include <iostream>
#include <sstream>
#include <iomanip>
#include <fstream>

namespace mpasv::routing
{

	Route::Route()
	{
	}

	Route::~Route()
	{
	}

	void Route::createRouteWithOneWaypoint(const std::string & name, const tools::geo::GeoPos & geoPos)
	{
		CHECK_TRUE(!name.empty());
		CHECK_TRUE(geoPos.isValid());

		m_name = name;
		m_waypoints.resize(0);

		Waypoint waypoint(geoPos, 5_m, 0_m, std::string("WP_1"), -1_s);

		LOG_DEBUG("Way Point "
		          , waypoint.getName()
		          , ", Latitude: "
		          , std::setprecision(12)
		          , waypoint.pos().lat()
		          , ", Longitude: "
		          , waypoint.pos().lon()
		          , ", distance from start: "
		          , waypoint.getDistanceFromStart()
		          , ", Duration to stay: "
		          , waypoint.getDurationToStay());
		m_waypoints.push_back(waypoint);
	}

	void Route::loadRoute(const std::string & route)
	{
		m_name = route;

		const std::string fileName = "data/routes/" + route + ".json";
		std::ifstream     file(fileName, std::ifstream::in);
		if (file.is_open())
		{
			nlohmann::json data;
			file >> data;

			m_waypoints = {};

			meter_t  distanceFromStart        = 0.0_m;
			unsigned waypointCount            = 0;
			meter_t  defaultDistanceToPass    = meter_t(data["Default distance to pass (m)"].get<double>());
			second_t defaultDurationToStay    = second_t(data["Default duration to stay (s)"].get<double>());
			meter_t  defaultMaxDistanceToStay = meter_t(data["Default max distance to stay (m)"].get<double>());
			knot_t   speedToHere              = 0.0_kts;

			for (auto wp : data["Way points"])
			{
				degree_t longitude = degree_t(wp["Lon (deg)"].get<double>());
				degree_t latitude  = degree_t(wp["Lat (deg)"].get<double>());
				GeoPos   pos(latitude, longitude);

				if (waypointCount > 0)
				{
					distanceFromStart += m_waypoints[waypointCount - 1].pos().distanceTo(pos);
				}

				meter_t distanceToPass;
				{
					auto it = wp.find("Distance to pass (m)");
					if (it != wp.end())
					{
						distanceToPass = meter_t(it->get<double>());
					}
					else
					{
						distanceToPass = defaultDistanceToPass;
					}
				}

				std::string wpName;
				{
					auto it = wp.find("Name");
					if (it != wp.end())
					{
						wpName = it->get<std::string>();
					}
					else
					{
						wpName = std::string("WP") + std::to_string(waypointCount);
					}
				}

				second_t durationToStay;
				{
					auto it = wp.find("Duration to stay (s)");
					if (it != wp.end())
					{
						durationToStay = second_t(it->get<double>());
					}
					else
					{
						durationToStay = defaultDurationToStay;
					}
				}

				Waypoint waypoint(pos, distanceToPass, distanceFromStart, wpName, durationToStay);

				meter_t maxDistanceToStay;
				{
					auto it = wp.find("Max distance to stay (m)");
					if (it != wp.end())
					{
						maxDistanceToStay = meter_t(it->get<double>());
					}
					else
					{
						maxDistanceToStay = defaultMaxDistanceToStay;
					}
				}

				waypoint.setMaxDistanceToStay(maxDistanceToStay);

				// speed to here
				{
					auto it = wp.find("Speed to here (kts)");
					if (it != wp.end())
					{
						speedToHere = knot_t(it->get<double>());
					}
				}
				waypoint.setSpeedToHere(speedToHere);

				m_waypoints.push_back(waypoint);

				LOG_DEBUG("Way Point "
				          , waypoint.getName()
				          , ", Latitude: "
				          , std::setprecision(12)
				          , waypoint.pos().lat()
				          , ", Longitude: "
				          , waypoint.pos().lon()
				          , ", distance from start: "
				          , waypoint.getDistanceFromStart()
				          , ", distance to pass: "
				          , waypoint.getMaxDistanceToPass()
				          , ", Duration to stay: "
				          , waypoint.getDurationToStay()
				          , ", Max distance to stay: "
				          , waypoint.getMaxDistanceToStay()
				          , ", Speed to here: "
				          , waypoint.getSpeedToHere());

				++waypointCount;
			}

			LOG_DEBUG("Parameters loaded");
		}
		else
		{
			LOG_ERR("Cannot open route file ", fileName, " in read mode");
		}
	}
} // mpasv
