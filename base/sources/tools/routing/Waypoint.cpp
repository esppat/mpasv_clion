#include "Waypoint.h"

#include "tools/miscel/Exception.h"

namespace mpasv::routing
{

	Waypoint::Waypoint()
	{
		m_valid = false;
	}

	Waypoint::Waypoint(const tools::geo::GeoPos & pos
	                   , const meter_t maxDistanceToPass
	                   , const meter_t distanceFromStart
	                   , const std::string & name
	                   , const second_t durationToStay)
	{
		m_pos = pos;

		CHECK_TRUE(maxDistanceToPass > 1.0_m);
		m_maxDistanceToPass = maxDistanceToPass;
		m_distanceFromStart = distanceFromStart;
		m_name              = name;
		m_valid             = true;
		m_durationToStay    = durationToStay;
	}

	Waypoint::~Waypoint() = default;

	meter_t Waypoint::getMaxDistanceToPass() const
	{
		return m_maxDistanceToPass;
	}

	meter_t Waypoint::getDistanceFromStart() const
	{
		return m_distanceFromStart;
	}

	std::string Waypoint::getName() const
	{
		return m_name;
	}

	bool Waypoint::isValid() const
	{
		return m_valid;
	}

	second_t Waypoint::getDurationToStay() const
	{
		return m_durationToStay;
	}

	void Waypoint::setDurationToStay(const second_t durationToStay)
	{
		m_durationToStay = durationToStay;
	}

	meter_t Waypoint::getMaxDistanceToStay() const
	{
		return m_maxDistanceToStay;
	}

	void Waypoint::setMaxDistanceToStay(const meter_t maxDistanceToStay)
	{
		m_maxDistanceToStay = maxDistanceToStay;
	}

	knot_t Waypoint::getSpeedToHere() const
	{
		return m_speedToHere;
	}

	void Waypoint::setSpeedToHere(const knot_t speed)
	{
		CHECK_GT(speed, 0.0_kts);
		m_speedToHere = speed;
	}
} // mpasv
