#pragma once

#include "tools/gps/GeoPos.h"

#include <string>

namespace mpasv::routing
{

	class Waypoint
	{
	public:
		Waypoint();

		Waypoint(const tools::geo::GeoPos & pos
		         , const meter_t maxDistanceToPass
		         , const meter_t distanceFromStart
		         , const std::string & name
		         , const second_t durationToStay);

		~Waypoint();

		tools::geo::GeoPos pos() const
		{
			return m_pos;
		}

		meter_t getMaxDistanceToPass() const;

		meter_t getDistanceFromStart() const;

		std::string getName() const;

		bool isValid() const;

		second_t getDurationToStay() const;

		void setDurationToStay(const second_t durationToStay);

		meter_t getMaxDistanceToStay() const;

		void setMaxDistanceToStay(const meter_t maxDistanceToStay);

		knot_t getSpeedToHere() const;

		void setSpeedToHere(const knot_t speed);

	private:
		tools::geo::GeoPos m_pos;
		meter_t            m_maxDistanceToPass = 0_m;
		std::string        m_name;
		meter_t            m_distanceFromStart = 0_m;
		bool               m_valid             = false;
		second_t           m_durationToStay    = 0_s;
		meter_t            m_maxDistanceToStay = 0_m;
		knot_t             m_speedToHere       = 0_kts;
	};
}

using namespace mpasv::routing;
