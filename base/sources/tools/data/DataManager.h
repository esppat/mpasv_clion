//
// Created by esppat on 25/03/2020.
//

#pragma once

#include "tools/miscel/Exception.h"
#include "tools/miscel/Nameable.h"
#include "tools/miscel/Lockable.h"
#include "tools/miscel/Lock.h"
#include "tools/miscel/Demangler.h"

#include <memory>
#include <string>
#include <map>

namespace mpasv::tools::data
{

	class DataManager
		: public LockableShared
	{
	public:

		DataManager() = default;

		~DataManager() = default;

		DataManager(const DataManager &) = delete;

		DataManager & operator=(const DataManager &) = delete;

		static std::shared_ptr<DataManager> getInstance()
		{
			if (sm_instance == nullptr)
			{
				LOCK(sm_mutex);
				if (sm_instance == nullptr)
				{
					sm_instance = std::make_shared<DataManager>();
				}
			}

			return sm_instance;
		}

		template<typename TYPE> void declareData(const std::string & name)
		{
			//LOG_DEBUG("Declare data '", name, "' with value '", defaultValue, "'");
			std::lock_guard<std::shared_mutex> lock(mutex());
			CHECK_EQUAL_MSG(m_data.find(name), m_data.end(), "Data named '" + name + "' already exists");
			m_data[name] = std::dynamic_pointer_cast<BaseData>(std::make_shared<Data < TYPE>>
			());
		}

		template<typename TYPE> void declareConstantData(const std::string & name, const TYPE & value)
		{
			//LOG_DEBUG("Declare constant data '", name, "' with value '", value, "'");
			std::lock_guard<std::shared_mutex> lock(mutex());
			CHECK_EQUAL_MSG(m_data.find(name), m_data.end(), "Data named '" + name + "' already exists");
			m_data[name] = std::dynamic_pointer_cast<BaseData>(std::make_shared<ConstantData < TYPE>>
			(value));
		}

		template<typename TYPE> void declareDataLimits(const std::string & name, const TYPE & min, const TYPE & max)
		{
			CHECK_LT(min, max);

			std::lock_guard<std::shared_mutex> lock(mutex());
			getData<TYPE>(name)->setLimits(min, max);
		}

		template<typename TYPE> bool valueExists(const std::string & name) const
		{
			std::shared_lock<std::shared_mutex> lock(mutex());
			return dataExists<TYPE>(name);
		}

		template<typename TYPE> TYPE getValue(const std::string & name) const
		{
			std::shared_lock<std::shared_mutex> lock(mutex());
			return getData<TYPE>(name)->getValue();
		}

		template<typename TYPE> void setValue(const std::string & name, const TYPE & value)
		{
			std::shared_lock<std::shared_mutex> lock(mutex());
			getData<TYPE>(name)->setValue(value);
			//LOG_DEBUG("Set value name '", name, "', value '", value, "'");
		}

		template<typename TYPE> void setValueNoLimits(const std::string & name, const TYPE & value)
		{
			std::shared_lock<std::shared_mutex> lock(mutex());
			getData<TYPE>(name)->setValueNoLimits(value);
			//LOG_DEBUG("Set value name '", name, "', value '", value, "'");
		}

		template<typename TYPE> void setValueUnavailable(const std::string & name)
		{
			std::shared_lock<std::shared_mutex> lock(mutex());
			getData<TYPE>(name)->setValueUnavailable();
		}

		template<typename TYPE> bool valueIsAvailable(const std::string & name)
		{
			std::shared_lock<std::shared_mutex> lock(mutex());
			return getData<TYPE>(name)->isAvailable();
		}

		template<typename TYPE> TYPE getConstantValue(const std::string & name) const
		{
			std::shared_lock<std::shared_mutex> lock(mutex());
			return getConstantData<TYPE>(name)->getValue();
		}

		[[nodiscard]]
		bool isArchived(const std::string & name) const
		{
			return getBaseData(name)->isArchived();
		}

		void setArchived(const std::string & name)
		{
			getBaseData(name)->setArchived(true);
		}

	public:
	protected:
	protected:
	private:

	private:

		static std::recursive_mutex         sm_mutex;
		static std::shared_ptr<DataManager> sm_instance;

		class BaseData
		{
		public:

			BaseData() = default;

			virtual ~BaseData() = default;

			[[nodiscard]]
			bool isArchived() const
			{
				return m_archived;
			}

			void setArchived(const bool archived)
			{
				m_archived = archived;
			}

		public:

			bool m_archived = false;
		};

		template<typename TYPE>
		class Data
			: public BaseData
			  , public LockableShared
		{
		public:

			Data() = default;

			Data(const Data<TYPE> & other) = delete;

			Data<TYPE> & operator=(const Data<TYPE> & other) = delete;

			TYPE getValue() const
			{
				std::shared_lock<std::shared_mutex> lock(mutex());
				CHECK_TRUE(m_available);
				return m_value;
			}

			void setValue(const TYPE & value)
			{
				std::lock_guard<std::shared_mutex> lock(mutex());

				if (m_hasLimits)
				{
					CHECK_LE(m_min, value);
					CHECK_LE(value, m_max);
				}

				m_value     = value;
				m_available = true;

				setArchived(false);
			}

			void setValueNoLimits(const TYPE & value)
			{
				std::lock_guard<std::shared_mutex> lock(mutex());

				CHECK_FALSE(m_hasLimits);

				m_value     = value;
				m_available = true;

				setArchived(false);
			}

			void setValueUnavailable()
			{
				std::lock_guard<std::shared_mutex> lock(mutex());
				m_available = false;
			}

			bool isAvailable() const
			{
				std::shared_lock<std::shared_mutex> lock(mutex());
				return m_available;
			}

			void setLimits(const TYPE & min, const TYPE & max)
			{
				std::lock_guard<std::shared_mutex> lock(mutex());

				m_hasLimits = true;
				m_min       = min;
				m_max       = max;
			}

		private:

			TYPE m_value;

			bool m_hasLimits = false;
			TYPE m_min;
			TYPE m_max;

			bool m_available = false;
		};

		template<typename TYPE>
		class ConstantData
			: public BaseData
		{
		public:

			ConstantData(const TYPE & value)
				: m_value(value)
			{
			}

			ConstantData(const ConstantData<TYPE> &) = default;

			ConstantData<TYPE> & operator=(const ConstantData<TYPE> &) = default;

			TYPE getValue() const
			{
				return m_value;
			}

		private:

			TYPE m_value;
		};

		std::shared_ptr<BaseData> getBaseData(const std::string & name) const
		{
			auto result = m_data.at(name);
			CHECK_NOT_NULLPTR_MSG(result, "Data named '" + name + "' cannot be found");
			return result;
		}

		template<typename TYPE> std::shared_ptr<Data<TYPE>> getData(const std::string & name)
		{
			auto result1 = m_data.at(name);
			CHECK_NOT_NULLPTR_MSG(result1, "Data named '" + name + "' cannot be found");

			auto result = std::dynamic_pointer_cast<Data<TYPE>>(result1);
			CHECK_NOT_NULLPTR_MSG(result, "Data named '" + name + "' is not of type '" + typeName<TYPE>() + "'");

			return result;
		}

		template<typename TYPE> std::shared_ptr<const Data<TYPE>> getData(const std::string & name) const
		{
			auto result1 = m_data.at(name);
			CHECK_NOT_NULLPTR_MSG(result1, "Data named '" + name + "' cannot be found");

			auto result = std::dynamic_pointer_cast<const Data<TYPE>>(result1);
			CHECK_NOT_NULLPTR_MSG(result, "Data named '" + name + "' is not of type '" + typeName<TYPE>() + "'");

			return result;
		}

		template<typename TYPE> bool dataExists(const std::string & name) const
		{
			bool result  = false;
			auto result1 = m_data.find(name);
			if (result1 != m_data.end())
			{
				auto result2 = std::dynamic_pointer_cast<const Data<TYPE>>(result1->second);
				if (result2 != nullptr)
				{
					result = true;
				}
			}
			return result;
		}

		template<typename TYPE> std::shared_ptr<const ConstantData<TYPE>> getConstantData(const std::string & name) const
		{
			auto result1 = m_data.at(name);
			CHECK_NOT_NULLPTR_MSG(result1, "Constant Data named '" + name + "' cannot be found");

			auto result = std::dynamic_pointer_cast<const ConstantData<TYPE>>(result1);
			CHECK_NOT_NULLPTR_MSG(result, "Constant Data named '" + name + "' is not of type '" + typeName<TYPE>() + "'");

			return result;
		}

		std::map<const std::string, std::shared_ptr<BaseData>> m_data;
	};
}

using namespace mpasv::tools::data;

inline std::shared_ptr<DataManager> DM()
{
	return mpasv::tools::data::DataManager::getInstance();
}
