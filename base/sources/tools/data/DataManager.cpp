//
// Created by esppat on 25/03/2020.
//

#include "DataManager.h"

namespace mpasv::tools::data
{

	std::recursive_mutex         DataManager::sm_mutex;
	std::shared_ptr<DataManager> DataManager::sm_instance;
}
