/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   test_coverage.h
 * Author: esppat
 *
 * Created on 29 novembre 2018, 23:19
 */

#pragma once

#ifdef TEST_COVERAGE

#define __TT() testcoverage::testTarget(__FILE__, __LINE__)

namespace mpasv
{
	namespace tools
	{
		namespace test_coverage
		{

			void testTarget(const char * file, const unsigned long line);

		} // test_coverage
	} // tools
} // mpasv

#else // !TEST_COVERAGE

#define __TT() ;

#endif // TEST_COVERAGE
