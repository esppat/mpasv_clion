#pragma once

#include "Units.h"
#include <chrono>

namespace mpasv::tools::physics
{

	class SClock
	{
	public:

		SClock();

		void pause();

		bool isPaused() const;

		void resume();

		nanosecond_t now() const
		{
			if (m_paused)
			{
				return nanosecond_t(m_pauseTime - m_epoch);
			}
			else
			{
				return nanosecond_t(std::chrono::high_resolution_clock::now() - m_epoch);
			}
		}

	protected:
	protected:
	private:
	private:

		std::chrono::system_clock::time_point m_epoch;
		std::chrono::system_clock::time_point m_pauseTime;

		bool m_paused;
	};
}

using namespace mpasv::tools::physics;

extern SClock theClock;
