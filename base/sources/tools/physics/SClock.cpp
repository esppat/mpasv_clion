#include "SClock.h"

#include "tools/miscel/Exception.h"

namespace mpasv::tools::physics
{

	SClock::SClock()
	{
		m_epoch     = std::chrono::high_resolution_clock::now();
		m_pauseTime = m_epoch;
		m_paused    = false;
	}

	void SClock::pause()
	{
		CHECK_FALSE(m_paused);
		m_paused    = true;
		m_pauseTime = std::chrono::high_resolution_clock::now();
	}

	bool SClock::isPaused() const
	{
		return m_paused;
	}

	void SClock::resume()
	{
		CHECK_TRUE(m_paused);
		m_epoch += std::chrono::high_resolution_clock::now() - m_pauseTime;
		m_paused = false;
	}
}

SClock theClock;
