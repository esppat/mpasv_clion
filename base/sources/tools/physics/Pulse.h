/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   Pulse.h
 * Author: esppat
 *
 * Created on January 19, 2020, 10:47 PM
 */

#pragma once

#include "Units.h"
#include "SClock.h"

namespace mpasv::tools::physics
{

	class Pulse
	{
	public:

		Pulse()
		{
			init(9999_d, false);
		}

		virtual ~Pulse()
		{
		}

		void init(const nanosecond_t period, const bool activate)
		{
			setPeriod(period);
			setActive(activate);
		}

		void setFrequency(const hertz_t frequency)
		{
			m_period = 1.0 / frequency;
		}

		void setPeriod(const nanosecond_t period)
		{
			m_period = period;
		}

		void setActive(const bool active)
		{
			if (m_active != active)
			{
				m_active = active;
				if (m_active)
				{
					m_start = theClock.now();
				}
			}
		}

		bool shouldPulse(nanosecond_t now) const
		{
			bool result = false;

			if (m_active)
			{
				if (now - m_start >= m_period)
				{
					m_start = now;
					result  = true;
				}
			}

			return result;
		}

		bool shouldPulse() const
		{
			return shouldPulse(theClock.now());
		}

	public:
	protected:
	protected:
	private:
	private:

		nanosecond_t     m_period;
		bool             m_active;
		mutable second_t m_start;
	};
}

using namespace mpasv::tools::physics;
