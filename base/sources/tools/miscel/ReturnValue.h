//
// Created by esppat on 15/04/2020.
//

#pragma once

#include <string>

namespace mpasv::tools::communication
{

	class ReturnValue
	{
	public:

		ReturnValue() = default;

		virtual ~ReturnValue() = default;

		ReturnValue & operator=(const ReturnValue &) = default;

		operator bool() const
		{
			return m_result;
		}

		ReturnValue & operator=(const bool result)
		{
			m_result = result;
			return *this;
		}

		const std::string & message() const
		{
			return m_message;
		}

		ReturnValue & operator=(const std::string & message)
		{
			m_message = message;
			LOG_ERR(message);
			return *this;
		}

	public:
	protected:
	protected:
	private:
	private:

		bool        m_result = false;
		std::string m_message;
	};
}

using namespace mpasv::tools::communication;
