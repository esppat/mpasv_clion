#pragma once

#include <mutex>

//#define __DEBUG__

#ifndef __DEBUG__

#define LOCK(mutex) \
            mutex.lock(); \
            std::lock_guard<std::recursive_mutex> lock(mutex, std::adopt_lock)

#define LOCK2(mutex1, mutex2) \
            std::lock(mutex1, mutex2); \
            std::lock_guard<std::recursive_mutex> lock_1(mutex1, std::adopt_lock); \
            std::lock_guard<std::recursive_mutex> lock_2(mutex2, std::adopt_lock)

#define LOCK_SHARED(mutex) \
            std::shared_lock<std::shared_mutex> lock(mutex)

#else

#include <iostream>

class Locker
{
public:

	Locker(const char * file, const unsigned line, const char * func, const char * mutexName, std::recursive_mutex & mutex);
	Locker(const char * file, const unsigned line, const char * func, const char * mutex1Name, const char * mutex2Name, std::recursive_mutex & mutex1, std::recursive_mutex & mutex2);

	virtual ~Locker();

private:

	std::lock_guard<std::recursive_mutex> * m_lock1 = nullptr;
	std::lock_guard<std::recursive_mutex> * m_lock2 = nullptr;

	static unsigned m_nblock;
	static std::recursive_mutex m_mutex;

	std::string m_file;
	unsigned m_line;
	std::string m_func;
	std::string m_mutex1Name;
	std::string m_mutex2Name;
};

#define LOCK(mutex) Locker locker(__FILE__, __LINE__, __func__, #mutex, mutex)
#define LOCK_NAMED(name, mutex) Locker locker##name(__FILE__, __LINE__, __func__, mutex)
#define LOCK2(mutex1, mutex2) Locker locker(__FILE__, __LINE__, __func__, #mutex1, #mutex2, mutex1, mutex2)
#define LOCK2_NAMED(name, mutex1, mutex2)  Locker locker##name(__FILE__, __LINE__, __func__, mutex1, mutex2)

#endif // __DEBUG__
