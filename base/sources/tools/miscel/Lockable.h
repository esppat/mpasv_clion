#pragma once

#include <mutex>
#include <shared_mutex>

namespace mpasv
{
	namespace tools
	{

		class Lockable
		{
		public:

			Lockable()
			{
			}

			virtual ~Lockable()
			{
			}

			void lock() const
			{
				m_mutex.lock();
			}

			void unlock() const
			{
				m_mutex.unlock();
			}

			std::recursive_mutex & mutex() const
			{
				return m_mutex;
			}

		protected:
		protected:
		private:
		private:

			mutable std::recursive_mutex m_mutex;
		};

		class LockableShared
		{
		public:

			LockableShared()
			{
			}

			virtual ~LockableShared()
			{
			}

			void lock() const
			{
				m_mutex.lock();
			}

			void unlock() const
			{
				m_mutex.unlock();
			}

			void lockShared() const
			{
				m_mutex.lock_shared();
			}

			void unlockShared() const
			{
				m_mutex.unlock_shared();
			}

			std::shared_mutex & mutex() const
			{
				return m_mutex;
			}

		protected:
		protected:
		private:
		private:

			mutable std::shared_mutex m_mutex;
		};
	} // tools
} // mpasv
