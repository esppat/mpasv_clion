#include "Nameable.h"

#include "Exception.h"

Nameable::Nameable(const std::string & name)
	: m_name(name)
{
	CHECK_GT(m_name.length(), 0);
}

Nameable::Nameable(const std::list<std::string> & names)
	: Nameable(Nameable::createName(names))
{
}

std::string Nameable::createName(const std::list<std::string> & names)
{
	std::string result;

	for (std::string name : names)
	{
		if (result.length() == 0)
		{
			result = name;
		}
		else
		{
			result += "." + name;
		}
	}

	return result;
}
