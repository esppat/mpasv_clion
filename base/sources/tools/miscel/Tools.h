/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   Tools.h
 * Author: PEspie
 *
 * Created on November 20, 2018, 4:44 PM
 */

#pragma once

#include "tools/test_coverage/TestCoverage.h"
#include "SetUnsetValue.h"
#include "tools/physics/RegulatedValue.h"

#include <string>
#include <algorithm>
#include <cctype>
#include <locale>
#include <memory>
#include <vector>
#include <sstream>
#include <iterator>
#include <map>

#include <sys/time.h>

namespace mpasv
{
	namespace tools
	{

		bool startsWith(const std::string & value, const std::string & starting);

		bool endsWith(const std::string & value, const std::string & ending);

		bool contains(const std::string & value, const std::string & sub);

		// trim from start (in place)
		void leftTrim(std::string & s);

		// trim from end (in place)
		void rightTrim(std::string & s);

		// trim from both ends (in place)
		void trim(std::string & s);

		// trim from start (copying)
		std::string leftTrimCopy(const std::string & s);

		// trim from end (copying)
		std::string rightTrimCopy(const std::string & s);

		// trim from both ends (copying)
		std::string trimCopy(const std::string & s);

		std::vector<std::string> split(const std::string & s, const char delimiter);

		// formatted now time
		std::string getCurrentTime();

		template<class Container> void split(const std::string & str, Container & cont, char delim)
		{
			std::stringstream ss(str);
			std::string       token;
			while (std::getline(ss, token, delim))
			{
				cont.push_back(token);
			}
		}

		template<class T> void setInLimit(T & variable, const T minValue, const T maxValue)
		{
			if (variable < minValue)
			{
				variable = minValue;
			}
			else if (variable > maxValue)
			{
				variable = maxValue;
			}
		}

		template<class T> void setInLimit(T & variable, const double minValue, const double maxValue)
		{
			setInLimit(variable, T(minValue), T(maxValue));
		}

		class enable_shared_from_this_base
			: public std::enable_shared_from_this<enable_shared_from_this_base>
		{
		public:

			virtual ~enable_shared_from_this_base()
			{
			}
		};

		template<typename T>
		class enable_shared_from_this
			: public virtual enable_shared_from_this_base
		{
		public:

			std::shared_ptr<T> shared_from_this()
			{
				return std::dynamic_pointer_cast<T>(enable_shared_from_this_base::shared_from_this());
			}
		};
	} // tools
} // mpasv

using namespace mpasv::tools;

#define DECL_NAMED_MEMBER_GET_SET(namespace, GetterName, UpperCaseClassName, LowerCaseClassName) \
    public: \
    std::shared_ptr<namespace::UpperCaseClassName> get##GetterName(); \
    std::shared_ptr<const namespace::UpperCaseClassName> get##GetterName() const; \
    void set##GetterName(std::shared_ptr<namespace::UpperCaseClassName> LowerCaseClassName); \
    private: \
    std::shared_ptr<namespace::UpperCaseClassName> m_##LowerCaseClassName

#define DEF_NAMED_MEMBER_GET_SET(thisClass, namespace, GetterName, UpperCaseClassName, LowerCaseClassName) \
    std::shared_ptr<namespace::UpperCaseClassName> thisClass::get##GetterName() \
{ \
    CHECK_NOT_NULLPTR(m_##LowerCaseClassName); \
    return m_##LowerCaseClassName; \
    } \
    std::shared_ptr<const namespace::UpperCaseClassName> thisClass::get##GetterName() const \
{ \
    CHECK_NOT_NULLPTR(m_##LowerCaseClassName); \
    return m_##LowerCaseClassName; \
    } \
    void thisClass::set##GetterName(std::shared_ptr<namespace::UpperCaseClassName> LowerCaseClassName) \
{ \
    CHECK_NOT_NULLPTR(LowerCaseClassName); \
    m_##LowerCaseClassName = LowerCaseClassName; \
    }

#define DECL_MEMBER_GET_SET(namespace, UpperCaseClassName, LowerCaseClassName) \
    DECL_NAMED_MEMBER_GET_SET(namespace, UpperCaseClassName, UpperCaseClassName, LowerCaseClassName)

#define DEF_MEMBER_GET_SET(thisClass, namespace, UpperCaseClassName, LowerCaseClassName) \
    DEF_NAMED_MEMBER_GET_SET(thisClass, namespace, UpperCaseClassName, UpperCaseClassName, LowerCaseClassName)

// -------------------------------------

#define _DEF_SET(name, upperName, type) \
public: \
    void set##upperName(const type & value) \
{ \
    m_##name = value; \
    }

#define _DEF_REGULATED_SET(name, upperName, type) \
public: \
    void set##upperName(const type & value) \
{ \
    m_##name.set(value); \
    }

#define _DEF_SET_WITH_LIMIT(name, upperName, type, low, high) \
public: \
    void set##upperName(const type & value) \
{ \
    CHECK_GE(value, low); \
    CHECK_LE(value, high); \
    m_##name = value; \
    }

#define _DEF_REGULATED_SET_WITH_LIMIT(name, upperName, type, low, high) \
public: \
    void set##upperName(const type & value) \
{ \
    CHECK_GE(value, low); \
    CHECK_LE(value, high); \
    m_##name.set(value); \
    }

#define _DEF_SET_ISVALID(name, upperName, type) \
    void set##upperName(const type & value) \
{ \
    CHECK_TRUE(value.isValid()); \
    m_##name = value; \
    }

#define _DEF_SET_WITH_HISTO(name, upperName, type) \
    void set##upperName(const type & value) \
{ \
    m_##name = value; \
    pushBack##upperName##History(value); \
    }

#define _DEF_SET_WITH_HISTO_WITH_LIMIT(name, upperName, type, low, high) \
    void set##upperName(const type & value) \
{ \
    CHECK_GE(value, low); \
    CHECK_LE(value, high); \
    m_##name = value; \
    pushBack##upperName##History(value); \
    }

#define _DEF_HAS_AND_GET(name, upperName, type) \
public: \
    bool has##upperName() const \
{ \
    return m_##name.isSet(); \
    } \
    const type & get##upperName() const \
{ \
    return m_##name; \
    } \
    void unset##upperName() \
{ \
    m_##name.unset(); \
    }

#define _DEF_REGULATED_HAS_AND_GET(name, upperName, type) \
public: \
    bool has##upperName() const \
{ \
    return m_##name.isInitialized(); \
    } \
    const type & get##upperName() const \
{ \
    return m_##name.get(); \
    } \
    void unset##upperName() \
{ \
    m_##name.reset(); \
    }

#define _DEF_HAS_AND_GET_WITH_HISTO(name, upperName, type) \
    _DEF_HAS_AND_GET(name, upperName, type) \
    void pushBack##upperName##History(const type value) \
{ \
    m_##name##History.push_back(value); \
    } \
    bool is##upperName##ContantFor(const second_t duration, const type absMaxVariation) const \
{ \
    return m_##name##History.isConstantFor(duration, absMaxVariation); \
    } \
    void reset##upperName##History() \
{ \
    m_##name##History.reset(); \
    }

#define _DEF_MEMBER(name, type) \
    private: \
    tools::SetUnsetValue<type> m_##name;

#define _DEF_REGULATED_MEMBER(name, type, type_per_second) \
    private: \
    tools::physics::RegulatedValue<type, type_per_second> m_##name;

#define _DEF_MEMBER_WITH_HISTO(name, type) \
private: \
    tools::SetUnsetValue<type> m_##name; \
    tools::physics::ValueHistory<type> m_##name##History;

#define DEF_MEMBER(name, upperName, type) \
    _DEF_HAS_AND_GET(name, upperName, type); \
    _DEF_SET(name, upperName, type); \
    _DEF_MEMBER(name, type)

#define DEF_MEMBER_WITH_LIMITS(name, upperName, type, low, high) \
    _DEF_HAS_AND_GET(name, upperName, type); \
    _DEF_SET_WITH_LIMIT(name, upperName, type, low, high); \
    _DEF_MEMBER(name, type)

#define DEF_REGULATED_MEMBER(name, upperName, type, type_per_second) \
    _DEF_REGULATED_HAS_AND_GET(name, upperName, type); \
    _DEF_REGULATED_SET(name, upperName, type); \
    _DEF_REGULATED_MEMBER(name, type)

#define DEF_REGULATED_MEMBER_WITH_LIMITS(name, upperName, type, type_per_second, low, high) \
    _DEF_REGULATED_HAS_AND_GET(name, upperName, type); \
    _DEF_REGULATED_SET_WITH_LIMIT(name, upperName, type, low, high); \
    _DEF_REGULATED_MEMBER(name, type, type_per_second)

// -------------------------------------

#define ENSURE_TRUE(s) do { if (!(s)) LOG_FATAL(#s, " should be true"); } while (false);

template<typename T> T sqr(const T & t)
{
	return t * t;
}

template<typename TYPE> class MemberArray
{
public:

	MemberArray() = default;

	virtual ~MemberArray() = default;

	MemberArray & operator=(const MemberArray &) = delete;

	void declareMember(const std::string & name, const TYPE & minValue, const TYPE & maxValue)
	{
		CHECK_EQUAL(m_members.find(name), m_members.end());
		m_members.insert({name, Member(minValue, maxValue)});
	}

	TYPE & get(const std::string & name)
	{
		auto found = m_members.find(name);
		CHECK_NOT_EQUAL(found, m_members.end());
		return found->second.get();
	}

	const TYPE & get(const std::string & name) const
	{
		auto found = m_members.find(name);
		CHECK_NOT_EQUAL(found, m_members.end());
		return found->second.get();
	}

	void set(const std::string & name, const TYPE & value)
	{
		auto found = m_members.find(name);
		CHECK_NOT_EQUAL(found, m_members.end());
		found->second.set(value);
	}

	bool isAvailable(const std::string & name) const
	{
		auto found = m_members.find(name);
		CHECK_NOT_EQUAL(found, m_members.end());
		return found->second.isAvailable();
	}

public:
protected:
protected:
private:

	class Member
	{
	public:

		Member(const TYPE & minValue, const TYPE & maxValue)
			: m_minValue(minValue), m_maxValue(maxValue)
		{
		}

		const TYPE & get() const
		{
			CHECK_TRUE(m_available);
			return m_value;
		}

		TYPE & get()
		{
			CHECK_TRUE(m_available);
			return m_value;
		}

		void set(const TYPE & value)
		{
			CHECK_GE(value, m_minValue);
			CHECK_LE(value, m_maxValue);
			m_value     = value;
			m_available = true;
		}

		bool isAvailable() const
		{
			return m_available;
		}

	private:

		TYPE       m_value {};
		bool       m_available = false;
		const TYPE m_minValue;
		const TYPE m_maxValue;
	};

private:

	std::map<std::string, Member> m_members;
};
