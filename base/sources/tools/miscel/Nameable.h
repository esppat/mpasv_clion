#pragma once

#include <string>
#include <list>

class Nameable
{
public:

	Nameable(const std::string & name);

	Nameable(const std::list<std::string> & names);

	const std::string & name() const
	{
		return m_name;
	}

public:
protected:
protected:
private:

	static std::string createName(const std::list<std::string> & name);

private:

	const std::string m_name;
};
