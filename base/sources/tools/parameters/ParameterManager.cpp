#include "ParameterManager.h"
#include "tools/miscel/Logger.h"
#include "tools/miscel/Lock.h"
#include "tools/miscel/Tools.h"
#include "tools/miscel/Exception.h"
#include "tools/data/DataManager.h"

#include "nlohmann/json.hpp"

#include <fstream>
#include <ctime>

#include "sys/stat.h"

namespace mpasv::tools::parameters
{
	/*static*/
	std::string ParameterManager::sm_fileName = "data/parameters/parameters.json";

	/*static*/
	std::recursive_mutex ParameterManager::sm_parameterManagerMutex;

	/*static*/
	tools::parameters::ParameterManager * ParameterManager::sm_parameterManager = nullptr;

	void ParameterManager::setParameterFileName(const std::string & fileName)
	{
		LOCK(sm_parameterManagerMutex);
		sm_fileName = fileName;
		if (sm_parameterManager != nullptr)
		{
			delete sm_parameterManager;
			sm_parameterManager = nullptr;
		}
	}

	bool ParameterManager::hasParameter(const std::string & path) const
	{
		LOCK(sm_parameterManagerMutex);

		std::vector<std::string> keys;
		split(path, keys, '.');

		const nlohmann::json * value = &m_state;

		for (const std::string & key : keys)
		{
			if (value->count(key) == 0)
			{
				return false;
			}

			value = &((*value)[key]);
		}

		return true;
	}

	/*static*/
	tools::parameters::ParameterManager * ParameterManager::get()
	{
		// no double check to avoid race condition with setParameterFileName

		if (sm_parameterManager == nullptr)
		{
			LOCK(sm_parameterManagerMutex);
			if (sm_parameterManager == nullptr)
			{
				sm_parameterManager = new tools::parameters::ParameterManager();
			}
		}

		return sm_parameterManager;
	}

	void ParameterManager::load()
	{
		LOCK(sm_parameterManagerMutex);
		std::ifstream file(m_fileName, std::ifstream::in);
		if (file.is_open())
		{
			file >> m_state;
			LOG_DEBUG("Parameters loaded");
		}
		else
		{
			LOG_ERR("Cannot open parameters file ", m_fileName, " in read mode");
		}
	}

	void ParameterManager::loadInDataManager()
	{
		loadInDataManager("Param", m_state);
	}

	void ParameterManager::loadInDataManager(const std::string & name, const nlohmann::json & j)
	{
		for (auto & el : j.items())
		{
			std::string currentName = name + "." + el.key();

			if (el.value().is_boolean())
			{
				DM()->declareConstantData(currentName, el.value().get<bool>());
			}
			else if (el.value().is_number())
			{
				if (endsWith(currentName, "(m)"))
				{
					DM()->declareConstantData(currentName, (meter_t)el.value().get<double>());
				}
				else if (endsWith(currentName, "(dps)"))
				{
					DM()->declareConstantData(currentName, (degrees_per_second_t)el.value().get<double>());
				}
				else if (endsWith(currentName, "(deg)"))
				{
					DM()->declareConstantData(currentName, (degree_t)el.value().get<double>());
				}
				else if (endsWith(currentName, "(ms)"))
				{
					DM()->declareConstantData(currentName, (millisecond_t)el.value().get<double>());
				}
				else if (endsWith(currentName, "(kg)"))
				{
					DM()->declareConstantData(currentName, (kilogram_t)el.value().get<double>());
				}
				else if (endsWith(currentName, "(pct)"))
				{
					DM()->declareConstantData(currentName, (dimensionless_t)el.value().get<double>());
				}
				else if (endsWith(currentName, "(kts)"))
				{
					DM()->declareConstantData(currentName, (knot_t)el.value().get<double>());
				}
				else if (endsWith(currentName, "(A)"))
				{
					DM()->declareConstantData(currentName, (ampere_t)el.value().get<double>());
				}
				else if (endsWith(currentName, "(V)"))
				{
					DM()->declareConstantData(currentName, (volt_t)el.value().get<double>());
				}
				else if (endsWith(currentName, "(Wh)"))
				{
					DM()->declareConstantData(currentName, (watt_hour_t)el.value().get<double>());
				}
				else if (endsWith(currentName, "(N)"))
				{
					DM()->declareConstantData(currentName, (newton_t)el.value().get<double>());
				}
				else if (endsWith(currentName, "(N per Kt)"))
				{
					DM()->declareConstantData(currentName, (dimensionless_t)el.value().get<double>());
				}
				else
				{
					LOG_DEBUG("Loading unknown type parameter: ", currentName);
					DM()->declareConstantData(currentName, (dimensionless_t)el.value().get<double>());
				}
			}
			else if (el.value().is_string())
			{
				DM()->declareConstantData(currentName, el.value().get<std::string>());
			}
			else
			{
				loadInDataManager(currentName, el.value());
			}
		}
	}

	void ParameterManager::save()
	{
		LOCK(sm_parameterManagerMutex);
		std::ofstream file(m_fileName, std::ofstream::out);
		if (file.is_open())
		{
			file << std::setw(4) << m_state;
			LOG_DEBUG("Parameters saved");
		}
		else
		{
			LOG_ERR("Cannot open parameters file ", m_fileName, " in write mode");
		}
	}

	ParameterManager::ParameterManager()
		: m_fileName(sm_fileName)
	{
		try
		{
			load();
			loadInDataManager();
		}
		catch (std::exception & ex)
		{
			LOG_ERR("Exception caught while parsing file ", m_fileName, ": ", ex.what());
			throw ex;
		}
	}

	nlohmann::json & ParameterManager::getParameterValue(const std::string & path)
	{
		std::vector<std::string> keys;
		split(path, keys, '.');

		nlohmann::json * value = &m_state;

		for (std::string & key : keys)
		{
			try
			{
				value = &((*value)[key]);
			}
			catch (nlohmann::detail::type_error & ex)
			{
				THROW_ALWAYS("Error while getting parameter " + key + " in " + path + ": " + ex.what());
			}
		}

		return *value;
	}

	nlohmann::json & ParameterManager::getOrCreateParameterValue(const std::string & path)
	{
		std::vector<std::string> keys;
		split(path, keys, '.');

		nlohmann::json * value = &m_state;

		for (std::string & key : keys)
		{
			try
			{
				if (value->count(key) == 0)
				{
					(*value)[key] = {};
				}

				value = &((*value)[key]);
			}
			catch (nlohmann::detail::type_error & ex)
			{
				THROW_ALWAYS("Error while getting or creating parameter " + key + " in " + path + ": " + ex.what());
			}
		}

		return *value;
	}
} // mpasv
