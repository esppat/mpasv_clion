/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   GuiController.cpp
 * Author: esppat
 *
 * Created on January 2, 2020, 6:10 PM
 */

#include "GuiController.h"

#include "tools/miscel/Tools.h"
#include "tools/miscel/Logger.h"
#include "tools/gps/GeoPos.h"

GuiController::GuiController()
	: m_client("GuiController")
{
	std::setlocale(LC_ALL, "C");

	std::shared_ptr<GuiController> This(this);
	m_client.setCallback(This);
}

void GuiController::connectToVehicle()
{
	std::string serverAddress {"tcp://192.168.1.25"}; // TODO add this in parameters
	uint32_t    serverPort = 1883;
	std::string persistenceDirectory {"./dyndata/persistence"};

	if (m_client.asyncConnect(serverAddress, serverPort, persistenceDirectory))
	{
		m_client.waitForAsyncConnectionComplete();

		LOCK(mutex());

		if (m_client.isConnected())
		{
			// TODO define topics to subscribe and to publish

			const std::vector<TopicName> publishedTopicNames {Topic_Global_CaptainOrder};

			if (m_client.registerPublishTopics(publishedTopicNames))
			{
				const std::vector<TopicName> subscribeTopicNames {Topic_Routing_Route_BasicData
				                                                  , Topic_Routing_Waypoint_Description
				                                                  , Topic_Situation_Gps_Full
				                                                  , Topic_Situation_Wind_Full
				                                                  , Topic_Situation_Water_RelativeSpeed
				                                                  , Topic_Situation_IMU_Compass
				                                                  , Topic_Global_CaptainOrder};

				if (m_client.registerSubscribeTopics(subscribeTopicNames))
				{
					for (auto topicName : subscribeTopicNames)
					{
						if (!m_client.asyncSubscribe(topicName))
						{
							break;
						}
					}
				}
			}
		}
	}
}

void GuiController::disconnectFromVehicle()
{
	// TODO
}

void GuiController::onAsyncConnectFailure(const mqtt::token & asyncActionToken)
{
}

void GuiController::onAsyncConnectSuccess(const mqtt::token & asyncActionToken)
{
}

void GuiController::onAsyncDisconnectFailure(const mqtt::token & asyncActionToken)
{
}

void GuiController::onAsyncDisconnectSuccess(const mqtt::token & asyncActionToken)
{
}

void GuiController::onAsyncPublishFailure(const mqtt::token & asyncActionToken)
{
}

void GuiController::onAsyncPublishSuccess(const mqtt::token & asyncActionToken)
{
}

void GuiController::onAsyncSubscribeFailure(const mqtt::token & asyncActionToken)
{
}

void GuiController::onAsyncSubscribeSuccess(const mqtt::token & asyncActionToken)
{
}

void GuiController::onAsyncUnsubscribeFailure(const mqtt::token & asyncActionToken)
{
}

void GuiController::onAsyncUnsubscribeSuccess(const mqtt::token & asyncActionToken)
{
}

void GuiController::onMessageReceived(const TopicName & topicName, const char * buffer, const size_t bufferSize)
{
#define DEFINE_TOPIC_MANAGEMENT(Name) \
        if (topicName == Topic_##Name) \
        { \
            if (bufferSize != sizeof(Data_##Name)) \
            { \
                LOG_ERR("Bad buffer size received for Data_", #Name); \
            } \
            else \
            { \
                Data_##Name * data = (Data_##Name *)buffer; \
                manage_##Name(data); \
            } \
            return; \
        }

	DEFINE_TOPIC_MANAGEMENT(Routing_Route_BasicData)
}

void GuiController::manage_Routing_Route_BasicData(Data_Routing_Route_BasicData * data)
{
	LOG_DEBUG("Received Data_Routing_Route_BasicData totalWaypointCount=", data->totalWaypointCount, " routeName='", data->routeName, "'");
}
