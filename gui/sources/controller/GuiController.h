/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   GuiController.h
 * Author: esppat
 *
 * Created on January 2, 2020, 6:10 PM
 */

#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <QtCore/QtCore>

#include "tools/gps/GeoPos.h"
#include "tools/miscel/Lockable.h"
#include "tools/communication/Client.h"

#include "communication/MessageDefinition.h"

class GuiController
	: public QObject
	  , public IClientCallback
	  , public Lockable
{
Q_OBJECT

public:

	GuiController();

	GuiController(const GuiController & orig) = delete;

	virtual ~GuiController() = default;

public slots:

	void connectToVehicle();

	void disconnectFromVehicle();

signals:

	void connectedToVehicle(const bool isConnected);

	void disconnectedFromVehicle(const bool isConnected);

	void GSPSpeedChanged(const double & value);

	void GSPBearingChanged(const double & value);

	void GPSPositionChanged(const double & lat, const double & lon);

	void waterSpeedChanged(const double & value);

	void relativeWindSpeedChanged(const double & value);

	void relativeWindBearingChanged(const double & value);

	void realWindSpeedChanged(const double & value);

	void realWindBearingChanged(const double & value);

	void compassBearingChanged(const double & value);

	void helmPercentChanged(const double & value);

	void targetBearingChanged(const double & value);

	void bearingErrorChanged(const double & value);

	void crossTrackErrorChanged(const double & value);

	void leftThrottleChanged(const double & value);

	void rightThrottleChanged(const double & value);

	void createSegment(const mpasv::tools::geo::GeoPos p1, const mpasv::tools::geo::GeoPos p2, const QString & color);

protected:

	// IClientCallback >>>>>>
	virtual void onAsyncConnectFailure(const mqtt::token & asyncActionToken) override;
	virtual void onAsyncConnectSuccess(const mqtt::token & asyncActionToken) override;

	virtual void onAsyncDisconnectFailure(const mqtt::token & asyncActionToken) override;
	virtual void onAsyncDisconnectSuccess(const mqtt::token & asyncActionToken) override;

	virtual void onAsyncPublishFailure(const mqtt::token & asyncActionToken) override;
	virtual void onAsyncPublishSuccess(const mqtt::token & asyncActionToken) override;

	virtual void onAsyncSubscribeFailure(const mqtt::token & asyncActionToken) override;
	virtual void onAsyncSubscribeSuccess(const mqtt::token & asyncActionToken) override;

	virtual void onAsyncUnsubscribeFailure(const mqtt::token & asyncActionToken) override;
	virtual void onAsyncUnsubscribeSuccess(const mqtt::token & asyncActionToken) override;

	virtual void onMessageReceived(const TopicName & topicName, const char * buffer, const size_t bufferSize) override;
	// IClientCallback <<<<<<

protected:
private:

	void manage_Routing_Route_BasicData(Data_Routing_Route_BasicData * data);

private:

	Client m_client;
};

#endif /* CONTROLLER_H */
