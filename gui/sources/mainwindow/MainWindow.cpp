#include "MainWindow.h"

MainWindow::MainWindow(QWidget * parent)
	: QMainWindow(parent)
{
	setDockOptions(QMainWindow::AnimatedDocks | QMainWindow::AllowNestedDocks | QMainWindow::GroupedDragging);

	createController();
	createMenus();
	createWindows();

	createConnections();
}

void MainWindow::connectedToVehicle(const bool isConnected)
{
	m_actionConnectVehicle->setChecked(isConnected);
	m_actionDisconnectVehicle->setChecked(!isConnected);

	m_actionConnectVehicle->setDisabled(isConnected);
	m_actionDisconnectVehicle->setDisabled(!isConnected);
}

void MainWindow::disconnectedFromVehicle(const bool isConnected)
{
	m_actionConnectVehicle->setChecked(isConnected);
	m_actionDisconnectVehicle->setChecked(!isConnected);

	m_actionConnectVehicle->setDisabled(isConnected);
	m_actionDisconnectVehicle->setDisabled(!isConnected);
}

void MainWindow::createController()
{
	m_controller = new GuiController();
}

void MainWindow::createWindows()
{
	initDockWindow<MapWidget>(m_mapWindow, "Map", "&Map", m_menuMap, Qt::RightDockWidgetArea);

	initDockWindow<BasicGaugeWidget>(m_gpsSpeedWindow, "GPS speed", "GPS &speed", m_menuGauges, Qt::TopDockWidgetArea);
	initDockWindow<BasicGaugeWidget>(m_waterSpeedWindow, "Water speed", "&Water speed", m_menuGauges, Qt::TopDockWidgetArea);
	initDockWindow<BasicGaugeWidget>(m_gpsBearingWindow, "GPS bearing", "GPS &bearing", m_menuGauges, Qt::TopDockWidgetArea);
	initDockWindow<BasicGaugeWidget>(m_compassBearingWindow, "Compass bearing", "Compass &bearing", m_menuGauges, Qt::TopDockWidgetArea);
	initDockWindow<BasicGaugeWidget>(m_targetBearingWindow, "Target bearing", "Target &bearing", m_menuGauges, Qt::TopDockWidgetArea);
	initDockWindow<BasicGaugeWidget>(m_bearingErrorWindow, "Bearing error", "Bearing &error", m_menuGauges, Qt::TopDockWidgetArea);
	initDockWindow<BasicGaugeWidget>(m_crossTrackErrorWindow, "Cross track error", "Cross track error", m_menuGauges, Qt::TopDockWidgetArea);
	initDockWindow<BasicGaugeWidget>(m_helmWindow, "Helm", "&Helm", m_menuGauges, Qt::TopDockWidgetArea);
	initDockWindow<BasicGaugeWidget>(m_relativeWindSpeedWindow, "Relative wind speed", "&Relative wind speed", m_menuGauges, Qt::TopDockWidgetArea);
	initDockWindow<BasicGaugeWidget>(m_relativeWindBearingWindow, "Relative wind bearing", "Re&lative wind bearing", m_menuGauges, Qt::TopDockWidgetArea);
	initDockWindow<BasicGaugeWidget>(m_realWindSpeedWindow, "Real wind speed", "Real wind speed", m_menuGauges, Qt::TopDockWidgetArea);
	initDockWindow<BasicGaugeWidget>(m_realWindBearingWindow, "Real wind bearing", "Real wind bearing", m_menuGauges, Qt::TopDockWidgetArea);

#ifdef __DEBUG__
	//	createDebugCharts();
#endif
}

void MainWindow::createMenus()
{
	m_menuApplication = menuBar()->addMenu("&Application");
	m_menuVehicle     = menuBar()->addMenu("&Vehicle");
	m_menuMap         = menuBar()->addMenu("&Map");
	m_menuGauges      = menuBar()->addMenu("&Gauges");
	m_menuCharts      = menuBar()->addMenu("&Charts");

	m_actionConnectVehicle = new QAction("Connect", this);
	m_actionConnectVehicle->setCheckable(true);
	m_actionConnectVehicle->setChecked(false);
	m_actionConnectVehicle->setDisabled(false);
	m_menuVehicle->addAction(m_actionConnectVehicle);

	m_actionDisconnectVehicle = new QAction("Disconnect", this);
	m_actionDisconnectVehicle->setCheckable(true);
	m_actionDisconnectVehicle->setChecked(true);
	m_actionDisconnectVehicle->setDisabled(true);
	m_menuVehicle->addAction(m_actionDisconnectVehicle);
}

void MainWindow::createConnections()
{
	// controller updates
	CHECK_TRUE((bool)connect(m_controller, &GuiController::GSPSpeedChanged, m_gpsSpeedWindow->m_widget, &BasicGaugeWidget::setCurrentValue));
	CHECK_TRUE((bool)connect(m_controller, &GuiController::GSPBearingChanged, m_gpsBearingWindow->m_widget, &BasicGaugeWidget::setCurrentValue));
	CHECK_TRUE((bool)connect(m_controller, &GuiController::GPSPositionChanged, m_mapWindow->m_widget, &MapWidget::createCircleAtPos));
	CHECK_TRUE((bool)connect(m_controller, &GuiController::GPSPositionChanged, m_mapWindow->m_widget, &MapWidget::gotoGPSPosition));
	CHECK_TRUE((bool)connect(m_controller, &GuiController::createSegment, m_mapWindow->m_widget, &MapWidget::createSegment));
	CHECK_TRUE((bool)connect(m_controller, &GuiController::waterSpeedChanged, m_waterSpeedWindow->m_widget, &BasicGaugeWidget::setCurrentValue));
	CHECK_TRUE((bool)connect(m_controller, &GuiController::relativeWindSpeedChanged, m_relativeWindSpeedWindow->m_widget, &BasicGaugeWidget::setCurrentValue));
	CHECK_TRUE((bool)connect(m_controller
	                         , &GuiController::relativeWindBearingChanged
	                         , m_relativeWindBearingWindow->m_widget
	                         , &BasicGaugeWidget::setCurrentValue));
	CHECK_TRUE((bool)connect(m_controller, &GuiController::realWindSpeedChanged, m_realWindSpeedWindow->m_widget, &BasicGaugeWidget::setCurrentValue));
	CHECK_TRUE((bool)connect(m_controller, &GuiController::realWindBearingChanged, m_realWindBearingWindow->m_widget, &BasicGaugeWidget::setCurrentValue));
	CHECK_TRUE((bool)connect(m_controller, &GuiController::compassBearingChanged, m_compassBearingWindow->m_widget, &BasicGaugeWidget::setCurrentValue));
	CHECK_TRUE((bool)connect(m_controller, &GuiController::helmPercentChanged, m_helmWindow->m_widget, &BasicGaugeWidget::setCurrentValue));
	CHECK_TRUE((bool)connect(m_controller, &GuiController::targetBearingChanged, m_targetBearingWindow->m_widget, &BasicGaugeWidget::setCurrentValue));
	CHECK_TRUE((bool)connect(m_controller, &GuiController::bearingErrorChanged, m_bearingErrorWindow->m_widget, &BasicGaugeWidget::setCurrentValue));
	CHECK_TRUE((bool)connect(m_controller, &GuiController::crossTrackErrorChanged, m_crossTrackErrorWindow->m_widget, &BasicGaugeWidget::setCurrentValue));

	// Vehicle connection
	CHECK_TRUE((bool)connect(m_actionConnectVehicle, &QAction::triggered, m_controller, &GuiController::connectToVehicle));
	CHECK_TRUE((bool)connect(m_controller, &GuiController::connectedToVehicle, this, &MainWindow::connectedToVehicle));
	CHECK_TRUE((bool)connect(m_actionDisconnectVehicle, &QAction::triggered, m_controller, &GuiController::disconnectFromVehicle));
	CHECK_TRUE((bool)connect(m_controller, &GuiController::disconnectedFromVehicle, this, &MainWindow::disconnectedFromVehicle));
}
