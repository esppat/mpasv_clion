#include "mainwindow/MainWindow.h"

#include <QApplication>

#include "Parameters.h"

#include <csignal>

void receive_SIGINT(int /*param*/)
{
}

void receive_SIGABRT(int /*param*/)
{
}

void receive_SIGPIPE(int /*param*/)
{
}

int main(int argc, char * argv[])
{
	// install signal handlers
	//	signal(SIGINT, receive_SIGINT);
	//	signal(SIGABRT, receive_SIGABRT);
	signal(SIGPIPE, receive_SIGPIPE);

	mpasv::tools::parameters::ParameterManager::setParameterFileName("data/parameters/gui-parameters.json");
	mpasv::tools::parameters::ParameterManager::get();
	mpasv::tools::parameters::ParameterManager::get()->save(); // for auto-formatting

	QApplication a(argc, argv);
	MainWindow   w;
	w.show();
	return QApplication::exec();
}
